/*

-- 数据库  :能源(电能)管理平台
-- 脚本类型:期初数据

-- 创建信息 --
-- 作者    :杨永红
-- 版本    :V 1.0
-- 时间    :2017-05-10

-- 修订信息 --
-- 修订者  :
-- 版本    :
-- 时间    :
-- 修订概要:

*/

-- tmp table
DROP TABLE IF EXISTS "tmp_sys_config" CASCADE;
CREATE TABLE "tmp_sys_config" (
"class_id" VARCHAR(40),
"id" VARCHAR(40),
"name" VARCHAR(40),
"flag" BOOLEAN,
"info_id" VARCHAR(40),
"info_name" VARCHAR(40),
"info_flag" BOOLEAN,
"infoset_id" VARCHAR(40),
"infoset_name" VARCHAR(40),
"infoset_flag" BOOLEAN,
"info_value" TEXT,
"origin_class_id" VARCHAR(40),
"relate_class_id" VARCHAR(40),
"origin_id" VARCHAR(40),
"relate_id" VARCHAR(40),
"parent_id" VARCHAR(40),
"pre_id" VARCHAR(40)
)
INHERITS ("sys_void") 
;

-- bu item config
TRUNCATE TABLE "tmp_sys_config" CASCADE;
COPY tmp_sys_config(class_id,parent_id,pre_id,id,name,flag,info_id,infoset_id,info_value)
	FROM 'D:/myApp/script/eCloud/public/bu_data/21.bu_item_data.csv'
	DELIMITER AS ',' csv HEADER QUOTE AS '"' ENCODING 'gbk';
	
-- 07.sys_item
WITH cte AS(
	SELECT DISTINCT id,name,flag,parent_id,pre_id,class_id FROM tmp_sys_config
	)SELECT dml_ci_insert(jsonb_agg(cte)) FROM cte;
	
-- 08.sys_item_info
WITH cte AS(
	SELECT id,info_id,infoset_id,info_value
		FROM tmp_sys_config
	 WHERE info_id IS NOT NULL
	)SELECT dml_ci_info_insert(jsonb_agg(cte)) FROM cte;

-- system item relation config
TRUNCATE TABLE "tmp_sys_config" CASCADE;
COPY tmp_sys_config(origin_id,relate_id,flag)
	FROM 'D:/myApp/script/eCloud/public/bu_data/22.bu_item_relation_data.csv'
	DELIMITER AS ',' csv HEADER QUOTE AS '"' ENCODING 'gbk';

-- 09.sys_item_relation
WITH cte AS(
	SELECT origin_id,relate_id,flag FROM tmp_sys_config
	)SELECT dml_ci_relation_insert(jsonb_agg(cte)) FROM cte;

-- clear;
DROP TABLE IF EXISTS "tmp_sys_config" CASCADE;