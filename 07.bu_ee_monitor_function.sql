-- 获取tag信息
/*test script
	SELECT func_get_tag()
*/
CREATE OR REPLACE FUNCTION "func_get_tag" () RETURNS JSONB
  VOLATILE
AS $BODY$
DECLARE
	v_result JSONB := NULL;
BEGIN
	WITH cte AS(
		SELECT ci.uid,ci.id,ci.name,ci.flag
				 , func_get_ci_infoset(ci.uid) AS info
			FROM ci_tag AS ci
		 WHERE ci.flag = TRUE
	)	
	SELECT json_agg(cte) INTO v_result FROM cte;
	RETURN v_result;
END;
$BODY$ LANGUAGE plpgsql
;

-- 获取监测站布局
/*test script
	SELECT func_get_station(uid,'') FROM ci_account;
	SELECT func_get_station('c68c7e8a-4df3-4f01-85cd-5710feee5fa1',jsonb_agg(rel)::TEXT) FROM ci_station AS rel WHERE rel.parent_uid IS NULL;
*/
CREATE OR REPLACE FUNCTION "func_get_station" (UUID,TEXT) RETURNS JSONB
  VOLATILE
AS $BODY$
DECLARE
	v_result JSONB := NULL;
BEGIN
	IF $2 = '' OR $2 = '[{}]' OR $2 IS NULL THEN
		WITH cte_st_inf AS(
			SELECT DISTINCT ci.uid,ci.id,ci.name,ci.flag,ci.class_uid
					 , func_get_ci_infoset(ci.uid)-'contents' AS info
				FROM ci_account AS ca 
			 INNER JOIN v_ci_relation AS ao ON ca.uid = ao.origin_uid
			 INNER JOIN v_ci_relation AS oz ON ao.relate_uid = oz.origin_uid
			 INNER JOIN ci_station AS ci ON oz.relate_uid = ci.uid
 			 WHERE ca.uid = $1
-- 			 WHERE ci.parent_uid IS NULL
		),
		cte_st_point AS(
			SELECT st.uid ,jsonb_agg(rel.relate_id) AS point
				FROM cte_st_inf AS st
			 INNER JOIN v_ci_relation AS rel ON st.uid = rel.origin_uid AND rel.relate_flag = TRUE
			 GROUP BY st.uid
		),
		cte_st AS(
			SELECT st.*,pt.point
				FROM cte_st_inf AS st
				LEFT JOIN cte_st_point AS pt ON st.uid = pt.uid
		)
		SELECT json_agg(cte_st) INTO v_result FROM cte_st;
	ELSE
		WITH cte_st_inf AS(
			SELECT DISTINCT ci.uid,ci.id,ci.name,ci.flag,ci.class_uid
					 , func_get_ci_infoset(ci.uid) AS info
				FROM jsonb_to_recordset($2::JSONB) AS t1(uid UUID)
			 INNER JOIN ci_station AS ci ON t1.uid = ci.uid
			 INNER JOIN v_ci_relation AS oz ON ci.uid = oz.relate_uid
			 INNER JOIN v_ci_relation AS ao ON oz.origin_uid = ao.relate_uid
			 INNER JOIN ci_account AS ca ON ca.uid = ao.origin_uid
		),
		cte_st_point AS(
			SELECT st.uid,jsonb_agg(rel.relate_id) AS point
				FROM cte_st_inf AS st
			 INNER JOIN v_ci_relation AS rel ON st.uid = rel.origin_uid AND rel.relate_flag = TRUE
			 GROUP BY st.uid
		),
		cte_st AS(
			SELECT st.*,pt.point
				FROM cte_st_inf AS st
				LEFT JOIN cte_st_point AS pt ON st.uid = pt.uid
		)
		SELECT json_agg(cte_st) INTO v_result FROM cte_st;
	END IF;
	RETURN v_result;
END;
$BODY$ LANGUAGE plpgsql
;

-- 获取监测站布局
/*test script
	SELECT func_get_station_layout(jsonb_agg(ci_station)) FROM ci_station WHERE parent_uid IS NULL;
	SELECT func_get_station_layout('[{"uid":"83752f8d-2aae-42b1-9062-e6b3ef3c0bb0"}]')	
*/
CREATE OR REPLACE FUNCTION "func_get_station_layout"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_jsonb JSONB := $1::JSONB;
	v_result JSONB := NULL;
	BEGIN
		-- station info
		WITH cte_st_inf AS(
			SELECT ci.uid,ci.id,ci.name,ci.flag
					 , func_get_ci_infoset(ci.uid)-'contents' AS info
				FROM jsonb_to_recordset(v_jsonb) AS t1(uid UUID)
			 INNER JOIN ci_station AS ci ON t1.uid = ci.uid
		),
		-- transf info
		cte_tf_inf AS(
			SELECT t1.uid AS station_uid
					 , ci.uid,ci.id,ci.name,ci.flag
					 , func_get_ci_infoset(ci.uid) AS info 
					 , func_get_ci_sub_info(ci.uid) AS feeder
				FROM jsonb_to_recordset(v_jsonb) AS t1(uid UUID)
			 INNER JOIN rel_station_point AS rel ON t1.uid = rel.origin_uid
			 INNER JOIN ci_point AS ci ON rel.relate_uid = ci.uid
			 WHERE ci.parent_uid IS NULL
 -- for NJYC test
			   AND ci.id NOT IN('NJYC_49','NJYC_50')
		),
		cte_st AS(
			SELECT st.uid,row_to_json(st) AS station FROM cte_st_inf AS st
		),
		cte_tf AS(
			select tf.station_uid,json_agg(tf) AS transf FROM cte_tf_inf AS tf GROUP BY tf.station_uid
		),
		cte_rel AS(
			SELECT st.station,tf.transf
				FROM cte_st AS st
			 INNER JOIN cte_tf AS tf ON st.uid = tf.station_uid
		)
		SELECT json_agg(cte_rel) INTO v_result FROM cte_rel;
		RETURN v_result;			
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 获取监测站布局树形结构
/*test script
	SELECT func_get_station_tree(jsonb_agg(ci_station)) FROM ci_station WHERE parent_uid IS NULL;
*/
CREATE OR REPLACE FUNCTION "func_get_station_tree"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_jsonb JSONB := $1::JSONB;
	v_result JSONB := NULL;
	BEGIN
		WITH cte AS(
			SELECT pt.uid,pt.id,pt.name,pt.flag
					 , func_get_ci_sub(pt.uid) AS sub
					 , t1.uid AS station_uid
				FROM jsonb_to_recordset(v_jsonb) AS t1(uid UUID)
			 INNER JOIN rel_station_point AS rel ON t1.uid = rel.origin_uid
			 INNER JOIN ci_point AS pt ON pt.uid = rel.relate_uid
		),
		cte_st AS(
			SELECT st.uid,st.id,st.name,st.flag
					 , json_agg(cte) AS sub
				FROM cte
			 INNER JOIN ci_station AS st ON cte.station_uid = st.uid
			 GROUP BY st.uid,st.id,st.name,st.flag
		)
		SELECT json_agg(cte_st) INTO v_result FROM cte_st;
		RETURN v_result;			
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 获取监测站变压器信息
/*test script
	SELECT func_get_station_device(jsonb_agg(ci_station)) FROM ci_station WHERE parent_uid IS NULL;
*/
CREATE OR REPLACE FUNCTION "func_get_station_device" (JSONB) RETURNS JSONB
  VOLATILE
AS $BODY$
DECLARE
	v_jsonb JSONB := $1::JSONB;
	v_result JSONB := NULL;
BEGIN
	WITH cte AS(
		SELECT dev.uid,dev.id,dev.name,dev.flag 
				 , st.uid AS station_uid,st.id AS station_id,st.name AS station_name,st.flag AS station_flag
				 , pt.uid AS point_uid,pt.id AS point_id,pt.name AS point_name,pt.flag AS point_flag				 
				 , pre.uid AS pre_point_uid,pre.id AS pre_point_id,pre.name AS pre_point_name,pre.flag AS pre_point_flag
				 , func_get_ci_infoset(dev.uid) AS info
			FROM jsonb_to_recordset(v_jsonb) AS t1(uid UUID)		 
		 INNER JOIN rel_station_point AS st_pt ON t1.uid = st_pt.origin_uid		 
		 INNER JOIN rel_point_device AS pt_dev ON pt_dev.origin_uid = st_pt.relate_uid	
		 INNER JOIN ci_device AS dev ON dev.uid = pt_dev.relate_uid
		 INNER JOIN ci_station AS st ON t1.uid = st.uid
		 INNER JOIN ci_point AS pt ON st_pt.relate_uid = pt.uid
		 INNER JOIN ci_point AS pre ON pt.pre_uid = pre.uid
	 )
	SELECT json_agg(cte) INTO v_result FROM cte;	 
	RETURN v_result;
END;
$BODY$ LANGUAGE plpgsql
;
