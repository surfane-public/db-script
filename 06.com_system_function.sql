/*

-- 数据库  :平台通用
-- 脚本类型:系统运行基本函数创建脚本

-- 创建信息 --
-- 作者    :杨永红
-- 版本    :V 1.0
-- 时间    :2017-10-10

-- 修订信息 --
-- 修订者  :
-- 版本    :
-- 时间    :
-- 修订概要:

*/


-- 帐号验证
/*test script
  SELECT func_account_authentication('{"id":"SysAdmin","password":"SysAdmin"}');
*/
CREATE OR REPLACE FUNCTION "func_account_authentication" (JSONB)  RETURNS JSONB
  VOLATILE
AS $BODY$
DECLARE
		v_data JSONB := $1::JSONB;
		v_count INTEGER := 0;
		v_psw_account INTEGER := 0;
		v_id VArCHAR := NULL;
    v_result JSONB := NULL;
BEGIN
	SELECT count(0),t0.id into v_count,v_id
		FROM jsonb_to_record(v_data) AS t0(id text)
	 INNER JOIN ci_account t1 ON t0.id = t1.id
	 GROUP BY t0.id LIMIT 1;
	SELECT count(0) INTO v_psw_account
		FROM jsonb_to_record(v_data) AS t0(id text)
	 INNER JOIN ci_account t1 ON t0.id = t1.id
	 INNER JOIN info_account t2 ON t1.uid = t2.item_uid
	 INNER JOIN sys_info t3 ON t2.info_uid = t3.uid
	 WHERE t3.id = 'password';
	IF 1 > v_count THEN
		SELECT dml_function_state_select('U0201',v_id) INTO v_result;
	ELSEIF 1 < v_count THEN
		SELECT dml_function_state_select('U0202',v_id) INTO v_result;
	ELSEIF 1 > v_psw_account THEN
		SELECT dml_function_state_select('U0203') INTO v_result;
	ELSEIF 1 < v_psw_account THEN
		SELECT dml_function_state_select('U0204') INTO v_result;
	ELSE
		SELECT count(0) INTO v_count
			FROM jsonb_to_record(v_data) AS t0(id text,password text)
		 INNER JOIN ci_account t1 ON t0.id = t1.id
		 INNER JOIN info_account t2 ON t1.uid = t2.item_uid
		 INNER JOIN sys_info t3 ON t2.info_uid = t3.uid
		 WHERE t3.id = 'password'
			 AND t2.info_value = MD5(t0.password);
		IF 1 != v_count THEN
			SELECT dml_function_state_select('U0205',v_id) INTO v_result;
		ELSE
			-- account info
			WITH cte AS(
				SELECT t1.uid,t1.id,t1.name
						 , jsonb_object(array_agg(t3.id),array_agg(COALESCE(t4.info_value,t2.info_value))) - 'password'::TEXT AS info
					FROM jsonb_to_record(v_data) AS t0(id text)
				 INNER JOIN ci_account t1 ON t0.id = t1.id
				 INNER JOIN info_account t2 ON t1.uid = t2.item_uid
				 INNER JOIN sys_info t3 ON t2.info_uid = t3.uid
				  LEFT JOIN sys_infoset t4 ON t2.infoset_uid = t4.uid
				 GROUP BY t1.uid,t1.id,t1.name
			)
			SELECT row_to_json (cte) INTO v_result FROM cte;
		END IF;
	END IF;

	RETURN v_result;
	END;
$BODY$ LANGUAGE plpgsql
;

-- 获取帐号对应菜单
/*test script
  SELECT func_run(16,uid::TEXT,''::TEXT) FROM ci_account;
  SELECT func_account_menu(uid,'') FROM ci_account;
*/
CREATE OR REPLACE FUNCTION "func_account_menu" (UUID,TEXT) RETURNS JSONB
  VOLATILE
AS $BODY$
DECLARE
	v_account_uid UUID := $1::UUID;
	v_menu_uid UUID := NULL;	
	v_result JSONB := NULL;
BEGIN
	IF '' = $2 OR $2 IS NULL THEN 
		SELECT uid INTO v_menu_uid FROM ci_menu WHERE parent_uid ISNULL AND pre_uid IS NULL LIMIT 1;
	ELSE
		v_menu_uid := $2;
	END IF;
	-- for old function
	WITH cte AS(
			SELECT v_menu_uid AS menu_uid, func_account_menu_list(v_account_uid,v_menu_uid) AS menu
		)
	SELECT row_to_json(cte) INTO v_result FROM cte;
	RETURN v_result;
	-- for vue win function
	-- RETURN func_account_menu_list(v_account_uid,v_menu_uid);
END;
$BODY$ LANGUAGE plpgsql
;

-- 获取帐号对应递归菜单
CREATE OR REPLACE FUNCTION "func_account_menu_list" (UUID,UUID) RETURNS JSONB
  VOLATILE
AS $BODY$
DECLARE
	v_account_uid UUID := $1::UUID;
	v_menu_uid UUID := $2::UUID;	
  v_result JSONB := NULL;
BEGIN
	-- get recursive menu list
	WITH RECURSIVE cte1 AS (
		SELECT ci.id,ci.name,ci.uid,ci.parent_uid,ci.pre_uid,ci.flag
		      , func_account_menu_list(v_account_uid,ci.uid) sub 
			FROM ci_menu AS ci
		 WHERE ci.parent_uid = v_menu_uid 
			 AND ci.pre_uid IS NULL 
		UNION
		SELECT ci.id,ci.name,ci.uid,ci.parent_uid,ci.pre_uid,ci.flag
		     , func_account_menu_list(v_account_uid,ci.uid) sub 
			FROM ci_menu AS ci
		 INNER JOIN cte1 ON ci.pre_uid = cte1.uid
	),
	-- get menu info
	cte2 AS(
		SELECT cte1.uid,jsonb_object(array_agg(inf.info_id),array_agg(COALESCE(inf.infoset_value,inf.info_value))) AS info
			FROM cte1
			LEFT JOIN rel_ui_menu AS rel ON cte1.uid = rel.relate_uid
		 INNER JOIN v_ci_info AS inf ON inf.uid = cte1.uid OR inf.uid = rel.origin_uid
		 WHERE inf.info_uid IS NOT NULL
		 GROUP BY cte1.uid
	),
	-- get function privilege
	cte3 AS(
		SELECT um.relate_uid as uid
				 , CASE SUM(CAST(COALESCE(ru.flag,FALSE) AS INT)) WHEN 0 THEN FALSE ELSE TRUE END AS privilege
			FROM rel_account_role AS ar
		 INNER JOIN rel_role_ui AS ru ON ru.origin_uid = ar.relate_uid
		 INNER JOIN rel_ui_menu AS um ON ru.relate_uid = um.origin_uid
		 WHERE ar.origin_uid = v_account_uid
		 GROUP BY um.relate_uid
	),
	-- join all
	cte4 AS(
		SELECT cte1.uid,cte1.id,cte1.name,cte1.parent_uid,cte1.pre_uid,cte1.flag
				 , cte1.sub
				 , cte2.info
				 , cte3.privilege
			FROM cte1 
			LEFT JOIN cte2 ON cte1.uid = cte2.uid
		  LEFT JOIN cte3 ON cte1.uid = cte3.uid
	)
	SELECT jsonb_agg (cte4) INTO v_result FROM cte4;
	RETURN v_result;
END;
$BODY$ LANGUAGE plpgsql
;

-- 获取帐号收藏信息(桌面快捷方式)
/*test script
	SELECT func_account_favourite(uid) FROM ci_account;
*/
CREATE OR REPLACE FUNCTION "func_account_favourite" (UUID)  RETURNS JSONB
  VOLATILE
AS $BODY$
DECLARE
		v_account_uid UUID := $1::UUID;
    v_result JSONB := NULL;
BEGIN
	-- get menu list by account
	WITH cte AS(
		SELECT origin_uid,relate_uid FROM rel_account_menu WHERE origin_uid = v_account_uid 
	),
	-- get privilege of account,distinct menu
	cte1 AS(
		SELECT nm.uid,nm.id,nm.name
				 , CASE SUM(CAST(COALESCE(ru.flag,FALSE) AS INT)) WHEN 0 THEN FALSE ELSE TRUE END AS privilege
			FROM cte AS am
		 INNER JOIN rel_account_role ar ON am.origin_uid = ar.origin_uid 
		 INNER JOIN ci_menu nm ON am.relate_uid = nm.uid
			LEFT JOIN rel_ui_menu um ON am.relate_uid = um.relate_uid
			LEFT JOIN rel_role_ui ru ON um.origin_uid = ru.relate_uid AND ar.relate_uid = ru.origin_uid
		 GROUP BY nm.uid,nm.id,nm.name
	),
	-- get menu ifo
	cte2 AS(
		SELECT cte1.uid,jsonb_object(array_agg(inf.info_id),array_agg(COALESCE(inf.infoset_value,inf.info_value))) AS info
			FROM cte1
			LEFT JOIN rel_ui_menu AS rel ON cte1.uid = rel.relate_uid
		 INNER JOIN v_ci_info AS inf ON inf.uid = cte1.uid OR inf.uid = rel.origin_uid
		 WHERE inf.info_uid IS NOT NULL
		 GROUP BY cte1.uid
	),
	cte3 AS(
		SELECT cte1.uid,cte1.id,cte1.name,cte1.privilege
				 , cte2.info
			FROM cte1
		  LEFT JOIN cte2 ON cte1.uid = cte2.uid 
	)
	SELECT jsonb_agg (cte3) into v_result FROM cte3;	 
	RETURN v_result;
END;
$BODY$ LANGUAGE plpgsql
;

-- 添加帐号收藏信息(桌面快捷方式)
CREATE OR REPLACE FUNCTION "func_account_favourite_add" (UUID,JSONB)  RETURNS JSONB
  VOLATILE
AS $BODY$
DECLARE
		v_account_uid UUID := $1::UUID;
		v_data JSONB := $2::JSONB;
    v_result JSONB := NULL;
BEGIN
	WITH cte AS(
		INSERT INTO rel_account_menu(origin_class_uid,origin_uid,relate_class_uid,relate_uid)
		SELECT ca.class_uid,ca.uid,cm.class_uid,cm.uid
			FROM jsonb_to_recordset(v_data) AS t0(uid UUID),ci_menu AS cm,ci_account AS ca
		 WHERE t0.uid = cm.uid
			 AND ca.uid = v_account_uid
		   RETURNING rel_account_menu.*
	)
	SELECT jsonb_agg(cte) INTO v_result FROM cte;
	RETURN v_result;
END;
$BODY$ LANGUAGE plpgsql
;

-- 删除帐号收藏信息(桌面快捷方式)
CREATE OR REPLACE FUNCTION "func_account_favourite_remove" (UUID,JSONB)  RETURNS JSONB
  VOLATILE
AS $BODY$
DECLARE
		v_account_uid UUID := $1::UUID;
		v_data JSONB := $2::JSONB;
    v_result JSONB := NULL;
BEGIN
	WITH cte AS(
		DELETE FROM rel_account_menu		
		 USING jsonb_to_recordset(v_data) AS t1(uid UUID)
		 WHERE rel_account_menu.origin_uid = v_account_uid
			 AND rel_account_menu.relate_uid = t1.uid
		   RETURNING rel_account_menu.*
	)
	SELECT jsonb_agg(cte) INTO v_result FROM cte;
	RETURN v_result;
END;
$BODY$ LANGUAGE plpgsql
;

-- 修改帐号密码
/*test script
  SELECT func_account_init('','[{"id":"Admin","birthday":"2002-02-02","password":"Adminswwww"}]');
*/
CREATE OR REPLACE FUNCTION "func_account_init"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_data JSONB := $2;
	v_uid UUID := NULL;
	v_id VARCHAR := NULL;
	v_result JSONB := NULL;
	BEGIN
		SELECT rel.id,rel.uid INTO v_id,v_uid 
			FROM jsonb_to_recordset(v_data) AS t1(uid UUID, id VARCHAR)
		 INNER JOIN ci_account AS rel ON t1.uid = rel.uid AND t1.id != rel.id LIMIT 1;
		IF FOUND THEN
			SELECT dml_function_state_select('U0103'::CHAR(5),v_id,v_uid::VARCHAR) INTO v_result;
		ELSE
			SELECT t1.id,t1.uid INTO v_id,v_uid
				FROM jsonb_to_recordset(v_data) AS t1(uid UUID, id VARCHAR, birthday VARCHAR)
				LEFT JOIN ci_account AS rel ON t1.uid = rel.uid OR t1.id = rel.id
			 WHERE rel.uid IS NULL LIMIT 1;
			IF FOUND THEN
				SELECT dml_function_state_select('U0201'::CHAR(5),COALESCE(v_uid::TEXT,v_id)::VARCHAR) INTO v_result;
			ELSE
				SELECT ca.id INTO v_id
				  FROM jsonb_to_recordset(v_data) AS t1(uid UUID, id VARCHAR, birthday VARCHAR)
				 INNER JOIN ci_account AS ca ON t1.uid = ca.uid OR t1.id = ca.id
				 INNER JOIN info_account AS ia ON ca.uid = ia.item_uid
				 RIGHT JOIN sys_info AS inf ON inf.uid = ia.info_uid 
				 WHERE inf.id = 'birthday'
				   AND (ia.info_value IS NULL OR t1.birthday != ia.info_value) LIMIT 1;
				IF FOUND THEN
					SELECT dml_function_state_select('U0206'::CHAR(5),v_id) INTO v_result;
				ELSE
					WITH cte AS(
						SELECT ca.uid,inf.uid AS info_uid,COALESCE(t1.password,t1.birthday) AS info_value
							FROM jsonb_to_recordset(v_data) AS t1(uid UUID, id VARCHAR, birthday VARCHAR, password VARCHAR)
						 INNER JOIN ci_account AS ca ON t1.uid = ca.uid OR t1.id = ca.id
						 INNER JOIN info_account AS ia ON ca.uid = ia.item_uid
						 INNER JOIN sys_info AS inf ON inf.uid = ia.info_uid AND inf.id = 'password'
					)
					SELECT dml_ci_info_update(jsonb_agg(cte)) INTO v_result FROM cte;
				END IF;
			END IF;
		END IF;
		RETURN v_result;
	END;
$BODY$ LANGUAGE plpgsql
;

-- 账号注册-- 修改账号信息
/*test script
SELECT func_account_register('','[{"id":"测试ID2","name":"测试名2","info":[{"info_uid":"31cf9c4b-2003-4d73-b4f3-11f2a4a5ddf7","info_value":"测试属性"}]}]');
*/
CREATE OR REPLACE FUNCTION "func_account_register"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_data JSONB := $2 ::JSONB;
	v_id VARCHAR := NULL;	
	v_result JSONB := NULL;
	BEGIN
		SELECT rel.id INTO v_id
			FROM jsonb_to_recordset(v_data) AS t1(id VARCHAR)
		 INNER JOIN ci_account AS rel ON t1.id = rel.id LIMIT 1;
		IF FOUND THEN
			SELECT dml_function_state_select('U0200'::CHAR(5),v_id) INTO v_result;
		ELSE
			SELECT rel.name INTO v_id
				FROM jsonb_to_recordset(v_data) AS t1(name VARCHAR)
			 INNER JOIN ci_account AS rel ON t1.name = rel.name LIMIT 1;
			IF FOUND THEN
				SELECT dml_function_state_select('U0200'::CHAR(5),v_id) INTO v_result;
			ELSE
				WITH cte AS(
					SELECT cls.uid AS class_uid
							 , t1.id,t1.name,COALESCE(t1.flag,TRUE) AS flag
							 , cr.class_uid AS relate_class_uid,COALESCE(t1.role_uid,cr.uid) AS role_uid
							 , t1.info
						FROM jsonb_to_recordset(v_data) AS t1(id VARCHAR,name VARCHAR,flag BOOLEAN,role_uid UUID,info JSONB)
							 , sys_class AS cls
							 , ci_role AS cr
					 WHERE cls.id = 'account' AND cr.id = 'role_visitor'
				),
				cte_info AS(
					SELECT cte.id,inf.info_uid,inf.infoset_uid,inf.info_value,info_flag AS flag
						FROM cte,jsonb_to_recordset(cte.info) AS inf(info_uid UUID,infoset_uid UUID,info_value VARCHAR,info_flag BOOLEAN) 
				),
				-- insert ci_account
				insert_ci AS(
					INSERT INTO ci_account(id,name,flag,class_uid)
					SELECT id,name,flag,class_uid
						FROM cte
					RETURNING ci_account.*
				),
				-- insert info_account
				insert_info AS(
					INSERT INTO info_account(class_uid,item_uid,info_uid,infoset_uid,info_value)
					SELECT ci.class_uid,ci.uid,inf.info_uid,inf.infoset_uid,inf.info_value
						FROM insert_ci AS ci
					 INNER JOIN cte_info AS inf ON ci.id = inf.id
					 RETURNING info_account.*
			 ),
			 -- insert rel_account_role
			 insert_rel AS(
					INSERT INTO rel_account_role(origin_class_uid,relate_class_uid,origin_uid,relate_uid)
					SELECT ci.class_uid,cte.relate_class_uid,ci.uid,cte.role_uid
						FROM insert_ci AS ci
					 INNER JOIN cte ON ci.id = cte.id
				   RETURNING rel_account_role.*
			 )
			 SELECT jsonb_agg(insert_rel) INTO v_result FROM insert_rel;
			END IF ;
		END IF;
		RETURN v_result;
	END;
$BODY$ LANGUAGE plpgsql
;

-- 上传配置信息,配置项信息,配置项属性,配置项关联关系
/*test script
SELECT func_import_config('[{"uid":"","config_file":"25g1"},{"uid":"","config_file":"25g2"}]');
*/
CREATE OR REPLACE FUNCTION "func_import_config"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_rel VARCHAR(40) := NULL;
	v_station_uid UUID := NULL;
	v_cfg_file TEXT := NULL;
	v_par JSONB := NULL;
	v_sqlcmd TEXT := NULL;
	v_result JSONB := NULL;
	BEGIN
		-- remove config,库表需设置将父节点删除时递归删除子节点
		DELETE FROM sys_item 
			 USING jsonb_to_recordset($1) AS t1(uid UUID)
		   INNER JOIN sys_item_relation AS rel ON t1.uid = rel.origin_uid
			 WHERE rel.relate_uid = sys_item.uid;
			 
		-- create tmp table
	  SELECT REPLACE(extract(epoch FROM now())::VARCHAR,'.','_') INTO v_rel;		
		v_rel = FORMAT('tmp_%s',v_rel);
		v_sqlcmd := '
			DROP TABLE IF EXISTS %1$s CASCADE
			; 	
			CREATE TABLE %1$s (
				class_id VARCHAR(40),
				id VARCHAR(40),
				name VARCHAR(40),
				flag BOOLEAN,
				info_id VARCHAR(40),
				info_name VARCHAR(40),
				info_flag BOOLEAN,
				infoset_id VARCHAR(40),
				infoset_name VARCHAR(40),
				infoset_flag BOOLEAN,
				info_value TEXT,
				origin_class_id VARCHAR(40),
				relate_class_id VARCHAR(40),
				origin_id VARCHAR(40),
				relate_id VARCHAR(40),
				parent_id VARCHAR(40),
				pre_id VARCHAR(40)
				)
				INHERITS ("sys_void") 
			;
		';
		v_sqlcmd := FORMAT(v_sqlcmd,v_rel);
		EXECUTE v_sqlcmd;	
		
		FOR v_station_uid,v_cfg_file IN SELECT uid, config_file  
			FROM jsonb_to_recordset($1) AS t1(uid UUID, config_file VARCHAR)
			LOOP
			v_sqlcmd := '';
			-- clear tmp table
			v_sqlcmd :=v_sqlcmd || 'TRUNCATE TABLE %1$s CASCADE;';
			-- copy file to table
			v_sqlcmd := v_sqlcmd || '
				COPY %1$s(class_id,parent_id,pre_id,id,name,flag,info_id,infoset_id,info_value)
				FROM %2$s
				DELIMITER AS %4$s csv HEADER QUOTE AS %5$s ENCODING %6$s
				;
			';
			-- create  ci
			v_sqlcmd := v_sqlcmd || '
				WITH cte AS(
					SELECT DISTINCT id,name,flag,parent_id,pre_id,class_id FROM %1$s
				)SELECT dml_ci_insert(jsonb_agg(cte)) FROM cte
				;
			';
			-- create  ci_info
			v_sqlcmd := v_sqlcmd || '
				WITH cte AS(
					SELECT DISTINCT id,info_id,infoset_id,info_value	FROM %1$s
					 WHERE info_id IS NOT NULL
				)SELECT dml_ci_info_insert(jsonb_agg(cte)) FROM cte
				;
			';
			-- create  ci_relation
			v_sqlcmd := v_sqlcmd || '
				WITH cte AS(
					SELECT DISTINCT %3$s AS origin_uid,id AS relate_id,flag FROM %1$s
					WHERE parent_id IS NULL
				)SELECT dml_ci_relation_insert(jsonb_agg(cte)) FROM cte;
				;
			';
			v_sqlcmd := FORMAT(v_sqlcmd,v_rel,quote_literal(v_cfg_file),quote_literal(v_station_uid),quote_literal(','),quote_literal('"'),quote_literal('gbk'));
			EXECUTE v_sqlcmd;	
		END LOOP; 
		-- drop tmp table;
		v_sqlcmd := FORMAT('DROP TABLE IF EXISTS %1$s CASCADE;',v_rel);
		EXECUTE v_sqlcmd;
		RETURN v_result;
	END;
$BODY$
  LANGUAGE plpgsql
;

-- 获取全局配置信息
CREATE OR REPLACE FUNCTION "func_get_profile"()
  RETURNS JSONB
AS $BODY$
DECLARE
	v_result JSONB := NULL;
	BEGIN
-- 		SELECT json_agg(cls) INTO v_profile FROM ci_profile AS cls;
-- 		RETURN func_get_ci_info(v_profile);
		WITH CTE AS (
			SELECT ci.id,ci.name,jsonb_object(array_agg(rel.info_id),array_agg(COALESCE(rel.infoset_value,rel.info_value))) AS info
				FROM ci_profile AS ci
				INNER JOIN v_ci_info AS rel ON ci.class_uid = rel.class_uid
				GROUP BY ci.id,ci.name
		)SELECT json_agg(CTE) INTO v_result FROM CTE;
		RETURN v_result;		
	END;
$BODY$
  LANGUAGE plpgsql
;

-- 获取企业/部门信息
-- SELECT func_get_organiz()
CREATE OR REPLACE FUNCTION "func_get_organiz"()
  RETURNS JSONB
AS $BODY$
DECLARE
	v_result JSONB := NULL;
	BEGIN
		SELECT jsonb_agg(ci) INTO v_result FROM ci_organiz AS ci WHERE parent_uid IS NULL;
		RETURN v_result;		
	END;
$BODY$
  LANGUAGE plpgsql
;