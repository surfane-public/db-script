/*

-- 数据库  :能源(电能)管理平台
-- 脚本类型:系统运行基础配置脚本

-- 创建信息 --
-- 作者    :杨永红
-- 版本    :V 1.0
-- 时间    :2017-05-10

-- 修订信息 --
-- 修订者  :
-- 版本    :
-- 时间    :
-- 修订概要:

*/

-- 新增能源分类
-- 用于统计及统一导航入口
/*
SELECT dml_class_insert ('[
		{"id":"energy","name":"能源"},
		{"id":"electric","name":"电能","parent_id":"energy"},
		{"id":"device","name":"能源设备"}
]');

-- 新增电能分类,并创建对应实例与信息表
SELECT ddl_class_create ('[
	{"id":"point_ee","name":"电能监测点(位置)"},
	{"id":"station","name":"能源监测站"},		
	{"id":"device_ee","name":"电能设备"},
	{"id":"tag","name":"监测量"}
]');
-- 创建能源监测对应关系,
-- 关系n:1
SELECT ddl_class_relation_create('[
	{"id": "rel_organiz_station", "name": "企业所属站信息", "origin_class_id": "organiz", "relate_class_id": "station", "flag": FALSE}, 
	{"id": "rel_station_point_ee", "name": "站监测站信息", "origin_class_id": "station", "relate_class_id": "point_ee", "flag": true}, 
	{"id": "rel_device_ee_point_ee", "name": "点安装设备信息", "origin_class_id": "point_ee", "relate_class_id": "device_ee", "flag": true}
]');
-- 创建能源监测相关属性
SELECT dml_info_insert('[
	{"class_id":"point_ee","id":"rated_votlage","name":"额定电压"},
	{"class_id":"point_ee","id":"rated_current","name":"额定电流"},
	{"class_id":"point_ee","id":"heigh","name":"高度单元"},

	{"class_id":"station","id":"contents","name":"一次系统图内容"},
	{"class_id":"station","id":"title","name":"一次系统图名称"},

	{"class_id":"device_ee","id":"type","name":"设备种类"}
]');
*/
-- 新增电能分类,并创建对应实例与信息表
SELECT ddl_class_create ('[
	{"id":"station","name":"能源监测站信息"},
	{"id":"point","name":"电能监测点(位置)"},	
	{"id":"device","name":"能源设备"},
	{"id":"tag","name":"监测量"}
]');
-- 创建能源监测对应关系,
-- 关系n:1
SELECT ddl_class_relation_create('[
	{"id": "rel_organiz_station", "name": "企业所属站信息", "origin_class_id": "organiz", "relate_class_id": "station", "flag": false}, 
	{"id": "rel_station_point", "name": "站监测点信息", "origin_class_id": "station", "relate_class_id": "point", "flag": false}, 
	{"id": "rel_point_device", "name": "点安装设备信息", "origin_class_id": "point", "relate_class_id": "device", "flag": true}
]');

-- 创建能源监测相关属性
-- TRUNCATE TABLE ci_tag CASCADE;
SELECT dml_info_insert('[
	{"class_id":"station","id":"heigh","name":"模数单元"},
	{"class_id":"station","id":"contents","name":"一次系统图内容"},
	{"class_id":"station","id":"title","name":"一次系统图名称"},

	{"class_id":"device","id":"type","name":"设备种类"},
	{"class_id":"device","id":"rated_votlage","name":"额定电压"},
	{"class_id":"device","id":"rated_current","name":"额定电流"}
]');

-- 创建监测量tag
SELECT dml_ci_insert ('[
	{"class_id":"tag","id":"vt","name":"失压阈值 voltage loss threshold value"},
	{"class_id":"tag","id":"vs","name":"失压状态 voltage loss status"},

	{"class_id":"tag","id":"gcd","name":"当日发电量 daily generating capacity"},
	{"class_id":"tag","id":"gc","name":"总发电量 generating capacity"},
	{"class_id":"tag","id":"gtd","name":"当日发电时间 daily generating time"},
	{"class_id":"tag","id":"gt","name":"总发电时间 generating time"},

	{"class_id":"tag","id":"pt","name":"电压变比"},
	{"class_id":"tag","id":"ct","name":"电流变比"},
	{"class_id":"tag","id":"sw","name":"开关量 switch status"},
	{"class_id":"tag","id":"t","name":"温度 temperature"},
	{"class_id":"tag","id":"h","name":"湿度 humidity"},
	{"class_id":"tag","id":"f","name":"频率 frequency"},
	{"class_id":"tag","id":"baud","name":"波特率 baud rate"},
	{"class_id":"tag","id":"pulse","name":"脉冲常数 pulse constant"},
	{"class_id":"tag","id":"soe","name":"事件代码"},

	{"class_id":"tag","id":"u","name":"电压"},
	{"class_id":"tag","id":"i","name":"电流"},
	{"class_id":"tag","id":"r","name":"电阻"},
	{"class_id":"tag","id":"n","name":"压力"},
	{"class_id":"tag","id":"v","name":"速度"},
	{"class_id":"tag","id":"time","name":"时间"},

	{"class_id":"tag","id":"ir","name":"R相电流值"},
	{"class_id":"tag","id":"is","name":"S相电流值"},
	{"class_id":"tag","id":"it","name":"T相电流值"},
	{"class_id":"tag","id":"ur","name":"R相电压值"},
	{"class_id":"tag","id":"us","name":"S相电压值"},
	{"class_id":"tag","id":"ut","name":"T相电压值"},
	{"class_id":"tag","id":"fr","name":"R相频率"},
	{"class_id":"tag","id":"fs","name":"S相频率"},
	{"class_id":"tag","id":"ft","name":"T相频率"},

	{"class_id":"tag","id":"uu","name":"市电电压上限"},
	{"class_id":"tag","id":"ul","name":"市电电压下限"},
	{"class_id":"tag","id":"fu","name":"市电频率上限"},
	{"class_id":"tag","id":"fl","name":"市电频率下限"},

	{"class_id":"tag","id":"ia","name":"A相电流值"},
	{"class_id":"tag","id":"ib","name":"B相电流值"},
	{"class_id":"tag","id":"ic","name":"C相电流值"},
	{"class_id":"tag","id":"ua","name":"A相电压值"},
	{"class_id":"tag","id":"ub","name":"B相电压值"},
	{"class_id":"tag","id":"uc","name":"C相电压值"},
	{"class_id":"tag","id":"uab","name":"A-B线电压值"},
	{"class_id":"tag","id":"ubc","name":"B-C线电压值"},
	{"class_id":"tag","id":"uac","name":"A-C线电压值"},

	{"class_id":"tag","id":"pf","name":"总功率因素"},
	{"class_id":"tag","id":"pfa","name":"A相功率因素"},
	{"class_id":"tag","id":"pfb","name":"B相功率因素"},
	{"class_id":"tag","id":"pfc","name":"C相功率因素"},

	{"class_id":"tag","id":"p","name":"总有功功率"},
	{"class_id":"tag","id":"pa","name":"A相有功功率"},
	{"class_id":"tag","id":"pb","name":"B相有功功率"},
	{"class_id":"tag","id":"pc","name":"C相有功功率"},

	{"class_id":"tag","id":"q","name":"总无功功率"},
	{"class_id":"tag","id":"qa","name":"A相无功功率"},
	{"class_id":"tag","id":"qb","name":"B相无功功率"},
	{"class_id":"tag","id":"qc","name":"C相无功功率"},

	{"class_id":"tag","id":"s","name":"总视在功率"},
	{"class_id":"tag","id":"sa","name":"A相视在功率"},
	{"class_id":"tag","id":"sb","name":"B相视在功率"},
	{"class_id":"tag","id":"sc","name":"C相视在功率"},

	{"class_id":"tag","id":"wa","name":"A相有功电能"},
	{"class_id":"tag","id":"wb","name":"B相有功电能"},
	{"class_id":"tag","id":"wc","name":"C相有功电能"},

	{"class_id":"tag","id":"ae","name":"有功总电能 active energy"},
	{"class_id":"tag","id":"ate","name":"有功尖电能 active top energy"},
	{"class_id":"tag","id":"ape","name":"有功峰电能 active peak energy"},
	{"class_id":"tag","id":"afe","name":"有功平电能 active plain energy"},
	{"class_id":"tag","id":"ave","name":"有功谷电能 active valley energy"},

	{"class_id":"tag","id":"pae","name":"正向有功总电能 positive active energy"},
	{"class_id":"tag","id":"pate","name":"正向有功尖电能 positive active top energy"},
	{"class_id":"tag","id":"pape","name":"正向有功峰电能 positive active peak energy"},
	{"class_id":"tag","id":"pafe","name":"正向有功平电能 positive active plain energy"},
	{"class_id":"tag","id":"pave","name":"正向有功谷电能 positive active valley energy"},

	{"class_id":"tag","id":"nae","name":"反向有功总电能 negative active energy"},
	{"class_id":"tag","id":"nate","name":"反向有功尖电能 negative active top energy"},
	{"class_id":"tag","id":"nape","name":"反向有功峰电能 negative active peak energy"},
	{"class_id":"tag","id":"nafe","name":"反向有功平电能 negative active plain energy"},
	{"class_id":"tag","id":"nave","name":"反向有功谷电能 negative active valley energy"},

	{"class_id":"tag","id":"re","name":"无功总电能 reactive energy"},
	{"class_id":"tag","id":"rte","name":"无功尖电能 reactive top energy"},
	{"class_id":"tag","id":"rpe","name":"无功峰电能 reactive peak energy"},
	{"class_id":"tag","id":"rfe","name":"无功平电能 reactive plain energy"},
	{"class_id":"tag","id":"rve","name":"无功谷电能 reactive valley energy"},

	{"class_id":"tag","id":"pre","name":"正向无功总电能 positive reactive energy"},
	{"class_id":"tag","id":"prte","name":"正向无功尖电能 positive reactive top energy"},
	{"class_id":"tag","id":"prpe","name":"正向无功峰电能 positive reactive peak energy"},
	{"class_id":"tag","id":"prfe","name":"正向无功平电能 positive reactive plain energy"},
	{"class_id":"tag","id":"prve","name":"正向无功谷电能 positive reactive valley energy"},

	{"class_id":"tag","id":"nre","name":"反向无功总电能 negative reactive energy"},
	{"class_id":"tag","id":"nrte","name":"反向无功尖电能 negative reactive top energy"},
	{"class_id":"tag","id":"nrpe","name":"反向无功峰电能 negative reactive peak energy"},
	{"class_id":"tag","id":"nrfe","name":"反向无功平电能 negative reactive plain energy"},
	{"class_id":"tag","id":"nrve","name":"反向无功谷电能 negative reactive valley energy"},

	{"class_id":"tag","id":"w1","name":"1月电能"},
	{"class_id":"tag","id":"w2","name":"2月电能"},
	{"class_id":"tag","id":"w3","name":"3月电能"},
	{"class_id":"tag","id":"w4","name":"4月电能"},
	{"class_id":"tag","id":"w5","name":"5月电能"},
	{"class_id":"tag","id":"w6","name":"6月电能"},
	{"class_id":"tag","id":"w7","name":"7月电能"},
	{"class_id":"tag","id":"w8","name":"8月电能"},
	{"class_id":"tag","id":"w9","name":"9月电能"},
	{"class_id":"tag","id":"w10","name":"10月电能"},
	{"class_id":"tag","id":"w11","name":"11月电能"},
	{"class_id":"tag","id":"w12","name":"12月电能"},

	{"class_id":"tag","id":"upv1","name":"第1路PV电压"},
	{"class_id":"tag","id":"upv2","name":"第2路PV电压"},
	{"class_id":"tag","id":"upv3","name":"第3路PV电压"},
	{"class_id":"tag","id":"upv4","name":"第4路PV电压"},
	{"class_id":"tag","id":"ipv1","name":"第1路PV电流"},
	{"class_id":"tag","id":"ipv2","name":"第2路PV电流"},
	{"class_id":"tag","id":"ipv3","name":"第3路PV电流"},
	{"class_id":"tag","id":"ipv4","name":"第4路PV电流"}
]');



