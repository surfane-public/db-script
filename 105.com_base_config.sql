/*

-- 数据库  :平台通用
-- 脚本类型:系统运行基础配置脚本

-- 创建信息 --
-- 作者    :杨永红
-- 版本    :V 1.0
-- 时间    :2017-05-10

-- 修订信息 --
-- 修订者  :
-- 版本    :
-- 时间    :
-- 修订概要:

*/

-- clear data
TRUNCATE TABLE sys_void CASCADE;
/*
	system info configuation;
*/

-- 函数执行结果状态信息
-- TRUNCATE TABLE sys_function_state;
-- COPY sys_function_state FROM 'd:/test/function_state.csv' DELIMITER as ',' csv header QUOTE as '"'
-- 创建函数路由信息
-- TRUNCATE TABLE sys_function_router;
-- COPY sys_function_router FROM 'd:/test/function_router.csv' DELIMITER as ',' csv header QUOTE as '"'

-- 函数执行结果状态信息
-- TRUNCATE TABLE sys_function_state;
SELECT dml_function_state_insert('[
	{"state":"00000","text":"successful completion","flag":"true"},
	
	{"state":"U0100","text":"config item not exists"},
	{"state":"U0101","text":"config item exists"},
	{"state":"U0102","text":"config item duplicate"},
	{"state":"U0103","text":"config item uid and id mismatch"},
	{"state":"U0104","text":"config item parent or preview can not be itself"},
	
	{"state":"U0200","text":"account exists"},
	{"state":"U0201","text":"account not exists"},	
	{"state":"U0202","text":"account duplicate"},
	{"state":"U0203","text":"password not exists"},	
	{"state":"U0204","text":"password duplicate"},
	{"state":"U0205","text":"account mismatch password"},
	{"state":"U0206","text":"account mismatch birthday"}
]');

-- 创建函数路由信息
-- TRUNCATE TABLE sys_function_router;
SELECT dml_function_router_insert('[
	{"id":1410,"function":"dml_function_state_select()","name":"test","remark":"测试"},
	{"id":11,"function":"func_account_register(%2$s)","name":"user account register","remark":"帐号注册,创建帐号密码等信息"},
	{"id":13,"function":"func_account_init(%2$s)","name":"user account init","remark":"帐号密码初始化,默认为8位生日数字"},
	{"id":15,"function":"func_account_cancel(%2$s)","name":"user account cancel","remark":"帐号标识为注消,不删除帐号信息"},
	{"id":17,"function":"func_account_favourite_add(%1$s,%2$s)","name":"add user favourite","remark":"添加用户桌面快捷方式,收藏内容"},
	{"id":19,"function":"func_account_favourite_remove(%1$s,%2$s)","name":"delete user favourite","remark":"删除用户桌面快捷方式,收藏内容"},

	{"id":12,"function":"func_account_logout(%1$s)","name":"user account logout","remark":"登出处理"},	
	{"id":14,"function":"func_account_authentication(%2$s)","name":"user account login authentication","remark":"登入验证"},
	{"id":16,"function":"func_account_menu(%1$s,%2$s)","name":"get user menu","remark":"获取用户角色的菜单"},	
	{"id":18,"function":"func_account_favourite(%1$s)","name":"get user favourite","remark":"获取用户桌面快捷方式,收藏内容"},	
	{"id":20,"function":"func_get_class_layout(%2$s)","name":"获取类型布局","remark":"获取类型布局层次"},
	{"id":22,"function":"func_get_class_info(%2$s)","name":"获取类型布局属性","remark":"获取类型布局属性信息"},
	{"id":24,"function":"func_get_class_infoset(%2$s)","name":"获取类型布局属性及约束","remark":"获取类型布局属性及约束信息"},
	{"id":26,"function":"func_get_class_relation(%2$s)","name":"获取类型关联关系","remark":"获取类型关联关系信息"},
	{"id":28,"function":"func_get_ci_layout(%2$s)","name":"获取实例布局","remark":"根据类型获取实例布局"},
	{"id":30,"function":"func_get_ci_info(%2$s)","name":"获取实例详细信息","remark":"根据类型获取实例详细信息"},
	{"id":32,"function":"func_get_ci_relation(%2$s)","name":"获取实例关联关系","remark":"根据类型获取实例关联关系"},	
	
	{"id":100,"function":"dml_function_router_select()","name":"查询函数路由表","remark":"查询函数路由表"},
	{"id":102,"function":"dml_function_state_select()","name":"查询函数执行结果状态","remark":"获取函数执行结果状态"},
	
	{"id":101,"function":"ddl_ci_table_create()","name":"创建配置项表","remark":"创建配置项数据库表,约束,规则"},
	{"id":103,"function":"ddl_ci_info_table_create()","name":"创建配置项属性表","remark":"创建配置项属性表,约束,规则"},
	{"id":105,"function":"ddl_ci_relation_table_create()","name":"创建配置项关联表","remark":"创建配置项关联表,约束,规则"},	
	{"id":107,"function":"ddl_parent_check_rule_alter()","name":"修改约束与规则","remark":"修改配置项及属性信息表约束与规则"},	

	{"id":111,"function":"ddl_class_drop(%2$s)","name":"删除分类及表结构","remark":"删除分类及表结构"},	
	{"id":113,"function":"ddl_class_create(%2$s)","name":"新增分类及表结构","remark":"新增分类及表结构"},	
	{"id":115,"function":"ddl_class_alter(%2$s)","name":"更新分类及表结构","remark":"更新分类及表结构"},	
	{"id":117,"function":"ddl_class_relation_drop(%2$s)","name":"删除分类关联关系及表结构","remark":"删除分类关联关系及表结构"},	
	{"id":119,"function":"ddl_class_relation_create(%2$s)","name":"新增分类关联关系及表结构","remark":"新增分类关联关系及表结构"},		
	
	{"id":131,"function":"dml_function_router_delete(%2$s)","name":"删除函数路由信息","remark":"删除函数路由信息,结果代码"},
	{"id":133,"function":"dml_function_state_delete(%2$s)","name":"删除函数执行结果信息","remark":"删除函数执行结果信息,结果代码"},
	{"id":135,"function":"dml_ci_delete(%2$s)","name":"删除配置项","remark":"删除分类,属性,属性约束,分类关联关系,实例配置信息"},
	{"id":143,"function":"dml_ci_info_delete(%2$s)","name":"删除配置项属性","remark":"删除配置项属性"},
	{"id":145,"function":"dml_ci_relation_delete(%2$s)","name":"删除配置项关联关系","remark":"删除配置项关联关系"},	

	{"id":151,"function":"dml_function_router_insert(%2$s)","name":"新增函数路由信息","remark":"新增函数路由信息,结果代码"},
	{"id":153,"function":"dml_function_state_insert(%2$s)","name":"新增函数执行结果信息","remark":"新增函数执行结果信息,结果代码"},	
	{"id":155,"function":"dml_class_insert(%2$s)","name":"新增分类","remark":"新增分类,包含对应表"},	
	{"id":157,"function":"dml_info_insert(%2$s)","name":"新增属性配置","remark":"新增属性配置"},
	{"id":159,"function":"dml_infoset_insert(%2$s)","name":"新增属性集配置","remark":"新增属性集配置"},	
	{"id":161,"function":"dml_ci_insert(%2$s)","name":"新增配置项","remark":"新增配置项"},
	{"id":163,"function":"dml_ci_info_insert(%2$s)","name":"新增配置项属性","remark":"新增配置项属性"},
	{"id":165,"function":"dml_ci_relation_insert(%2$s)","name":"新增配置项关联关系","remark":"删除配置项关联关系"},	
	{"id":167,"function":"dml_class_relation_insert(%2$s)","name":"新增类型关联关系","remark":"新增类型关联关系"},	

	{"id":171,"function":"dml_function_router_update(%2$s)","name":"更新函数路由信息","remark":"更新函数路由信息,结果代码"},
	{"id":173,"function":"dml_function_state_update(%2$s)","name":"更新函数执行结果信息","remark":"更新函数执行结果信息,结果代码"},	
	{"id":175,"function":"dml_class_update(%2$s)","name":"更新分类","remark":"更新分类,包含对应表"},
	{"id":177,"function":"dml_info_update(%2$s)","name":"更新属性配置","remark":"更新属性配置"},
	{"id":179,"function":"dml_infoset_update(%2$s)","name":"更新属性集配置","remark":"更新属性集配置"},
	{"id":181,"function":"dml_ci_update(%2$s)","name":"更新配置项","remark":"更新配置项"},
	{"id":183,"function":"dml_ci_info_update(%2$s)","name":"更新配置项属性","remark":"更新配置项属性"},
	
	{"id":191,"function":"func_replace_ci_info(%2$s)","name":"替换实例属性","remark":"配置项新属性,清空配置项原有属性"},
	
	{"id":1008,"function":"func_get_organiz()","name":"获取组织机构","remark":"获取组织机构"},
	{"id":1010,"function":"func_get_station(%1$s,%2$s)","input":{"uid":"必需"},"name":"获取变电站","remark":"获取变电站"},
	{"id":1012,"function":"func_get_station_layout(%2$s)","input":{"uid":"必需"},"name":"获取变电站点位布局","remark":"获取变电站点位布局"},	
	{"id":1014,"function":"func_get_station_device(%2$s)","input":{"uid":"必需"},"name":"获取变电站设备","remark":"获取变电站设备"},
	{"id":1016,"function":"func_get_tag()","input":{},"name":"获取监测量","remark":"获取监测量"},
	{"id":1018,"function":"func_get_station_tree(%2$s)","input":{"uid":"必需"},"name":"获取变电站点位树形结构","remark":"获取变电站点位树形结构"}
]');

-- system class dml_class_insert
-- TRUNCATE table sys_class CASCADE
SELECT ddl_class_create ('[
		{"id":"account","name":"帐号"},
		{"id":"role","name":"角色"},
		{"id":"organiz","name":"组织机构"},
		{"id":"ui","name":"用户界面"},
		{"id":"menu","name":"菜单"}
]');
/*
	system item relation table create;
  flag标识是否n:n对应关系,同时对应的实例关系表origin_uid与relate_uid是否为n:n
  true:origin_uid:relate_uid为n:n,UNIQUE(origin_uid,relate_uid)
  false:origin_uid:relate_uid为1:n,UNIQUE(relate_uid)
*/
-- role_ui
-- flag=true:编辑;flag=false:审核;false=null:只读
-- flow_ui
-- flag=true:编辑;flag=false:审核;false=null:通知
SELECT ddl_class_relation_create('[
	{"origin_id":"account","relate_id":"menu","id":"rel_account_menu","name":"帐号收藏信息","flag":"true"},
	{"origin_id":"account","relate_id":"role","id":"rel_account_role","name":"帐号角色信息","flag":"true"},
	{"origin_id":"account","relate_id":"organiz","id":"rel_account_organiz","name":"帐号组织信息","flag":"false"},
	{"origin_id":"role","relate_id":"ui","id":"rel_role_ui","name":"角色界面权限","flag":"true"},
	{"origin_id":"ui","relate_id":"menu","id":"rel_menu_ui","name":"菜单对应界面","flag":"true"}
]');
/*
	system item attribute configuration;
*/
-- account,ui,menu attribute
SELECT dml_info_insert ('[
		{"class_id":"account","id":"name","name":"姓名"},
		{"class_id":"account","id":"password","name":"密码","flag":"false"},
		{"class_id":"account","id":"birthday","name":"生日"},
		
		{"class_id":"ui","id":"page_url","name":"页面文件"},
		{"class_id":"ui","id":"page_class","name":"页面js类"},
		
		{"class_id":"menu","id":"target","name":"打开方式"},
		{"class_id":"menu","id":"hint","name":"提示"},
		{"class_id":"menu","id":"icon","name":"图标"},
		{"class_id":"menu","id":"shortcut_icon","name":"桌面图标"},
		{"class_id":"menu","id":"topic_icon","name":"分类图标"}
]');

/*
	system data;
*/
-- account item
SELECT dml_ci_insert ('[
	{"class_id":"account","id":"SysAdmin","name":"系统管理员帐号"}
]');

-- account item info
SELECT dml_ci_info_insert('[
	{"id":"SysAdmin","info_id":"password","info_value":"SysAdmin"},
	{"id":"SysAdmin","info_id":"birthday","info_value":"2001-01-01"},
	{"id":"SysAdmin","info_id":"name","info_value":"系统管理员姓名"}
]');

-- role item
SELECT dml_ci_insert ('[
	{"class_id":"role","id":"role_system_administrator","name":"系统管理员"}
]');

-- account role relation
SELECT dml_ci_relation_insert ('[
	{"origin_id":"SysAdmin","relate_id":"role_system_administrator"}
]');

-- organiz item
SELECT dml_ci_insert ('[
	{"class_id":"organiz","id":"sz-zhpr","name":"苏州中恒普瑞能源互联网科技有限公司"},
	{"class_id":"organiz","id":"sz-zhpr-admin","name":"管理员","parent_id":"sz-zhpr"},
	{"class_id":"organiz","id":"sz-zhpr-RD","name":"研发部","parent_id":"sz-zhpr"},
	{"class_id":"organiz","id":"sz-zhpr-Operation","name":"运行部","parent_id":"sz-zhpr"},	
	{"class_id":"organiz","id":"sz-zhpr-service","name":"客服部","parent_id":"sz-zhpr"},
	{"class_id":"organiz","id":"sz-zhpr-Engineering","name":"工程部","parent_id":"sz-zhpr"}
]');

-- account organiz relation
SELECT dml_ci_relation_insert ('[
	{"origin_id":"SysAdmin","relate_id":"sz-zhpr"}
]');

-- ui item
SELECT dml_ci_insert ('[
	{"class_id":"ui","id":"ui_system_class_manage","name":"分类管理界面"},		
	{"class_id":"ui","id":"ui_system_class_info_manage","name":"分类属性管理界面"},		
	{"class_id":"ui","id":"ui_system_class_relation_manage","name":"分类关系管理界面"},
	{"class_id":"ui","id":"ui_system_ci_manage","name":"实例管理界面"},
	{"class_id":"ui","id":"ui_system_ci_relation_manage","name":"实例关系管理界面"}
]');

-- ui item info
SELECT dml_ci_info_insert('[
	{"id":"ui_system_class_manage","info_id":"page_url","info_value":"page/classManagement.html"},
	{"id":"ui_system_class_manage","info_id":"page_class","info_value":"classManagement"},
	{"id":"ui_system_class_manage","info_id":"target","info_value":"div"},
	
	{"id":"ui_system_class_info_manage","info_id":"page_url","info_value":"page/classInfoManagement.html"},
	{"id":"ui_system_class_info_manage","info_id":"page_class","info_value":"classInfoManagement"},
	{"id":"ui_system_class_info_manage","info_id":"target","info_value":"div"},
	
	{"id":"ui_system_class_relation_manage","info_id":"page_url","info_value":"page/classRelationManagement.html"},
	{"id":"ui_system_class_relation_manage","info_id":"page_class","info_value":"classRelationManagement"},
	{"id":"ui_system_class_relation_manage","info_id":"target","info_value":"div"},
	
	{"id":"ui_system_ci_manage","info_id":"page_url","info_value":"page/ciManagement.html"},
	{"id":"ui_system_ci_manage","info_id":"page_class","info_value":"ciManagement"},
	{"id":"ui_system_ci_manage","info_id":"target","info_value":"div"},
	
	{"id":"ui_system_ci_relation_manage","info_id":"page_url","info_value":"page/ciRelationManagement.html"},
	{"id":"ui_system_ci_relation_manage","info_id":"page_class","info_value":"ciRelationManagement"},
	{"id":"ui_system_ci_relation_manage","info_id":"target","info_value":"div"}
]');
	
-- menu item
SELECT dml_ci_insert ('[	
	{"class_id":"menu","id":"menu_portal","name":"平台菜单"},
	{"class_id":"menu","parent_id":"menu_portal","id":"menu_system_config","name":"系统运行配置"},
	{"class_id":"menu","parent_id":"menu_system_config","id":"menu_system_class_manage","name":"分类管理"},
	{"class_id":"menu","parent_id":"menu_system_config","id":"menu_system_class_info_manage","name":"分类属性管理","pre_id":"menu_system_class_manage"},		
	{"class_id":"menu","parent_id":"menu_system_config","id":"menu_system_class_relation_manage","name":"分类关系管理","pre_id":"menu_system_class_info_manage"},
	{"class_id":"menu","parent_id":"menu_system_config","id":"menu_system_ci_manage","name":"实例管理","pre_id":"menu_system_class_relation_manage"},
	{"class_id":"menu","parent_id":"menu_system_config","id":"menu_system_ci_relation_manage","name":"实例关系管理","pre_id":"menu_system_ci_manage"}
]');

-- menu item info
SELECT dml_ci_info_insert('[
	{"id":"menu_system_config","info_id":"hint","info_value":"系统配置"},
	{"id":"menu_system_config","info_id":"icon","info_value":"image/icons/logo_Diag.ico"},
	{"id":"menu_system_config","info_id":"shortcut_icon","info_value":"image/icons/logo_Diag.ico"},
	{"id":"menu_system_config","info_id":"topic_icon","info_value":"fa fa-cog"},
	
	{"id":"menu_system_class_manage","info_id":"hint","info_value":"分类管理"},
	{"id":"menu_system_class_manage","info_id":"icon","info_value":"image/icons/logo_Diag.ico"},
	{"id":"menu_system_class_manage","info_id":"shortcut_icon","info_value":"image/icons/logo_Diag.ico"},
	{"id":"menu_system_class_manage","info_id":"topic_icon","info_value":"fa fa-cog"},

	{"id":"menu_system_class_info_manage","info_id":"hint","info_value":"分类信息管理"},
	{"id":"menu_system_class_info_manage","info_id":"icon","info_value":"image/icons/logo_Diag.ico"},
	{"id":"menu_system_class_info_manage","info_id":"shortcut_icon","info_value":"image/icons/logo_Diag.ico"},
	{"id":"menu_system_class_info_manage","info_id":"topic_icon","info_value":"fa fa-cog"},
	
	{"id":"menu_system_class_relation_manage","info_id":"hint","info_value":"分类关系管理"},
	{"id":"menu_system_class_relation_manage","info_id":"icon","info_value":"image/icons/logo_Diag.ico"},
	{"id":"menu_system_class_relation_manage","info_id":"shortcut_icon","info_value":"image/icons/logo_Diag.ico"},
	{"id":"menu_system_class_relation_manage","info_id":"topic_icon","info_value":"fa fa-cog"},

	{"id":"menu_system_ci_manage","info_id":"hint","info_value":"实例管理"},
	{"id":"menu_system_ci_manage","info_id":"icon","info_value":"image/icons/logo_Diag.ico"},
	{"id":"menu_system_ci_manage","info_id":"shortcut_icon","info_value":"image/icons/logo_Diag.ico"},
	{"id":"menu_system_ci_manage","info_id":"topic_icon","info_value":"fa fa-cog"},

	{"id":"menu_system_ci_relation_manage","info_id":"hint","info_value":"实例关系管理"},
	{"id":"menu_system_ci_relation_manage","info_id":"icon","info_value":"image/icons/logo_Diag.ico"},
	{"id":"menu_system_ci_relation_manage","info_id":"shortcut_icon","info_value":"image/icons/logo_Diag.ico"},
	{"id":"menu_system_ci_relation_manage","info_id":"topic_icon","info_value":"fa fa-cog"}
]');

-- menu ui relation
SELECT dml_ci_relation_insert ('[
	{"relate_id":"menu_system_class_manage","origin_id":"ui_system_class_manage"},
	{"relate_id":"menu_system_class_info_manage","origin_id":"ui_system_class_info_manage"},
	{"relate_id":"menu_system_class_relation_manage","origin_id":"ui_system_class_relation_manage"},
	{"relate_id":"menu_system_ci_manage","origin_id":"ui_system_ci_manage"},
	{"relate_id":"menu_system_ci_relation_manage","origin_id":"ui_system_ci_relation_manage"}
]');

-- role ui relation
SELECT dml_ci_relation_insert ('[
	{"origin_id":"role_system_administrator","relate_id":"ui_system_class_manage"},	
	{"origin_id":"role_system_administrator","relate_id":"ui_system_class_info_manage"},
	{"origin_id":"role_system_administrator","relate_id":"ui_system_class_relation_manage"},
	{"origin_id":"role_system_administrator","relate_id":"ui_system_ci_manage"},
	{"origin_id":"role_system_administrator","relate_id":"ui_system_ci_relation_manage"}
]');

/*
-- 1.export sys_function_state
COPY(
	SELECT * FROM sys_function_state ORDER BY result_state
	)TO 'D:/myApp/script/eCloud/public/system_config/01.sys_function_state.csv'
	WITH (FORMAT csv,HEADER TRUE,QUOTE '"',DELIMITER ',',ENCODING 'gbk');

-- 2.export sys_function_router
COPY(
	SELECT * FROM sys_function_router ORDER BY id
	)TO 'D:/myApp/script/eCloud/public/system_config/02.sys_function_router.csv'
	WITH (FORMAT csv,HEADER TRUE,QUOTE '"',DELIMITER ',',ENCODING 'gbk');

-- 3.export sys_class_config
COPY(
	SELECT par.id AS parent_id,pre.id AS pre_id
			 , rel.id,rel.name,rel.flag
			 , inf.id AS info_id,inf.name AS info_name,inf.flag AS info_flag
			 , infset.id AS infoset_id,infset.name AS infoset_name,infset.flag AS infoset_flag,infset.info_value AS info_value
		FROM sys_class AS rel
		LEFT JOIN sys_class AS par ON rel.parent_uid = par.uid 
		LEFT JOIN sys_class AS pre ON rel.pre_uid = pre.uid
		LEFT JOIN sys_info AS inf ON rel.uid = inf.class_uid
		LEFT JOIN sys_infoset AS infset ON inf.uid = infset.info_uid
	 ORDER BY rel.id,inf.id,infset.id
	)TO 'D:/myApp/script/eCloud/public/system_config/03.sys_class_config.csv'
	WITH (FORMAT csv,HEADER TRUE,QUOTE '"',DELIMITER ',',ENCODING 'gbk');

-- 4.export sys_class_relation
COPY(
	SELECT rel.id,rel.name,rel.flag,clso.id AS origin_class_id,clsr.id AS relate_class_id
		FROM sys_class_relation AS rel
	 INNER JOIN sys_class AS clso ON rel.origin_class_uid = clso.uid 
	 INNER JOIN sys_class AS clsr ON rel.relate_class_uid = clsr.uid 
	 ORDER BY rel.id
	)TO 'D:/myApp/script/eCloud/public/system_config/04.sys_class_relation.csv'
	WITH (FORMAT csv,HEADER TRUE,QUOTE '"',DELIMITER ',',ENCODING 'gbk');

-- 5.export sys_item_config
COPY(
	SELECT cls.id AS class_id,par.id AS parent_id,pre.id AS pre_id
			 , rel.id,rel.name,rel.flag
			 , inf.id AS info_id,infset.id AS infoset_id,relinf.info_value
		FROM sys_item AS rel
	 INNER JOIN sys_class AS cls ON rel.class_uid = cls.uid
		LEFT JOIN sys_item AS par ON rel.parent_uid = par.uid 
		LEFT JOIN sys_item AS pre ON rel.pre_uid = pre.uid
	  LEFT JOIN sys_item_info AS relinf ON rel.uid = relinf.item_uid
		LEFT JOIN sys_info AS inf ON relinf.info_uid = inf.uid
		LEFT JOIN sys_infoset AS infset ON relinf.infoset_uid = infset.uid
	 ORDER BY cls.id,rel.id,inf.id,infset.id
	)TO 'D:/myApp/script/eCloud/public/system_config/05.sys_item_config.csv'
	WITH (FORMAT csv,HEADER TRUE,QUOTE '"',DELIMITER ',',ENCODING 'gbk');

-- 6.export sys_item_relation
COPY(
	SELECT cio.id AS origin_id,cir.id AS relate_id,rel.flag
		FROM sys_item_relation AS rel
	 INNER JOIN sys_class AS clso ON rel.origin_class_uid = clso.uid 
	 INNER JOIN sys_class AS clsr ON rel.relate_class_uid = clsr.uid 
	 INNER JOIN sys_item AS cio ON rel.origin_uid = cio.uid
	 INNER JOIN sys_item AS cir ON rel.relate_uid = cir.uid
	 ORDER BY clso.id,clsr.id,cio.id,cir.id
	)TO 'D:/myApp/script/eCloud/public/system_config/06.sys_item_relation.csv'
	WITH (FORMAT csv,HEADER TRUE,QUOTE '"',DELIMITER ',',ENCODING 'gbk');
*/