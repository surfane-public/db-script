-- ui item
SELECT dml_ci_insert ('[
	{"class_id":"ui","id":"ui_station_monitor","name":"变电站监测界面"},		
	{"class_id":"ui","id":"ui_station_point_monitor","name":"点位监测界面"},		
	{"class_id":"ui","id":"ui_station_device_monitor","name":"变压器监测界面"},
	{"class_id":"ui","id":"ui_station_chart_edit","name":"一次系统图编辑界面"},
	{"class_id":"ui","id":"ui_station_config","name":"工程站点配置"}
]');

-- ui item info
SELECT dml_ci_info_insert('[
	{"id":"ui_station_monitor","info_id":"page_url","info_value":"page/stationPoint.html"},
	{"id":"ui_station_monitor","info_id":"page_class","info_value":"stationPoint"},
	{"id":"ui_station_monitor","info_id":"target","info_value":"div"},
	
	{"id":"ui_station_point_monitor","info_id":"page_url","info_value":"page/lowVoltageCabinet.html"},
	{"id":"ui_station_point_monitor","info_id":"page_class","info_value":"lowVoltageCabinet"},
	{"id":"ui_station_point_monitor","info_id":"target","info_value":"div"},
	
	{"id":"ui_station_device_monitor","info_id":"page_url","info_value":"page/.html"},
	{"id":"ui_station_device_monitor","info_id":"page_class","info_value":"NA"},
	{"id":"ui_station_device_monitor","info_id":"target","info_value":"div"},

	{"id":"ui_station_chart_edit","info_id":"page_url","info_value":"draw2d/diagramEditor.html"},
	{"id":"ui_station_chart_edit","info_id":"page_class","info_value":"com.pr.views.DrawCanvas"},
	{"id":"ui_station_chart_edit","info_id":"target","info_value":"div"},

	{"id":"ui_station_config","info_id":"page_url","info_value":"page/stationConfig.html"},
	{"id":"ui_station_config","info_id":"page_class","info_value":"stationConfig"},
	{"id":"ui_station_config","info_id":"target","info_value":"div"}
]');
	
-- menu item
SELECT dml_ci_insert ('[
	{"class_id":"menu","parent_id":"menu_portal","id":"menu_energy_monitor","name":"能源监测"},
	{"class_id":"menu","parent_id":"menu_energy_monitor","id":"menu_station_monitor","name":"变电站监测"},
	{"class_id":"menu","parent_id":"menu_energy_monitor","id":"menu_station_point_monitor","name":"配电柜监测","pre_id":"menu_station_monitor"},
	{"class_id":"menu","parent_id":"menu_energy_monitor","id":"menu_station_device_monitor","name":"变压器监测","pre_id":"menu_station_point_monitor"},

	{"class_id":"menu","parent_id":"menu_portal","id":"menu_edit","name":"应用配置"},
	{"class_id":"menu","parent_id":"menu_edit","id":"menu_station_chart_edit","name":"一次系统图编辑"},
	{"class_id":"menu","parent_id":"menu_edit","id":"menu_stationConfig","name":"工程站点配置"}
]');

-- menu item info
SELECT dml_ci_info_insert('[
	{"id":"menu_energy_monitor","info_id":"hint","info_value":"能源监测"},
	{"id":"menu_energy_monitor","info_id":"icon","info_value":"image/icons/logo_Diag.ico"},
	{"id":"menu_energy_monitor","info_id":"shortcut_icon","info_value":"image/icons/logo_Diag.ico"},
	{"id":"menu_energy_monitor","info_id":"topic_icon","info_value":"fa fa-cog"},
	
	{"id":"menu_station_monitor","info_id":"hint","info_value":"变电站监测"},
	{"id":"menu_station_monitor","info_id":"icon","info_value":"image/icons/logo_Diag.ico"},
	{"id":"menu_station_monitor","info_id":"shortcut_icon","info_value":"image/icons/logo_Diag.ico"},
	{"id":"menu_station_monitor","info_id":"topic_icon","info_value":"fa fa-cog"},

	{"id":"menu_station_point_monitor","info_id":"hint","info_value":"配电柜监测"},
	{"id":"menu_station_point_monitor","info_id":"icon","info_value":"image/icons/logo_Diag.ico"},
	{"id":"menu_station_point_monitor","info_id":"shortcut_icon","info_value":"image/icons/logo_Diag.ico"},
	{"id":"menu_station_point_monitor","info_id":"topic_icon","info_value":"fa fa-cog"},
	
	{"id":"menu_station_device_monitor","info_id":"hint","info_value":"变压器监测"},
	{"id":"menu_station_device_monitor","info_id":"icon","info_value":"image/icons/logo_Diag.ico"},
	{"id":"menu_station_device_monitor","info_id":"shortcut_icon","info_value":"image/icons/logo_Diag.ico"},
	{"id":"menu_station_device_monitor","info_id":"topic_icon","info_value":"fa fa-cog"},

	{"id":"menu_station_chart_edit","info_id":"hint","info_value":"一次系统图编辑"},
	{"id":"menu_station_chart_edit","info_id":"icon","info_value":"image/icons/logo_Diag.ico"},
	{"id":"menu_station_chart_edit","info_id":"shortcut_icon","info_value":"image/icons/logo_Diag.ico"},
	{"id":"menu_station_chart_edit","info_id":"topic_icon","info_value":"fa fa-cog"},
	
	{"id":"menu_stationConfig","info_id":"hint","info_value":"工程站点配置"},
	{"id":"menu_stationConfig","info_id":"icon","info_value":"image/icons/logo_Diag.ico"},
	{"id":"menu_stationConfig","info_id":"shortcut_icon","info_value":"image/icons/logo_Diag.ico"},
	{"id":"menu_stationConfig","info_id":"topic_icon","info_value":"fa fa-cog"}
]');

-- menu ui relation
SELECT dml_ci_relation_insert ('[
	{"relate_id":"menu_station_monitor","origin_id":"ui_station_monitor"},
	{"relate_id":"menu_station_point_monitor","origin_id":"ui_station_point_monitor"},
	{"relate_id":"menu_station_device_monitor","origin_id":"ui_station_device_monitor"},
	{"relate_id":"menu_station_chart_edit","origin_id":"ui_station_chart_edit"},
	{"relate_id":"menu_stationConfig","origin_id":"ui_station_config"}
]');

-- role ui relation
SELECT dml_ci_relation_insert ('[
	{"origin_id":"role_system_administrator","relate_id":"ui_station_monitor"},	
	{"origin_id":"role_system_administrator","relate_id":"ui_station_point_monitor"},
	{"origin_id":"role_system_administrator","relate_id":"ui_station_device_monitor"},
	{"origin_id":"role_system_administrator","relate_id":"ui_station_chart_edit"},
	{"origin_id":"role_system_administrator","relate_id":"ui_station_config"}
]');