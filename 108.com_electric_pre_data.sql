TRUNCATE TABLE ci_station CASCADE;
TRUNCATE TABLE ci_point CASCADE;
TRUNCATE TABLE ci_device CASCADE;
-- 新增能源分类:苏州工业园区建屋发展集团有限公司D2配电房,451馈线柜布局
SELECT dml_ci_insert ('[
		{"class_id":"station", "id":"jw2.5d2","name":"尚高科技有限公司20KV变电站"},
		{"class_id":"device",  "id":"jw2.5d2#5","name":"5#变压器"},

		{"class_id":"point","id":"jw2.5d2#5_hv","name":"5#配电室高压柜"},	
		{"class_id":"point","id":"jw2.5d2_211","name":"211进线柜",       "parent_id":"jw2.5d2#5_hv"},	
		{"class_id":"point","id":"jw2.5d2_213","name":"213主变柜",       "parent_id":"jw2.5d2#5_hv"},	
		
		{"class_id":"point","id":"jw2.5d2_211-1","name":"211进线开关",   "parent_id":"jw2.5d2_211"},		
		{"class_id":"point","id":"jw2.5d2_213-1","name":"213#5主变开关", "parent_id":"jw2.5d2_213"},
		
		{"class_id":"point","id":"jw2.5d2#5_lv","name":"5#配电室低压柜", "pre_id":"jw2.5d2#5_hv"},
		
		{"class_id":"point","id":"jw2.5d2_451#5","name":"451#5进线柜",                   "parent_id":"jw2.5d2#5_lv"},
		{"class_id":"point","id":"jw2.5d2_T5-2","name":"T5-2AA电容补偿柜",               "parent_id":"jw2.5d2#5_lv","pre_id":"jw2.5d2_451#5"},
		{"class_id":"point","id":"jw2.5d2_T5-3","name":"T5-3AA电容补偿柜",               "parent_id":"jw2.5d2#5_lv","pre_id":"jw2.5d2_T5-2"},
		{"class_id":"point","id":"jw2.5d2_T5-4","name":"T5-4AA动力分计量柜",             "parent_id":"jw2.5d2#5_lv","pre_id":"jw2.5d2_T5-3"},
		{"class_id":"point","id":"jw2.5d2_T5-5","name":"T5-5AA出线柜",                   "parent_id":"jw2.5d2#5_lv","pre_id":"jw2.5d2_T5-4"},
		{"class_id":"point","id":"jw2.5d2_T5-6","name":"T5-6AA母联柜",                   "parent_id":"jw2.5d2#5_lv","pre_id":"jw2.5d2_T5-5"},
		
		{"class_id":"point","id":"jw2.5d2_451#5-1","name":"451#5进线开关",               "parent_id":"jw2.5d2_451#5"},		
		{"class_id":"point","id":"jw2.5d2_T5-2-1","name":"452电容开关",                  "parent_id":"jw2.5d2_T5-2"},
		{"class_id":"point","id":"jw2.5d2_T5-3-1","name":"453电容开关",                  "parent_id":"jw2.5d2_T5-3"},
		
		{"class_id":"point","id":"jw2.5d2_T5-4-1","name":"454-1 备用",                   "parent_id":"jw2.5d2_T5-4"},
		{"class_id":"point","id":"jw2.5d2_T5-4-2","name":"454-2 雨水处理泵房电控箱",     "parent_id":"jw2.5d2_T5-4","pre_id":"jw2.5d2_T5-4-1"},
		{"class_id":"point","id":"jw2.5d2_T5-4-3","name":"454-3 D.E.F路灯总箱",          "parent_id":"jw2.5d2_T5-4","pre_id":"jw2.5d2_T5-4-2"},
		{"class_id":"point","id":"jw2.5d2_T5-4-4","name":"454-4 应急动力电屏(主)",       "parent_id":"jw2.5d2_T5-4","pre_id":"jw2.5d2_T5-4-3"},
		
		{"class_id":"point","id":"jw2.5d2_T5-5-1","name":"455-1 D1栋空调电源",           "parent_id":"jw2.5d2_T5-5"},
		{"class_id":"point","id":"jw2.5d2_T5-5-2","name":"455-2 E2组团配套用房配电箱",   "parent_id":"jw2.5d2_T5-5","pre_id":"jw2.5d2_T5-5-1"},
		{"class_id":"point","id":"jw2.5d2_T5-5-3","name":"455-3 变电所电源",             "parent_id":"jw2.5d2_T5-5","pre_id":"jw2.5d2_T5-5-2"},
		{"class_id":"point","id":"jw2.5d2_T5-5-4","name":"455-4 E2组团配套用房配电箱",   "parent_id":"jw2.5d2_T5-5","pre_id":"jw2.5d2_T5-5-3"},
		{"class_id":"point","id":"jw2.5d2_T5-5-5","name":"455-5 应急照明配电箱",         "parent_id":"jw2.5d2_T5-5","pre_id":"jw2.5d2_T5-5-4"},
		
		{"class_id":"point","id":"jw2.5d2_T5-6-1","name":"456 联络开关",                 "parent_id":"jw2.5d2_T5-6"},
		{"class_id":"point","id":"jw2.5d2_T5-6-2","name":"456-1 G组团弱电及安保配电总箱","parent_id":"jw2.5d2_T5-6","pre_id":"jw2.5d2_T5-6-1"},
		{"class_id":"point","id":"jw2.5d2_T5-6-3","name":"456-2 备用",                   "parent_id":"jw2.5d2_T5-6","pre_id":"jw2.5d2_T5-6-2"},
		{"class_id":"point","id":"jw2.5d2_T5-6-4","name":"456-3 D/E组团地下室照明总箱",  "parent_id":"jw2.5d2_T5-6","pre_id":"jw2.5d2_T5-6-3"},
		{"class_id":"point","id":"jw2.5d2_T5-6-5","name":"456-4 D2组团配套用房配电箱",   "parent_id":"jw2.5d2_T5-6","pre_id":"jw2.5d2_T5-6-4"}
]');

-- 组织,站,设备对应关系
SELECT dml_ci_relation_insert('[
	{"origin_id":"sz-zhpr","relate_id":"jw2.5d2"},
	{"origin_id":"jw2.5d2","relate_id":"jw2.5d2#5_hv"},
	{"origin_id":"jw2.5d2","relate_id":"jw2.5d2#5_lv"},
	{"origin_id":"jw2.5d2#5_lv","relate_id":"jw2.5d2#5"}
]');

-- 属性
SELECT dml_ci_info_insert('[
	{"id":"jw2.5d2","info_id":"title","info_value":"尚高科技有限公司20KV变电站一次系统图"},
	{"id":"jw2.5d2","info_id":"contents","info_value":"[]"},
	
	{"id":"jw2.5d2#5","info_id":"rated_votlage","info_value":"110KV"},
	{"id":"jw2.5d2#5","info_id":"rated_current","info_value":"110A"},
	{"id":"jw2.5d2#5","info_id":"type","info_value":"干式变压器"}
]');

-- 新增能源分类:苏州工业园区建屋发展集团有限公司D2配电房,451馈线柜点位属性