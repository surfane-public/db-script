/*

-- 数据库  :平台通用
-- 脚本类型:DDL接口函数创建脚本

-- 创建信息 --
-- 作者    :surfane
-- 版本    :V 1.0
-- 时间    :2017-10-10

-- 修订信息 --
-- 修订者  :
-- 版本    :
-- 时间    :
-- 修订概要:

*/

-- 创建实例表
-- 创建后表名为:ci_[$1]
CREATE OR REPLACE FUNCTION "ddl_ci_table_create"(UUID)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_class_uid UUID := $1;
	v_class_flag BOOLEAN := TRUE;
	v_itemtable_name VARCHAR(40) := NULL;
	v_sqlcmd TEXT := NULL;
	v_result JSONB := NULL;
	BEGIN
		SELECT FORMAT('ci_%s',id),flag
			INTO v_itemtable_name,v_class_flag
			FROM sys_class
		 WHERE uid = v_class_uid; 
		IF NOT FOUND THEN
			SELECT dml_function_state_select('U0100'::CHAR(5),v_class_uid::VARCHAR) INTO v_result;
		ELSE
			v_sqlcmd := '	
				CREATE TABLE %1$s (
					CONSTRAINT pk_%1$s PRIMARY KEY (uid),
					CONSTRAINT uq_%1$s_id UNIQUE (class_uid,id),
					CONSTRAINT uq_%1$s_name UNIQUE (class_uid,name),
					CONSTRAINT fk_%1$s_class FOREIGN KEY (class_uid) REFERENCES sys_class (uid) ON DELETE CASCADE ON UPDATE CASCADE,
					CONSTRAINT fk_%1$s_parent FOREIGN KEY (parent_uid) REFERENCES %1$s (uid) ON DELETE CASCADE ON UPDATE CASCADE,
					CONSTRAINT fk_%1$s_pre FOREIGN KEY (pre_uid) REFERENCES %1$s (uid) ON DELETE SET NULL ON UPDATE CASCADE,
					CONSTRAINT chk_class CHECK (class_uid = %2$s::uuid)
				) INHERITS (sys_item)
				;				
				CREATE OR REPLACE RULE sys_item_insert_%1$s AS ON INSERT TO sys_item
				WHERE class_uid = %2$s
				DO INSTEAD INSERT INTO %1$s
				VALUES(NEW.uid, NEW.flag, NEW.id, NEW.name, NEW.parent_uid, NEW.pre_uid, NEW.class_uid)
				;
				';
			IF FALSE = v_class_flag THEN
				v_sqlcmd := v_sqlcmd || 'ALTER TABLE %1$s ALTER COLUMN "id" DROP NOT NULL;';
			END IF;
			v_sqlcmd := FORMAT(v_sqlcmd,v_itemtable_name,quote_literal(v_class_uid));
			EXECUTE v_sqlcmd;	
		END IF;		
		RETURN v_result;
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 创建实例属性表
-- 创建后表名为:info_[$1]
CREATE OR REPLACE FUNCTION "ddl_ci_info_table_create"(UUID)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_class_uid UUID := $1;
	v_infotable_name VARCHAR(40) := NULL;
	v_itemtable_name VARCHAR(40) := NULL;
	v_sqlcmd TEXT := NULL;
	v_result JSONB := NULL;
	BEGIN
		SELECT FORMAT('info_%s',id),FORMAT('ci_%s',id)
			INTO v_infotable_name,v_itemtable_name
			FROM sys_class
		 WHERE uid = v_class_uid;
		IF NOT FOUND THEN
			SELECT dml_function_state_select('U0100'::CHAR(5),v_class_uid::VARCHAR) INTO v_result;
		ELSE
			v_sqlcmd := '
				CREATE TABLE %1$s (
					CONSTRAINT pk_%1$s PRIMARY KEY ("uid"),
					CONSTRAINT uq_%1$s UNIQUE (item_uid, info_uid),
					CONSTRAINT fk_%1$s_item FOREIGN KEY (item_uid) REFERENCES %3$s (uid) ON DELETE CASCADE ON UPDATE CASCADE,
					CONSTRAINT fk_%1$s_info FOREIGN KEY (info_uid) REFERENCES sys_info (uid) ON DELETE CASCADE ON UPDATE CASCADE,
					CONSTRAINT fk_%1$s_infoset FOREIGN KEY (info_uid,infoset_uid) REFERENCES sys_infoset (info_uid,uid) ON DELETE SET NULL ON UPDATE CASCADE,
					CONSTRAINT chk_class CHECK (class_uid = %2$s::uuid)
				)	INHERITS (sys_item_info)
				;
				CREATE OR REPLACE RULE sys_item_info_insert_%1$s AS ON INSERT TO sys_item_info
				WHERE class_uid = %2$s 
				DO INSTEAD INSERT INTO %1$s
				VALUES(NEW.uid, NEW.flag, NEW.class_uid, NEW.item_uid, NEW.info_uid, NEW.infoset_uid, NEW.value)
				;
				';
			v_sqlcmd := FORMAT(v_sqlcmd,v_infotable_name,quote_literal(v_class_uid),v_itemtable_name);
			EXECUTE v_sqlcmd;	
		END IF;
		RETURN v_result;
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 创建实例关联表
-- 创建后表名为:rel_[$]_[$]
-- $3:关联是否为1:1关联,true:1:1,false:1:n
CREATE OR REPLACE FUNCTION "ddl_ci_relation_table_create"(UUID)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_relation_uid UUID := $1;
	v_relation_flag BOOLEAN := TRUE;
	v_reltable_name  VARCHAR(40) := NULL;
	v_origintable_name VARCHAR(40) := NULL;
	v_relatetable_name VARCHAR(40) := NULL;
	v_sqlcmd TEXT := NULL;
	v_result JSONB := NULL;
	BEGIN
 		SELECT FORMAT('rel_%s_%s',clso.id,clsr.id),FORMAT('ci_%s',clso.id),FORMAT('ci_%s',clsr.id),rel.flag
		  INTO v_reltable_name,v_origintable_name,v_relatetable_name,v_relation_flag
			FROM sys_class_relation AS rel
		 INNER JOIN sys_class AS clso ON rel.origin_uid = clso.uid
		 INNER JOIN sys_class AS clsr ON rel.relate_uid = clsr.uid
		 WHERE rel.uid = v_relation_uid;
		IF NOT FOUND THEN
			SELECT dml_function_state_select('U0100'::CHAR(5),v_relation_uid::VARCHAR) INTO v_result;
		ELSE	
			v_sqlcmd := '
				CREATE TABLE %1$s (
					CONSTRAINT pk_%1$s PRIMARY KEY (uid),
					CONSTRAINT uq_%1$s UNIQUE (origin_uid,relate_uid),
					CONSTRAINT fk_%1$s_uid FOREIGN KEY (relation_uid) REFERENCES sys_class_relation (uid) ON DELETE CASCADE ON UPDATE CASCADE,
					CONSTRAINT fk_%1$s_%3$s FOREIGN KEY (origin_uid) REFERENCES %3$s (uid) ON DELETE CASCADE ON UPDATE CASCADE,
					CONSTRAINT fk_%1$s_%4$s FOREIGN KEY (relate_uid) REFERENCES %4$s (uid) ON DELETE CASCADE ON UPDATE CASCADE,
					CONSTRAINT chk_class CHECK (relation_uid = %2$s::uuid)
				) INHERITS (sys_item_relation)
				;
				CREATE OR REPLACE RULE sys_item_relation_insert_%1$s AS ON INSERT TO sys_item_relation
				WHERE relation_uid = %2$s 
				DO INSTEAD INSERT INTO %1$s
				VALUES(NEW.uid, NEW.flag, NEW.relation_uid, NEW.origin_uid, NEW.relate_uid)
				;
				';
			IF FALSE = v_relation_flag THEN
				v_sqlcmd := v_sqlcmd || 'ALTER TABLE %1$s DROP CONSTRAINT uq_%1$s,ADD CONSTRAINT uq_%1$s UNIQUE (relate_uid);';
			END IF;
			v_sqlcmd := FORMAT(v_sqlcmd,v_reltable_name,quote_literal(v_relation_uid),v_origintable_name,v_relatetable_name);
			EXECUTE v_sqlcmd;	
		END IF;
		RETURN v_result;
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 删除类型及实例相关表结构
-- 删除表名为:ci_[$],info_[$],rel_[$]_%,rel_%_[$]的表
CREATE OR REPLACE FUNCTION "ddl_class_drop"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_id VARCHAR := NULL;
	v_rel VARCHAR := NULL;
	BEGIN	 
		FOR v_id 
		  IN SELECT cls.id
					 FROM jsonb_to_recordset($1) AS t1(uid UUID)
					INNER JOIN sys_class AS cls ON t1.uid = cls.uid
		LOOP
			EXECUTE FORMAT('DROP TABLE IF EXISTS ci_%s CASCADE;', v_id);
			EXECUTE FORMAT('DROP TABLE IF EXISTS info_%s CASCADE;', v_id);
			FOR v_rel 
				IN SELECT tablename
						 FROM pg_tables
						WHERE tablename LIKE FORMAT('rel_%s_%%',v_id) 
							 OR tablename LIKE FORMAT('rel_%%_%s',v_id)
			LOOP
				EXECUTE FORMAT('DROP TABLE IF EXISTS %s CASCADE;', v_rel);
			END LOOP;
		END LOOP;
		RETURN dml_ci_delete($1);
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 创建类型及表结构
CREATE OR REPLACE FUNCTION "ddl_class_create"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_uid UUID := NULL;
	v_result JSONB := NULL;
	BEGIN
		SELECT dml_class_insert($1) INTO v_result;
		IF COALESCE((v_result ->> 'flag')::BOOLEAN,TRUE) = TRUE THEN
			FOR v_uid
				IN SELECT uid FROM jsonb_to_recordset(v_result) AS(uid UUID)
			LOOP
					PERFORM ddl_ci_table_create(v_uid);
					PERFORM ddl_ci_info_table_create(v_uid);
			END LOOP;
		END IF;
		RETURN v_result;
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 修改类型及表结构
CREATE OR REPLACE FUNCTION "ddl_class_alter"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_id_old TEXT := NULL;
	v_id_new TEXT := NULL;
	v_rel TEXT := NULL;
	v_flag BOOLEAN := TRUE;
	BEGIN
		FOR v_id_old,v_flag
			IN SELECT cls.id,t1.flag
					 FROM jsonb_to_recordset($1) AS t1(uid UUID,flag BOOLEAN)
					INNER JOIN sys_class cls ON t1.uid = cls.uid
					WHERE COALESCE(t1.flag,cls.flag) != COALESCE(cls.flag,t1.flag)
		LOOP
			IF TRUE = v_flag THEN
				EXECUTE FORMAT('ALTER TABLE ci_%s ALTER COLUMN "id" SET NOT NULL;',v_id_old);
			ELSE
				EXECUTE FORMAT('ALTER TABLE ci_%s ALTER COLUMN "id" DROP NOT NULL;',v_id_old);
			END IF;
		END LOOP;
		FOR v_id_old,v_id_new
			IN SELECT cls.id,t1.id
					 FROM jsonb_to_recordset($1) AS t1(uid UUID,id VARCHAR) 
					INNER JOIN sys_class cls ON t1.uid = cls.uid AND t1.id != cls.id
		LOOP
			EXECUTE FORMAT('ALTER TABLE ci_%s RENAME TO ci_%s;',v_id_old,v_id_new);
			EXECUTE FORMAT('ALTER TABLE info_%s RENAME TO info_%s;',v_id_old,v_id_new);
			FOR v_rel 
				IN SELECT tablename
						 FROM pg_tables
						WHERE tablename LIKE FORMAT('rel_%s_%%',v_id_old) 
							 OR tablename LIKE FORMAT('rel_%%_%s',v_id_old)
			LOOP
				EXECUTE FORMAT('ALTER TABLE %s RENAME TO %s',v_rel,replace(v_rel,v_id_old,v_id_new));
			END LOOP;
		END LOOP;
		RETURN dml_class_update($1);
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 删除类型关联关系及表结构
CREATE OR REPLACE FUNCTION "ddl_class_relation_drop"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_rel VARCHAR := NULL;
	BEGIN	 
		FOR v_rel
			IN SELECT FORMAT('rel_%s_%s',cio.id,cir.id)
					 FROM jsonb_to_recordset($1) AS t1(uid UUID) 
				  INNER JOIN sys_class_relation AS cls ON t1.uid = cls.uid
					INNER JOIN sys_class cio ON cls.origin_uid = cio.uid
					INNER JOIN sys_class cir ON cls.relate_uid = cir.uid
			LOOP
				EXECUTE FORMAT('DROP TABLE IF EXISTS %s CASCADE;',v_rel);
		END LOOP;
		RETURN dml_ci_delete($1);	
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 创建类型关联关系及表结构
CREATE OR REPLACE FUNCTION "ddl_class_relation_create"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_uid UUID := NULL;
	v_result JSONB := NULL;
	BEGIN
		SELECT dml_class_relation_insert($1) INTO v_result;
		IF COALESCE((v_result ->> 'flag')::BOOLEAN,TRUE) = TRUE THEN
			FOR v_uid
				IN SELECT uid FROM jsonb_to_recordset(v_result) AS(uid UUID)
			LOOP
				PERFORM ddl_ci_relation_table_create(v_uid);
			END LOOP;
		END IF;
		RETURN v_result;
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 修改类型关联关系及表结构
CREATE OR REPLACE FUNCTION "ddl_class_relation_alter"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_flag BOOLEAN := TRUE;
	v_rel VARCHAR(40) := NULL;
	v_jsonb JSONB := NULL;
	BEGIN
		FOR v_flag,v_rel
			IN SELECT t1.flag,FORMAT('rel_%s_%s',cio.id,cir.id)
					 FROM jsonb_to_recordset($1) AS t1(uid UUID,flag BOOLEAN)
				  INNER JOIN sys_class_relation AS rel ON t1.uid = rel.uid
					INNER JOIN sys_class cio ON rel.origin_uid = cio.uid
					INNER JOIN sys_class cir ON rel.relate_uid = cir.uid
					WHERE COALESCE(t1.flag,rel.flag) != COALESCE(rel.flag,t1.flag)
		LOOP
			IF TRUE = v_flag THEN
				EXECUTE FORMAT('ALTER TABLE %1$s DROP CONSTRAINT uq_%1$s,ADD CONSTRAINT uq_%1$s UNIQUE (origin_uid,relate_uid);',v_rel);
			ELSE
				EXECUTE FORMAT('ALTER TABLE %1$s DROP CONSTRAINT uq_%1$s,ADD CONSTRAINT uq_%1$s UNIQUE (relate_uid);',v_rel);
			END IF;
		END LOOP;
		SELECT jsonb_agg(t1) INTO v_jsonb FROM jsonb_to_recordset($1) AS t1(uid UUID,flag BOOLEAN,id VARCHAR,name VARCHAR);
		RETURN dml_class_relation_update(v_jsonb);
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;
