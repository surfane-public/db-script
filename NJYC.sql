-- account item
SELECT dml_ci_insert ('[
	{"class_id":"account","id":"njycdl","name":"南京友成-管理员"}
]');

-- account item info
SELECT dml_ci_info_insert('[
	{"id":"njyc","info_id":"password","info_value":"njyc"},
	{"id":"njyc","info_id":"birthday","info_value":"2001-01-01"},
	{"id":"njyc","info_id":"name","info_value":"南京友成-管理员"}
]');

-- role item
SELECT dml_ci_insert ('[
	{"class_id":"role","id":"role_user","name":"运营商管理员角色"}
]');

-- account role relation
SELECT dml_ci_relation_insert ('[
	{"origin_id":"njycdl","relate_id":"role_user"}
]');

-- organiz item
SELECT dml_ci_insert ('[
	{"class_id":"organiz","id":"nj-ycdl","name":"南京友成电力工程有限公司"}
]');

-- account organiz relation
SELECT dml_ci_relation_insert ('[
	{"origin_id":"njycdl","relate_id":"nj-ycdl"}
]');
