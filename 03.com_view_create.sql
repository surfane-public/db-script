/*

-- 数据库  :通用
-- 脚本类型:视图创建脚本

-- 创建信息 --
-- 作者    :surfane
-- 版本    :V 1.0
-- 时间    :2017-05-10

-- 修订信息 --
-- 修订者  :
-- 版本    :
-- 时间    :
-- 修订概要:

*/
DROP VIEW IF EXISTS "v_ci_layout" CASCADE;
DROP VIEW IF EXISTS "v_ci_info" CASCADE;
DROP VIEW IF EXISTS "v_ci_relation" CASCADE;
DROP VIEW IF EXISTS "v_class_layout" CASCADE;
DROP VIEW IF EXISTS "v_class_info" CASCADE;
DROP VIEW IF EXISTS "v_class_relation" CASCADE;

-- 类型布局图
CREATE OR REPLACE VIEW "v_class_layout" AS 
WITH RECURSIVE cte AS(
	SELECT 0 AS layer
			 , ci.id::TEXT AS branch
			 , ci.uid, ci.id, ci.name, ci.flag
			 , ci.uid AS root_uid, ci.id AS root_id, ci.name AS root_name, ci.flag AS root_flag
			 , ci.parent_uid, NULL::VARCHAR AS parent_id, NULL::VARCHAR AS parent_name, NULL::BOOLEAN AS parent_flag
			 , ci.pre_uid			 
		FROM sys_class AS ci
   WHERE parent_uid IS NULL
	 UNION
	SELECT layer + 1 AS layer
			 , FORMAT('%s->%s',par.branch,ci.id) AS branch
			 , ci.uid, ci.id, ci.name, ci.flag
			 , par.root_uid, par.root_id, par.root_name, par.root_flag
			 , par.uid AS parent_uid, par.id AS parent_id, par.name AS parent_name, par.flag AS parent_flag
			 , ci.pre_uid			 
	 FROM cte AS par 
	INNER JOIN sys_class AS ci ON ci.parent_uid = par.uid
) 
SELECT cte.*
		 , rel.id AS pre_id, rel.name AS pre_name, rel.flag AS pre_flag
	FROM cte
	LEFT JOIN sys_class AS rel ON cte.pre_uid = rel.uid
;

-- 实例布局图
CREATE OR REPLACE VIEW "v_ci_layout" AS 
WITH RECURSIVE cte AS(
	SELECT 0 AS layer
			 , ci.id::TEXT AS branch
			 , ci.uid, ci.id, ci.name, ci.flag
			 , ci.parent_uid, NULL::VARCHAR AS parent_id, NULL::VARCHAR AS parent_name, NULL::BOOLEAN AS parent_flag
			 , ci.uid AS root_uid, ci.id AS root_id, ci.name AS root_name, ci.flag AS root_flag
			 , ci.pre_uid
			 , ci.class_uid
		FROM sys_item AS ci
   WHERE ci.parent_uid IS NULL
	 UNION
	SELECT layer + 1 AS layer
			 , FORMAT('%s->%s',par.branch,ci.id) AS branch
			 , ci.uid, ci.id, ci.name, ci.flag
			 , par.uid AS parent_uid, par.id AS parent_id, par.name AS parent_name, par.flag AS parent_flag
			 , par.root_uid, par.root_id, par.root_name, par.root_flag
			 , ci.pre_uid
			 , ci.class_uid
	 FROM cte AS par 
	INNER JOIN sys_item AS ci ON ci.parent_uid = par.uid 
) 
SELECT cte.layer
		 , cte.branch
		 , cte.uid, cte.id, cte.name, cte.flag
		 , cte.parent_uid, cte.parent_id, cte.parent_name, cte.parent_flag
		 , cte.root_uid, cte.root_id, cte.root_name, cte.root_flag
		 , pre.uid AS pre_uid, pre.id AS pre_id, pre.name AS pre_name, pre.flag AS pre_flag
		 , cls.uid AS class_uid, cls.id As class_id, cls.name AS class_name, cls.flag AS class_flag
	FROM cte 
 INNER JOIN sys_class As cls ON cls.uid = cte.class_uid
  LEFT JOIN sys_item AS pre ON cte.pre_uid = pre.uid
;

-- 类型包含继承属性视图
CREATE OR REPLACE VIEW "v_class_info" AS 
WITH RECURSIVE cte AS(
	SELECT CASE WHEN inf.class_uid  IS NULL THEN NULL
							WHEN ci.uid = inf.class_uid THEN TRUE 
							ELSE FALSE END AS self_flag			 
			 , 0 AS layer
			 , ci.id::TEXT AS branch
			 , ci.uid, ci.id, ci.name, ci.flag			 
			 , inf.uid AS info_uid, inf.id AS info_id, inf.name AS info_name, inf.flag AS info_flag
			 , ci.parent_uid
			 , inf.class_uid AS origin_uid
		FROM sys_class AS ci
		LEFT JOIN sys_info AS inf ON ci.uid = inf.class_uid
   WHERE parent_uid IS NULL
	 UNION
	SELECT CASE WHEN inf.class_uid  IS NULL THEN NULL
							WHEN ci.uid = inf.class_uid THEN TRUE 
							ELSE FALSE END AS self_flag			 
			 , layer + 1 AS layer
			 , FORMAT('%s->%s',par.branch,ci.id) AS branch
			 , ci.uid, ci.id, ci.name, ci.flag
			 , inf.uid AS info_uid, inf.id AS info_id, inf.name AS info_name, inf.flag AS info_flag	
			 , ci.parent_uid
			 , inf.class_uid AS origin_uid
	 FROM cte AS par 
	INNER JOIN sys_class AS ci ON ci.parent_uid = par.uid
	 LEFT JOIN sys_info AS inf ON ci.uid = inf.class_uid OR par.origin_uid = inf.class_uid
) 
SELECT cte.self_flag
		 , infset.value
		 , cte.layer
		 , cte.branch
	   , cte.uid, cte.id, cte.name, cte.flag
		 , cte.info_uid, cte.info_id, cte.info_name, cte.info_flag
		 , infset.uid AS infoset_uid, infset.id AS infoset_id, infset.name AS infoset_name, infset.flag AS infoset_flag
		 , par.uid AS parent_uid, par.id AS parent_id, par.name AS parent_name, par.flag AS parent_flag
		 , clo.uid AS origin_uid, clo.id AS origin_id, clo.name AS origin_name, clo.flag AS origin_flag
	FROM cte
	LEFT JOIN sys_infoset AS infset ON cte.info_uid = infset.info_uid
	LEFT JOIN sys_class AS par ON cte.parent_uid = par.uid
	LEFT JOIN sys_class AS clo ON cte.origin_uid = clo.uid
;

-- 实例包含继承属性视图
CREATE OR REPLACE VIEW "v_ci_info" AS 
WITH RECURSIVE cte AS(
	SELECT ci.uid, ci.id, ci.name, ci.flag, ci.parent_uid, ci.pre_uid, ci.class_uid			 
			 , CASE WHEN cinf.item_uid  IS NULL THEN NULL
							WHEN ci.uid = cinf.item_uid THEN TRUE 
							ELSE FALSE END AS self_flag			 
			 , 0 AS layer
			 , ci.id::TEXT AS branch
			 , cinf.value
			 , cinf.info_uid, cinf.infoset_uid
			 , cinf.item_uid AS origin_uid
			 , cinf.uid AS item_info_uid
			 , cinf.flag AS item_info_flag
		FROM sys_item AS ci
		LEFT JOIN sys_item_info AS cinf ON ci.uid = cinf.item_uid
   WHERE parent_uid IS NULL
	 UNION
	SELECT ci.uid, ci.id, ci.name, ci.flag, ci.parent_uid, ci.pre_uid, ci.class_uid		 
			 , CASE WHEN cinf.item_uid  IS NULL THEN NULL
							WHEN ci.uid = cinf.item_uid THEN TRUE 
							ELSE FALSE END AS self_flag			 
			 , layer + 1 AS layer
			 , FORMAT('%s->%s',par.branch,ci.id) AS branch
			 , cinf.value
			 , cinf.info_uid, cinf.infoset_uid
			 , cinf.item_uid AS origin_uid
			 , cinf.uid AS item_info_uid
			 , cinf.flag AS item_info_flag
	 FROM cte AS par 
	INNER JOIN sys_item AS ci ON ci.parent_uid = par.uid
	 LEFT JOIN sys_item_info AS cinf ON ci.uid = cinf.item_uid 
		 OR (
						 cinf.item_uid = par.origin_uid 
				 AND cinf.info_uid = par.info_uid 
				 AND cinf.info_uid NOT IN (SELECT info_uid FROM sys_item_info WHERE sys_item_info.item_uid = ci.uid)
			)
)
SELECT cte.item_info_uid
		 , cte.item_info_flag
		 , cte.self_flag
		 , COALESCE(infset.value,cte.value) AS value
		 , cte.layer
		 , cte.branch
		 , cte.uid, cte.id, cte.name, cte.flag
		 , inf.uid AS info_uid, inf.id AS info_id, inf.name AS info_name, inf.flag AS info_flag
		 , infset.uid AS infoset_uid, infset.id AS infoset_id, infset.name AS infoset_name, infset.flag AS infoset_flag
		 , par.uid AS parent_uid, par.id AS parent_id, par.name AS parent_name, par.flag AS parent_flag
		 , pre.uid AS pre_uid, pre.id AS pre_id, pre.name AS pre_name, pre.flag AS pre_flag
		 , cio.uid AS origin_uid, cio.id AS origin_id, cio.name AS origin_name, cio.flag AS origin_flag
		 , cls.uid AS class_uid, cls.id AS class_id, cls.name AS class_name, cls.flag AS class_flag
	FROM cte 
 INNER JOIN sys_class AS cls ON cte.class_uid = cls.uid
	LEFT JOIN sys_info AS inf ON cte.info_uid = inf.uid
	LEFT JOIN sys_infoset AS infset ON cte.infoset_uid = infset.uid
	LEFT JOIN sys_item AS par ON cte.parent_uid = par.uid
	LEFT JOIN sys_item AS pre ON cte.pre_uid = pre.uid
	LEFT JOIN sys_item AS cio ON cte.origin_uid = cio.uid
;

-- 类型关联关系视图
CREATE OR REPLACE VIEW "v_class_relation" AS 
SELECT rel.uid
     , rel.id
		 , rel.name
		 , rel.flag
		 , clo.uid AS origin_uid
		 , clo.id AS origin_id
		 , clo.name AS origin_name
		 , clo.flag AS origin_flag
		 , clr.uid AS relate_uid
		 , clr.id AS relate_id
		 , clr.name AS relate_name
		 , clr.flag AS relate_flag
	FROM sys_class_relation AS rel
 INNER JOIN sys_class AS clo ON clo.uid = rel.origin_uid
 INNER JOIN sys_class AS clr ON clr.uid = rel.relate_uid
;

-- 实例及其关联实例属性视图
CREATE OR REPLACE VIEW "v_ci_relation" AS 
WITH RECURSIVE cte AS(
	SELECT rel.uid
			 , rel.flag
			 , rel.relation_uid
			 , rel.origin_uid
			 , ci.uid AS relate_uid
			 , ci.id AS relate_id
			 , ci.name AS relate_name
			 , ci.flag AS relate_flag
			 , TRUE AS self_flag
		FROM sys_item_relation AS rel
	 INNER JOIN sys_item AS ci ON rel.relate_uid = ci.uid
	 UNION
	SELECT par.uid
			 , par.flag
			 , par.relation_uid
			 , par.origin_uid
			 , ci.uid AS relate_uid
			 , ci.id AS relate_id
			 , ci.name AS relate_name
			 , ci.flag AS relate_flag
			 , FALSE AS self_flag
	 FROM cte AS par 
	INNER JOIN sys_item AS ci ON par.relate_uid = ci.parent_uid
) 
SELECT cte.uid
		 , cte.flag
		 , cte.self_flag
		 , rel.uid AS relation_uid
     , rel.id AS relation_id
		 , rel.name AS relation_name
		 , rel.flag AS relation_flag
		 , cio.uid AS origin_uid
		 , cio.id AS origin_id
		 , cio.name AS origin_name
		 , cio.flag AS origin_flag
		 , cte.relate_uid
		 , cte.relate_id
		 , cte.relate_name
		 , cte.relate_flag
		 , clo.uid AS origin_class_uid
		 , clo.id AS origin_class_id
		 , clo.name AS origin_class_name
		 , clo.flag AS origin_class_flag
		 , clr.uid AS relate_class_uid
		 , clr.id AS relate_class_id
		 , clr.name AS relate_class_name
		 , clr.flag AS relate_class_flag
	FROM cte 
 INNER JOIN sys_class_relation AS rel ON cte.relation_uid = rel.uid
 INNER JOIN sys_class AS clo ON rel.origin_uid = clo.uid
 INNER JOIN sys_class AS clr ON rel.relate_uid = clr.uid
 INNER JOIN sys_item AS cio ON cte.origin_uid = cio.uid
;

-- 实例包含关联实例,继承属性视图
/*
CREATE OR REPLACE VIEW "v_ci_relation_info" AS 
WITH cte AS(
	SELECT ci.uid,ci.uid AS item_uid FROM sys_item AS ci
	 UNION
	SELECT ci.uid,rel.relate_uid AS item_uid
		FROM sys_item AS ci
   INNER JOIN sys_item_relation rel ON ci.uid = rel.origin_uid
)
SELECT cls.uid AS class_uid
		 , cls.id AS class_id
		 , cls.name AS class_name
		 , cls.flag AS class_flag
		 , cls.parent_uid AS class_parent_uid
		 , cls.pre_uid AS class_pre_uid
		 , ci.uid
		 , ci.name
		 , ci.flag
		 , ci.parent_uid
		 , ci.pre_uid
		 , inf.class_uid AS item_class_uid
		 , inf.class_id AS item_class_id
		 , inf.class_name AS item_class_name
		 , inf.class_flag AS item_class_flag
		 , inf.uid AS item_uid
		 , inf.id AS item_id
		 , inf.name AS item_name
		 , inf.flag AS item_flag
		 , inf.parent_uid AS item_parent_uid
		 , inf.pre_uid AS item_pre_uid
		 , inf.info_uid
		 , inf.info_id
		 , inf.info_name
		 , inf.info_flag
		 , inf.info_value
		 , inf.infoset_uid
		 , inf.infoset_id
		 , inf.infoset_name
		 , inf.infoset_flag
		 , inf.infoset_value
		 , inf.origin_uid
		 , inf.origin_id
		 , inf.origin_name
		 , inf.origin_flag
		 , CASE WHEN cte.uid = cte.item_uid THEN inf.self_flag ELSE FALSE END AS self_flag
		 , inf.layer
		 , inf.branch
	FROM cte
 INNER JOIN sys_item AS ci ON cte.uid = ci.uid
 INNER JOIN sys_class AS cls ON cls.uid = ci.class_uid
 INNER JOIN v_ci_info AS inf ON cte.item_uid = inf.uid 
;
*/
