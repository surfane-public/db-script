/*

-- 数据库  :能源(电能)管理平台
-- 脚本类型:系统运行基础配置脚本

-- 创建信息 --
-- 作者    :杨永红
-- 版本    :V 1.0
-- 时间    :2017-05-10

-- 修订信息 --
-- 修订者  :
-- 版本    :
-- 时间    :
-- 修订概要:

*/

-- tmp table
DROP TABLE IF EXISTS "tmp_sys_config" CASCADE;
CREATE TABLE "tmp_sys_config" (
"class_id" VARCHAR(40),
"id" VARCHAR(40),
"name" VARCHAR(40),
"flag" BOOLEAN,
"info_id" VARCHAR(40),
"info_name" VARCHAR(40),
"info_flag" BOOLEAN,
"infoset_id" VARCHAR(40),
"infoset_name" VARCHAR(40),
"infoset_flag" BOOLEAN,
"info_value" TEXT,
"origin_class_id" VARCHAR(40),
"relate_class_id" VARCHAR(40),
"origin_id" VARCHAR(40),
"relate_id" VARCHAR(40),
"parent_id" VARCHAR(40),
"pre_id" VARCHAR(40)
)
INHERITS ("sys_void") 
;
	
-- bu class config;
TRUNCATE TABLE "tmp_sys_config" CASCADE;
COPY tmp_sys_config(parent_id,pre_id,id,name,flag,info_id,info_name,info_flag,infoset_id,infoset_name,infoset_flag,info_value)
	FROM 'D:/myApp/script/eCloud/public/bu_data/11.bu_class_config.csv'
	DELIMITER AS ',' csv HEADER QUOTE AS '"' ENCODING 'gbk';

-- 03.sys_class
WITH cte AS(
	SELECT DISTINCT id,name,flag,parent_id,pre_id FROM tmp_sys_config
	)SELECT ddl_class_create(jsonb_agg(cte)) FROM cte;

-- 04.sys_info
WITH cte AS(
	SELECT DISTINCT info_id AS id,info_name AS name,info_flag AS flag, id AS class_id
		FROM tmp_sys_config
	 WHERE info_id IS NOT NULL
	)SELECT dml_info_insert(jsonb_agg(cte)) FROM cte;

-- 05.sys_infoset
WITH cte AS(
	SELECT infoset_id AS id,infoset_name AS name,infoset_flag AS flag,info_id,info_value
		FROM tmp_sys_config
	 WHERE infoset_id IS NOT NULL
	)SELECT dml_infoset_insert(jsonb_agg(cte)) FROM cte;

-- bu class relation config
TRUNCATE TABLE "tmp_sys_config" CASCADE;
COPY tmp_sys_config(id,name,flag,origin_class_id,relate_class_id)
	FROM 'D:/myApp/script/eCloud/public/bu_data/12.bu_class_relation.csv'
	DELIMITER AS ',' csv HEADER QUOTE AS '"' ENCODING 'gbk';
	
-- 06.sys_class_relation
WITH cte AS(
	SELECT id,name,flag,origin_class_id,relate_class_id FROM tmp_sys_config
	)SELECT ddl_class_relation_create(jsonb_agg(cte)) FROM cte;

-- bu item config
TRUNCATE TABLE "tmp_sys_config" CASCADE;
COPY tmp_sys_config(class_id,parent_id,pre_id,id,name,flag,info_id,infoset_id,info_value)
	FROM 'D:/myApp/script/eCloud/public/bu_data/13.bu_item_config.csv'
	DELIMITER AS ',' csv HEADER QUOTE AS '"' ENCODING 'gbk';
	
-- 07.sys_item
WITH cte AS(
	SELECT DISTINCT id,name,flag,parent_id,pre_id,class_id FROM tmp_sys_config
	)SELECT dml_ci_insert(jsonb_agg(cte)) FROM cte;
	
-- 08.sys_item_info
WITH cte AS(
	SELECT id,info_id,infoset_id,info_value
		FROM tmp_sys_config
	 WHERE info_id IS NOT NULL
	)SELECT dml_ci_info_insert(jsonb_agg(cte)) FROM cte;

-- system item relation config
TRUNCATE TABLE "tmp_sys_config" CASCADE;
COPY tmp_sys_config(origin_id,relate_id,flag)
	FROM 'D:/myApp/script/eCloud/public/bu_data/14.bu_item_relation_config.csv'
	DELIMITER AS ',' csv HEADER QUOTE AS '"' ENCODING 'gbk';

-- 09.sys_item_relation
WITH cte AS(
	SELECT origin_id,relate_id,flag FROM tmp_sys_config
	)SELECT dml_ci_relation_insert(jsonb_agg(cte)) FROM cte;

-- clear;
DROP TABLE IF EXISTS "tmp_sys_config" CASCADE;