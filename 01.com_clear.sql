/*

-- 数据库  :平台通用
-- 脚本类型:清除type,table,function脚本

-- 创建信息 --
-- 作者    :杨永红
-- 版本    :V 1.0
-- 时间    :2017-05-10

-- 修订信息 --
-- 修订者  :
-- 版本    :
-- 时间    :
-- 修订概要:

*/

-- clear extension
DROP EXTENSION IF EXISTS "uuid-ossp" CASCADE;
DROP EXTENSION IF EXISTS "tablefunc" CASCADE;
DROP EXTENSION IF EXISTS "dblink" CASCADE;

-- clear type
DROP TYPE IF EXISTS "type_layout" CASCADE;
DROP TYPE IF EXISTS "type_layer_layout" CASCADE;

-- drop table;
DROP TABLE IF EXISTS "sys_void" CASCADE;

-- clear ddl function:[1000,1099]
--1001
DROP FUNCTION IF EXISTS "ddl_ci_table_create"(UUID) CASCADE;
--1003
DROP FUNCTION IF EXISTS "ddl_ci_relation_table_create"(UUID) CASCADE;
--1005
DROP FUNCTION IF EXISTS "ddl_ci_info_table_create"(UUID) CASCADE;
--1011 dropClass
DROP FUNCTION IF EXISTS "ddl_class_drop"(JSONB) CASCADE;
--1013 createClass
DROP FUNCTION IF EXISTS "ddl_class_create"(JSONB) CASCADE;
--1015 alertClass
DROP FUNCTION IF EXISTS "ddl_class_alter"(JSONB) CASCADE;
--1021 dropClassRelation
DROP FUNCTION IF EXISTS "ddl_class_relation_drop"(JSONB) CASCADE;
--1023 createClassRelation
DROP FUNCTION IF EXISTS "ddl_class_relation_create"(JSONB) CASCADE;
--1025 alterClassRelation
DROP FUNCTION IF EXISTS "ddl_class_relation_alter"(JSONB) CASCADE;

-- clear dml function:[1100,1999]
-- delete:[1101,1299]
--1101 deleteCi
DROP FUNCTION IF EXISTS "dml_ci_delete"(JSONB) CASCADE;
-- common insert:[1201,1299]
--1201 addClass
DROP FUNCTION IF EXISTS "dml_class_insert"(JSONB) CASCADE;
--1203 addClassRelation
DROP FUNCTION IF EXISTS "dml_class_relation_insert"(JSONB) CASCADE;
--1205 addClassInfo
DROP FUNCTION IF EXISTS "dml_info_insert"(JSONB) CASCADE;
--1207 addClassInfoSet
DROP FUNCTION IF EXISTS "dml_infoset_insert"(JSONB) CASCADE;
--1211 addCi
DROP FUNCTION IF EXISTS "dml_ci_insert"(JSONB) CASCADE;
--1213 addCiRelation
DROP FUNCTION IF EXISTS "dml_ci_relation_insert"(JSONB) CASCADE;
--1215 addCiInfo
DROP FUNCTION IF EXISTS "dml_ci_info_insert"(JSONB) CASCADE;
-- common update:[1301,1399]
--1301 updateClass
DROP FUNCTION IF EXISTS "dml_class_update"(JSONB) CASCADE;
--1303 updateClassRelation
DROP FUNCTION IF EXISTS "dml_class_relation_update"(JSONB) CASCADE;
--1306 updateClassInfo
DROP FUNCTION IF EXISTS "dml_info_update"(JSONB) CASCADE;
--1307 updateClassInfoset
DROP FUNCTION IF EXISTS "dml_infoset_update"(JSONB) CASCADE;
--1311 updateCi
DROP FUNCTION IF EXISTS "dml_ci_update"(JSONB) CASCADE;
--1313 updateCiRelation
DROP FUNCTION IF EXISTS "dml_ci_relation_update"(JSONB) CASCADE;
--1315 updateCiInfo
DROP FUNCTION IF EXISTS "dml_ci_info_update"(JSONB) CASCADE;
--1321
DROP FUNCTION IF EXISTS "dml_ci_info_replace"(JSONB) CASCADE;
-- special:[1401,1499]
-- fcuntion router
--1400 getFunctionRouter
DROP FUNCTION IF EXISTS "dml_function_router_select"() CASCADE;
--1401 addFunctionRouter
DROP FUNCTION IF EXISTS "dml_function_router_insert"(JSONB) CASCADE;
--1403 updateFunctionRouter
DROP FUNCTION IF EXISTS "dml_function_router_update"(JSONB) CASCADE;
--1410 getFunctionState
DROP FUNCTION IF EXISTS "dml_function_state_select"() CASCADE;
DROP FUNCTION IF EXISTS "dml_function_state_select"(CHAR(5),JSONB) CASCADE;
DROP FUNCTION IF EXISTS "dml_function_state_select"(CHAR(5)) CASCADE;
--1411 addFunctionState
DROP FUNCTION IF EXISTS "dml_function_state_insert"(JSONB) CASCADE;
--1413 updateFunctionState
DROP FUNCTION IF EXISTS "dml_function_state_update"(JSONB) CASCADE;

-- clear system function:[2000,2999]
-- DROP FUNCTION IF EXISTS "func_run" (INT,VARCHAR,TEXT) CASCADE;
-- DROP FUNCTION IF EXISTS "func_get_class_layout"(TEXT) CASCADE;
-- DROP FUNCTION IF EXISTS "func_get_class_info"(TEXT) CASCADE;
-- DROP FUNCTION IF EXISTS "func_get_class_infoset"(TEXT) CASCADE;
-- DROP FUNCTION IF EXISTS "func_get_class_relation"(TEXT) CASCADE;
-- DROP FUNCTION IF EXISTS "func_get_ci_layout"(JSONB) CASCADE;
-- DROP FUNCTION IF EXISTS "func_get_ci_info"(JSONB) CASCADE;
-- DROP FUNCTION IF EXISTS "func_get_ci_relation"(JSONB) CASCADE;
-- DROP FUNCTION IF EXISTS "func_get_ci_infoset"(UUID) CASCADE;
-- DROP FUNCTION IF EXISTS "func_get_ci_infolist"(UUID) CASCADE;
-- DROP FUNCTION IF EXISTS "func_get_ci_sub_info"(UUID) CASCADE;
-- DROP FUNCTION IF EXISTS "func_get_ci_previous"(UUID) CASCADE;
-- DROP FUNCTION IF EXISTS "func_get_ci_next"(UUID) CASCADE;
-- DROP FUNCTION IF EXISTS "func_get_ci_sub"(UUID) CASCADE;
-- DROP FUNCTION IF EXISTS "func_ci_flag_commit"(UUID,bool) CASCADE;
-- DROP FUNCTION IF EXISTS "func_ci_flag_rollback"(UUID,bool) CASCADE;
-- 
-- DROP FUNCTION IF EXISTS "func_account_authentication"(JSONB) CASCADE;
-- DROP FUNCTION IF EXISTS "func_account_favourite"(UUID) CASCADE;
-- DROP FUNCTION IF EXISTS "func_account_favourite_add"(UUID,JSONB) CASCADE;
-- DROP FUNCTION IF EXISTS "func_account_favourite_remove"(UUID,JSONB) CASCADE;
-- DROP FUNCTION IF EXISTS "func_account_init"(JSONB) CASCADE;
-- DROP FUNCTION IF EXISTS "func_account_menu"(UUID,TEXT) CASCADE;
-- DROP FUNCTION IF EXISTS "func_account_menu_list"(UUID,UUID) CASCADE;
-- DROP FUNCTION IF EXISTS "func_account_register"(JSONB) CASCADE;

-- clear bu function:[3000,+)
-- DROP FUNCTION IF EXISTS "func_get_station"(UUID,TEXT) CASCADE;
-- DROP FUNCTION IF EXISTS "func_get_station_layout"(JSONB) CASCADE;
-- DROP FUNCTION IF EXISTS "func_get_station_device"(JSONB) CASCADE;
-- DROP FUNCTION IF EXISTS "func_get_tag"() CASCADE;
-- DROP FUNCTION IF EXISTS "func_get_profile"() CASCADE;
-- DROP FUNCTION IF EXISTS "func_import_config"(JSONB) CASCADE;