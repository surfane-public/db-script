/*

-- 数据库  :平台通用
-- 脚本类型:DML接口函数创建脚本

-- 创建信息 --
-- 作者    :surfane
-- 版本    :V 1.0
-- 时间    :2017-10-10

-- 修订信息 --
-- 修订者  :
-- 版本    :
-- 时间    :
-- 修订概要:

*/
-- uid与id不匹配错误
CREATE OR REPLACE FUNCTION "dml_mismatch_check"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_mismatch CHAR(5) := 'U0103';
	v_state CHAR(5) := '00000';
	v_result JSONB := NULL;
	BEGIN
		IF $1 IS NOT NULL THEN
			v_state := v_mismatch;
		END IF;
		SELECT dml_function_state_select(v_state,v_result) INTO v_result;
		RETURN v_result;		
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 配置项类型数据检测
-- class_uid与class_id匹配
CREATE OR REPLACE FUNCTION "dml_class_check"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_result JSONB := NULL;
	BEGIN
		WITH cte AS(
			SELECT DISTINCT t1.class_uid AS uid,t1.class_id AS id
				FROM jsonb_to_recordset($1) AS t1(class_uid UUID,class_id VARCHAR)
			 INNER JOIN sys_class AS rel ON t1.class_uid = rel.uid AND t1.class_id != rel.id
		)
		SELECT jsonb_agg(cte) INTO v_result FROM cte; 
		RETURN dml_mismatch_check(v_result);		
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 类型属性数据检测
-- info_uid与info_id匹配
CREATE OR REPLACE FUNCTION "dml_info_check"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_result JSONB := NULL;
	BEGIN
		WITH cte AS(
			SELECT DISTINCT t1.info_uid AS uid,t1.info_id AS id
				FROM jsonb_to_recordset($1) AS t1(info_uid UUID,info_id VARCHAR)
			 INNER JOIN sys_info AS rel ON t1.ino_uid = rel.uid AND t1.info_id != rel.id
		)
		SELECT jsonb_agg(cte) INTO v_result FROM cte; 
		RETURN dml_mismatch_check(v_result);		
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 类型属性约束数据检测
-- info_uid与info_id匹配
CREATE OR REPLACE FUNCTION "dml_infoset_check"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_result JSONB := NULL;
	BEGIN
		WITH cte AS(
			SELECT DISTINCT t1.infoset_uid AS uid,t1.infoset_id AS id
				FROM jsonb_to_recordset($1) AS t1(infoset_uid UUID,infoset_id VARCHAR)
			 INNER JOIN sys_infoset AS rel ON t1.inoset_uid = rel.uid AND t1.infoset_id != rel.id
		)
		SELECT jsonb_agg(cte) INTO v_result FROM cte; 
		RETURN dml_mismatch_check(v_result);		
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 类型关联关系数据检测
-- info_uid与info_id匹配
CREATE OR REPLACE FUNCTION "dml_relation_check"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_result JSONB := NULL;
	BEGIN
		WITH cte AS(
			SELECT DISTINCT t1.relation_uid AS uid,t1.relation_id AS id
				FROM jsonb_to_recordset($1) AS t1(relation_uid UUID,relation_id VARCHAR)
			 INNER JOIN sys_class_relation AS rel ON t1.relation_uid = rel.uid AND t1.relation_id != rel.id
		)
		SELECT jsonb_agg(cte) INTO v_result FROM cte; 
		RETURN dml_mismatch_check(v_result);			
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 配置项数据检测
-- class_uid与class_id匹配
CREATE OR REPLACE FUNCTION "dml_item_check"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_result JSONB := NULL;
	BEGIN
		SELECT dml_class_check($1) INTO v_result;
		IF COALESCE((v_result ->> 'flag')::BOOLEAN,TRUE) = TRUE THEN
			WITH cte AS(
				SELECT DISTINCT t1.uid,t1.id
					FROM jsonb_to_recordset($1) AS t1(class_uid UUID,class_id VARCHAR,uid UUID,id VARCHAR)
				 INNER JOIN sys_class AS cls ON t1.class_uid = cls.uid OR t1.class_id = cls.id
				 INNER JOIN sys_item AS rel ON t1.uid = rel.uid AND t1.id != rel.id
			)
			SELECT jsonb_agg(cte) INTO v_result FROM cte;
			RETURN dml_mismatch_check(v_result);
		END IF;
		RETURN v_result
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;


-- 关联关系数据检测
-- 1. origin_uid与origin_id匹配
-- 2. relate_uid与relate_id匹配
CREATE OR REPLACE FUNCTION "dml_class_relation_check"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_result JSONB := NULL;
	BEGIN
		WITH cte AS(
			SELECT DISTINCT t1.origin_uid AS uid,t1.origin_id AS id
				FROM jsonb_to_recordset($1) AS t1(origin_uid UUID,origin_id VARCHAR)
			 INNER JOIN sys_class AS rel ON t1.origin_uid = rel.uid AND t1.origin_id != rel.id
			UNION
			SELECT DISTINCT t1.relate_uid AS uid,t1.relate_id AS id
				FROM jsonb_to_recordset($1) AS t1(relate_uid UUID,relate_id VARCHAR)
			 INNER JOIN sys_class AS rel ON t1.relate_uid = rel.uid AND t1.relate_id != rel.id
		)
		SELECT jsonb_agg(cte) INTO v_result FROM cte; 
		RETURN dml_mismatch_check(v_result);	
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 关联关系数据检测
-- 1. class相同且origin_uid与origin_id匹配
-- 2. class相同且relate_uid与relate_id匹配
CREATE OR REPLACE FUNCTION "dml_item_relation_check"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_result JSONB := NULL;
	BEGIN
		SELECT dml_relation_check($1) INTO v_result;
		IF COALESCE((v_result ->> 'flag')::BOOLEAN,TRUE) = TRUE THEN
			WITH cte AS(
				SELECT DISTINCT t1.origin_uid AS uid,t1.origin_id AS id
					FROM jsonb_to_recordset($1) AS t1(relation_uid UUID,relation_id VARCHAR,origin_uid UUID,origin_id VARCHAR)
				 INNER JOIN sys_class_relation AS rel ON t1.relation_uid = rel.uid OR t1.relation_id = rel.id
				 INNER JOIN sys_item AS rel 
						ON rel.class_uid = cls.uid
					 AND rel.uid = t1.origin_uid
					 AND rel.id != t1.origin_id
				UNION
				SELECT DISTINCT t1.relate_uid AS uid,t1.relate_id AS id
					FROM jsonb_to_recordset($1) AS t1(relation_uid UUID,relation_id VARCHAR,relate_uid UUID,relate_id VARCHAR)
				 INNER JOIN sys_class_relation AS rel ON t1.relation_uid = rel.uid OR t1.relation_id = rel.id
				 INNER JOIN sys_item AS rel 
						ON rel.class_uid = cls.uid
					 AND rel.uid = t1.relate_uid
					 AND rel.id != t1.relate_id
			)
			SELECT jsonb_agg(cte) INTO v_result FROM cte; 
			RETURN dml_mismatch_check(v_result);	
		END IF;
		RETURN v_result;
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 类别数据检测
-- 1. parent_uid与parent_id匹配,且不与当前uid,id同一记录
-- 2. pre_uid与pre_id匹配,且不与当前uid,id同一记录
CREATE OR REPLACE FUNCTION "dml_class_data_check"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_mismatch CHAR(5) := 'U0103';
	v_error CHAR(5) := 'U0104';
	v_state CHAR(5) := '00000';
	v_result JSONB := NULL;
	BEGIN
		WITH cte AS(
			SELECT t1.parent_uid AS uid,t1.parent_id AS id
				FROM jsonb_to_recordset($1) AS t1(parent_uid UUID,parent_id VARCHAR)
			 INNER JOIN sys_class AS rel ON t1.parent_uid = rel.uid AND t1.parent_id != rel.id
		  UNION
		  SELECT t1.pre_uid AS uid,t1.pre_id AS id
				FROM jsonb_to_recordset($1) AS t1(pre_uid UUID,pre_id VARCHAR)
			 INNER JOIN sys_class AS rel ON t1.pre_uid = rel.uid AND t1.pre_id != rel.id
		)
 		SELECT jsonb_agg(cte) INTO v_result FROM cte; 
		IF v_result IS NOT NULL THEN
			v_state := v_mismatch;
		ELSE
			WITH cte AS(
				SELECT DISTINCT rel.uid,rel.id
					FROM jsonb_to_recordset($1)
						AS t1(uid UUID,id VARCHAR,parent_uid UUID,parent_id VARCHAR,pre_uid UUID,pre_id VARCHAR)
				 INNER JOIN sys_class AS rel ON t1.uid = rel.uid OR t1.id = rel.id
				 WHERE rel.uid = t1.parent_uid
						OR rel.uid = t1.pre_uid
						OR rel.id = t1.parent_id
						OR rel.id = t1.pre_id
			)
			SELECT jsonb_agg(cte) INTO v_result FROM cte; 
			IF v_result IS NOT NULL THEN
				v_state := v_error;
			END IF;
		END IF;
		SELECT dml_function_state_select(v_state,v_result) INTO v_result;
		RETURN v_result;		
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 配置项数据检测
-- 1. class_id与class_uid匹配
-- 2. parent_id与parent_uid匹配,且不与当前Id,uid同一记录
-- 3. pre_id与pre_uid匹配,且不与当前Id,uid同一记录
CREATE OR REPLACE FUNCTION "dml_item_data_check"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_mismatch CHAR(5) := 'U0103';
	v_error CHAR(5) := 'U0104';
	v_state CHAR(5) := '00000';
	v_result JSONB := NULL;
	BEGIN
		SELECT dml_class_check($1) INTO v_result;
		IF COALESCE((v_result ->> 'flag')::BOOLEAN,TRUE) = TRUE THEN
			WITH cte AS(
				SELECT DISTINCT t1.class_uid,t1.class_id,t1.parent_uid AS uid,t1.parent_id  AS id
					FROM jsonb_to_recordset($1) AS t1(class_uid UUID, class_id VARCHAR,parent_uid UUID,parent_id VARCHAR)
				 INNER JOIN sys_class AS cls ON cls.uid = t1.class_uid OR cls.id = t1.class_id
				 INNER JOIN sys_item AS rel
								 ON rel.class_uid = cls.uid
								AND rel.uid = t1.parent_uid
								AND rel.id != t1.parent_id
			  UNION
				SELECT DISTINCT t1.class_uid,t1.class_id,t1.pre_uid AS uid,t1.pre_id  AS id
					FROM jsonb_to_recordset($1) AS t1(class_uid UUID, class_id VARCHAR,pre_uid UUID,pre_id VARCHAR)
				 INNER JOIN sys_class AS cls ON cls.uid = t1.class_uid OR cls.id = t1.class_id
				 INNER JOIN sys_item AS rel
								 ON rel.class_uid = cls.uid
								AND rel.uid = t1.pre_uid 
								AND rel.id != t1.pre_id
			)
			SELECT jsonb_agg(cte) INTO v_result FROM cte; 
			IF v_result IS NOT NULL THEN
				v_state := v_mismatch;
			ELSE
				WITH cte AS(
				SELECT DISTINCT rel.uid,rel.id
					FROM jsonb_to_recordset($1)
						AS t1(class_uid UUID,class_id VARCHAR,uid UUID,id VARCHAR,parent_uid UUID,parent_id VARCHAR,pre_uid UUID,pre_id VARCHAR)
				 INNER JOIN sys_class AS cls ON t1.class_uid = cls.uid OR t1.class_id = cls.id
				 INNER JOIN sys_item AS rel ON cls.uid = rel.class_uid AND (t1.uid = rel.uid OR t1.id = rel.id)
				 WHERE rel.uid = t1.parent_uid
						OR rel.uid = t1.pre_uid
						OR rel.id = t1.parent_id
						OR rel.id = t1.pre_id
				)
				SELECT jsonb_agg(cte) INTO v_result FROM cte; 
				IF v_result IS NOT NULL THEN
					v_state := v_error;
				END IF;
			END IF;
			SELECT dml_function_state_select(v_state,v_result::TEXT) INTO v_result;
		END IF;
		RETURN v_result;		
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 删除配置项
CREATE OR REPLACE FUNCTION "dml_ci_delete"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_result JSONB := NULL;
	BEGIN
		WITH cte AS(
			DELETE FROM sys_void 
			 USING jsonb_to_recordset($1) AS t1(uid UUID)
			 WHERE t1.uid = sys_void.uid
			 RETURNING sys_void.*
		)
		SELECT jsonb_agg(cte) INTO v_result FROM cte;			 
		RETURN v_result;		
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 添加类型
CREATE OR REPLACE FUNCTION "dml_class_insert"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_result JSONB := NULL;
	BEGIN
		SELECT dml_class_data_check($1) INTO v_result;
		IF COALESCE((v_result ->> 'flag')::BOOLEAN,TRUE) = TRUE THEN		
			INSERT INTO sys_class(flag,id,name)
			SELECT COALESCE(flag,TRUE),id,name
				FROM jsonb_to_recordset($1) AS (flag BOOLEAN,id VARCHAR,name VARCHAR);					
			WITH cte AS(			
				UPDATE sys_class
					 SET parent_uid = t2.uid
					   , pre_uid = t3.uid
					FROM jsonb_to_recordset($1) AS t1(id VARCHAR,parent_uid UUID,pre_uid UUID,parent_id VARCHAR,pre_id VARCHAR)
					LEFT JOIN sys_class AS t2 ON t1.parent_uid = t2.uid OR t1.parent_id = t2.id
					LEFT JOIN sys_class AS t3 ON t1.pre_uid = t3.uid OR t1.pre_id = t3.id
				 WHERE sys_class.id = t1.id
				 RETURNING sys_class.*
			)
			SELECT jsonb_agg(cte) INTO v_result FROM cte;	
		END IF;
		RETURN v_result;			
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 更新类型
CREATE OR REPLACE FUNCTION "dml_class_update"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_result JSONB := NULL;
	BEGIN
		SELECT dml_class_data_check($1) INTO v_result;
		IF COALESCE((v_result ->> 'flag')::BOOLEAN,TRUE) = TRUE THEN	
			WITH cte AS(
				UPDATE sys_class 
					 SET flag = COALESCE(t1.flag,sys_class.flag)
						 , id = COALESCE(t1.id,sys_class.id)
						 , name = COALESCE(t1.name,sys_class.name)
						 , parent_uid = t1.parent_uid
						 , pre_uid = t1.pre_uid
					FROM jsonb_to_recordset($1) AS t1(uid UUID,flag BOOLEAN,id VARCHAR,name VARCHAR,parent_uid UUID,pre_uid UUID)
				 WHERE t1.uid = sys_class.uid
				 RETURNING sys_class.*
			)
			SELECT jsonb_agg(cte) INTO v_result FROM cte;
		END IF;
		RETURN v_result;			
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 添加类型关联关系
CREATE OR REPLACE FUNCTION "dml_class_relation_insert"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_result JSONB := NULL;
	BEGIN
		SELECT dml_class_relation_check($1) INTO v_result;
		IF COALESCE((v_result ->> 'flag')::BOOLEAN,TRUE) = TRUE THEN
			WITH cte AS(
				INSERT INTO sys_class_relation(flag,id,name,origin_uid,relate_uid)
				SELECT COALESCE(t1.flag,TRUE),t1.id,t1.name,clo.uid,clr.uid
					FROM jsonb_to_recordset ($1) 
						AS t1(flag BOOLEAN,id VARCHAR,name VARCHAR,origin_uid UUID,relate_uid UUID,origin_id VARCHAR,relate_id VARCHAR)
				 INNER JOIN sys_class clo ON t1.origin_uid = clo.uid OR t1.origin_id = clo.id
				 INNER JOIN sys_class clr ON t1.relate_uid = clr.uid OR t1.relate_id = clr.id
				  RETURNING sys_class_relation.*
			)
			SELECT jsonb_agg(cte) INTO v_result FROM cte;	
		END IF;
		RETURN v_result;			
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 更新类型关联关系
CREATE OR REPLACE FUNCTION "dml_class_relation_update"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_result JSONB := NULL;
	BEGIN
		WITH cte AS(
			UPDATE sys_class_relation 
				 SET flag = COALESCE(t1.flag,sys_class_relation.flag)
					 , id = COALESCE(t1.id,sys_class_relation.id)
					 , name = COALESCE(t1.name,sys_class_relation.name)
					 , origin_uid = COALESCE(t1.origin_uid,sys_class_relation.origin_uid)
					 , relate_uid = COALESCE(t1.relate_uid,sys_class_relation.relate_uid)
				FROM jsonb_to_recordset($1) AS t1(uid UUID,flag BOOLEAN,id VARCHAR,name VARCHAR,origin_uid UUID,relate_uid UUID)
			 WHERE t1.uid = sys_class_relation.uid
			 RETURNING sys_class_relation.*
		)
		SELECT jsonb_agg(cte) INTO v_result FROM cte;
		RETURN v_result;			
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 添加类型属性		 
CREATE OR REPLACE FUNCTION "dml_info_insert"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_id VARCHAR := NULL;
	v_uid UUID := NULL;
	v_result JSONB := NULL;
	BEGIN
		SELECT dml_class_check($1) INTO v_result;
		IF COALESCE((v_result ->> 'flag')::BOOLEAN,TRUE) = TRUE THEN
			WITH cte AS(
				INSERT INTO sys_info(flag,id,name,class_uid)
				SELECT COALESCE(t1.flag,TRUE),t1.id,t1.name,cls.uid
					FROM jsonb_to_recordset ($1) AS t1(flag BOOLEAN,id VARCHAR,name VARCHAR,class_uid UUID,class_id VARCHAR)
					LEFT JOIN sys_class AS cls ON t1.class_uid = cls.uid OR t1.class_id = cls.id
					RETURNING sys_info.*
			)
			SELECT jsonb_agg(cte) INTO v_result FROM cte;
		END IF;
		RETURN v_result;			
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 更新类型属性
CREATE OR REPLACE FUNCTION "dml_info_update"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_result JSONB := NULL;
	BEGIN
		WITH cte AS(
			UPDATE sys_info
				 SET flag = COALESCE(t1.flag,sys_info.flag)
					 , id = COALESCE(t1.id,sys_info.id)
					 , name = COALESCE(t1.name,sys_info.name)
					 , class_uid = COALESCE(t1.class_uid,sys_info.class_uid)
				FROM jsonb_to_recordset ($1) AS t1(flag BOOLEAN,uid UUID,id VARCHAR,name VARCHAR,class_uid UUID)
			 WHERE t1.uid = sys_info.uid
			 RETURNING sys_info.*
		)
		SELECT jsonb_agg(cte) INTO v_result FROM cte;
		RETURN v_result;			
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 添加类型属性值约束
CREATE OR REPLACE FUNCTION "dml_infoset_insert"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_id VARCHAR := NULL;
	v_uid UUID := NULL;
	v_result JSONB := NULL;
	BEGIN
		SELECT dml_info_check($1) INTO v_result;
		IF COALESCE((v_result ->> 'flag')::BOOLEAN,TRUE) = TRUE THEN
			WITH cte AS(
				INSERT INTO sys_infoset(flag,id,name,value,info_uid)
				SELECT COALESCE(t1.flag,TRUE),t1.id,t1.name,t1.value,inf.uid
					FROM jsonb_to_recordset ($1) 
						AS t1(flag BOOLEAN,id VARCHAR,name VARCHAR,value VARCHAR,info_uid UUID,info_id VARCHAR)
					LEFT JOIN sys_info AS inf ON t1.info_uid = inf.uid OR t1.info_id = inf.id
					RETURNING sys_infoset.*
			)
			SELECT jsonb_agg(cte) INTO v_result FROM cte;
	  END IF;
		RETURN v_result;		
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 更新类型属性值约束
CREATE OR REPLACE FUNCTION "dml_infoset_update"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_result JSONB := NULL;
	BEGIN
		WITH cte AS(
			UPDATE sys_infoset
				 SET flag = COALESCE(t1.flag,sys_infoset.flag)
					 , id = COALESCE(t1.id,sys_infoset.id)
					 , name = COALESCE(t1.name,sys_infoset.name)
					 , value = COALESCE(t1.value,sys_infoset.value)
					 , info_uid = COALESCE(t1.info_uid,sys_infoset.info_uid)
				FROM jsonb_to_recordset ($1) AS t1(uid UUID,flag BOOLEAN,id VARCHAR,name VARCHAR,value VARCHAR,info_uid UUID)
			 WHERE t1.uid = sys_infoset.uid
			 RETURNING sys_infoset.*
		)
		SELECT jsonb_agg(cte) INTO v_result FROM cte;
		RETURN v_result;			
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 添加实例
CREATE OR REPLACE FUNCTION "dml_ci_insert"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_id VARCHAR := NULL;
	v_uid UUID := NULL;
	v_result JSONB := NULL;
	BEGIN
		SELECT dml_item_data_check($1) INTO v_result;
		IF COALESCE((v_result ->> 'flag')::BOOLEAN,TRUE) = TRUE THEN
				INSERT INTO sys_item(flag,id,name,class_uid)
				SELECT COALESCE(t1.flag,TRUE),t1.id,t1.name,cls.uid 
					FROM jsonb_to_recordset ($1) AS t1(flag BOOLEAN,id VARCHAR,name VARCHAR,class_uid UUID,class_id VARCHAR)
					LEFT JOIN sys_class AS cls ON t1.class_uid = cls.uid OR t1.class_id = cls.id;		
				WITH cte AS(
					UPDATE sys_item
						 SET parent_uid = par.uid,pre_uid = pre.uid
						FROM jsonb_to_recordset ($1) AS t1(id VARCHAR,parent_uid UUID,parent_id VARCHAR,pre_uid UUID,pre_id VARCHAR,class_uid UUID,class_id VARCHAR)
						LEFT JOIN sys_class AS cls ON t1.class_uid = cls.uid OR t1.class_id = cls.id 
						LEFT JOIN sys_item AS par ON t1.parent_uid = par.uid OR t1.parent_id = par.id
						LEFT JOIN sys_item AS pre ON t1.pre_uid = pre.uid OR t1.pre_id = pre.id					 
					 WHERE t1.id = sys_item.id 
						 AND cls.uid = sys_item.class_uid
					 RETURNING sys_item.*
				)
				SELECT jsonb_agg(cte) INTO v_result FROM cte; 
			END IF;
		END IF;
		RETURN v_result;				
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 更新实例
CREATE OR REPLACE FUNCTION "dml_ci_update"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_result JSONB := NULL;
	BEGIN
		SELECT dml_item_data_check($1) INTO v_result;
		IF COALESCE((v_result ->> 'flag')::BOOLEAN,TRUE) = TRUE THEN
			WITH cte AS(
				UPDATE sys_item 
					 SET flag = COALESCE(t1.flag,sys_item.flag)
						 , id = COALESCE(t1.id,sys_item.id)
						 , name = COALESCE(t1.name,sys_item.name)
						 , parent_uid = t1.parent_uid
						 , pre_uid = t1.pre_uid
						 , class_uid = COALESCE(t1.class_uid,sys_item.class_uid)
					FROM jsonb_to_recordset($1) AS t1(uid UUID,flag BOOLEAN,id VARCHAR,name VARCHAR,parent_uid UUID,pre_uid UUID,class_uid UUID)
				 WHERE t1.uid = sys_item.uid
				 RETURNING sys_item.*
			)
			SELECT jsonb_agg(cte) INTO v_result FROM cte;
		END IF;
		RETURN v_result;		
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 添加实例关联关系
CREATE OR REPLACE FUNCTION "dml_ci_relation_insert"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_id VARCHAR := NULL;
	v_uid UUID := NULL;
	v_result JSONB := NULL;
	BEGIN
		SELECT dml_item_relation_check($1) INTO v_result;
		IF COALESCE((v_result ->> 'flag')::BOOLEAN,TRUE) = TRUE THEN	
			INSERT INTO sys_item_relation(flag,origin_uid,relate_uid)
			SELECT COALESCE(t1.flag,FALSE),cio.uid,cir.uid
				FROM jsonb_to_recordset ($1) 
					AS t1(flag BOOLEAN,relation_uid UUID,relation_id VARCHAR,origin_uid UUID,relate_uid UUID,origin_id VARCHAR,relate_id VARCHAR)
			 INNER JOIN sys_class_relation AS rel ON t1.relation_uid = rel.uid OR t1.relation_id = rel.id
			 INNER JOIN sys_item AS cio 
							 ON rel.origin_uid = cio.class_uid
							AND (t1.origin_uid = cio.uid OR t1.origin_id = cio.id)
			 INNER JOIN sys_item AS cir 
							 ON rel.relate_uid = cir.class_uid
							AND (t1.relate_uid = cir.uid OR t1.relate_id = cir.id)
		END IF;
		RETURN v_result;			
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 替换(添加,删除,修改)实例属性
CREATE OR REPLACE FUNCTION "dml_ci_relation_update"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_result JSONB := NULL;
	BEGIN
		WITH cte AS(
			UPDATE sys_item_relation
				 SET flag = COALESCE(t1.flag,sys_item_relation.flag)
					 , origin_uid = COALESCE(t1.origin_uid,sys_item_relation.origin_uid)
					 , relate_uid = COALESCE(t1.relate_uid,sys_item_relation.relate_uid)
				FROM jsonb_to_recordset ($1) AS t1(uid UUID,flag BOOLEAN,origin_uid UUID,relate_uid UUID)
			 WHERE t1.uid = sys_item_relation.uid
			 RETURNING sys_item_relation.*
		)
		SELECT jsonb_agg(cte) INTO v_result FROM cte;
		RETURN v_result;
	END
$BODY$ 
  LANGUAGE 'plpgsql'
;


-- 添加实例属性
CREATE OR REPLACE FUNCTION "dml_ci_info_insert"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_id VARCHAR := NULL;
	v_uid UUID := NULL;
	v_result JSONB := NULL;
	BEGIN
		SELECT dml_item_check($1) INTO v_result;
		IF COALESCE((v_result ->> 'flag')::BOOLEAN,TRUE) = TRUE THEN
			SELECT dml_info_check($1) INTO v_result;
			IF COALESCE((v_result ->> 'flag')::BOOLEAN,TRUE) = TRUE THEN
				SELECT dml_infoset_check($1) INTO v_result;
				IF COALESCE((v_result ->> 'flag')::BOOLEAN,TRUE) = TRUE THEN
					INSERT INTO sys_item_info(flag,class_uid,item_uid,info_uid,infoset_uid,value)
					SELECT COALESCE(t1.flag,TRUE),ci.class_uid,ci.uid,inf.uid,infset.uid,CASE inf.flag WHEN TRUE THEN t1.value ELSE MD5(t1.value) END
						FROM jsonb_to_recordset ($1)
							AS t1(flag BOOLEAN,uid UUID,id VARCHAR,info_uid UUID,info_id VARCHAR,infoset_uid UUID,infoset_id VARCHAR,value TEXT)
					 INNER JOIN sys_class AS cls ON t1.class_uid = cls.uid OR t1.class_id = cls.id
					 INNER JOIN sys_item AS ci
							ON cls.uid = ci.class_uid
						 AND t1.uid = ci.uid OR t1.id = ci.id
						LEFT JOIN sys_info AS inf ON t1.info_uid = inf.uid OR t1.info_id = inf.id
						LEFT JOIN sys_infoset AS infset ON t1.infoset_uid = infset.uid OR t1.infoset_id = infset.id;
					RETURNING sys_item_info.*
				END IF;
			END IF;
		END IF;
		RETURN v_result;
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 更新实例属性
CREATE OR REPLACE FUNCTION "dml_ci_info_update"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_result JSONB := NULL;
	BEGIN
		WITH cte AS(
			UPDATE sys_item_info
				 SET flag = COALESCE(t1.flag,sys_item_info.flag)
					 , infoset_uid = t1.infoset_uid
					 , value = (CASE inf.flag WHEN TRUE THEN t1.value ELSE MD5(t1.value) END)
				FROM jsonb_to_recordset ($1) AS t1(uid UUID,flag BOOLEAN,infoset_uid UUID,value VARCHAR)
			 WHERE t1.uid = sys_item_info.uid
			 RETURNING sys_item_info.*
		)
		SELECT jsonb_agg(cte) INTO v_result FROM cte;
		RETURN v_result;
	END
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 查询函数执行状态
CREATE OR REPLACE FUNCTION "dml_function_state_select"(CHAR(5))
  RETURNS JSONB
AS $BODY$
DECLARE
	BEGIN
		RETURN dml_function_state_select($1,null);			
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

CREATE OR REPLACE FUNCTION "dml_function_state_select"(CHAR(5),JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_result JSONB := NULL;
	BEGIN
		WITH cte AS(
			SELECT t1.uid,t1.flag,t1.state,t1.text,$2 AS data FROM sys_function_state AS t1 WHERE t1.state = $1
		)
		SELECT row_to_json(cte) INTO v_result FROM cte;
		RETURN v_result;				
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

CREATE OR REPLACE FUNCTION "dml_function_state_select"()
  RETURNS JSONB
AS $BODY$
DECLARE
	v_result JSONB := NULL;
	BEGIN
		SELECT jsonb_agg(t1) INTO v_result FROM sys_function_state AS t1;
		RETURN v_result;			
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 添加函数执行状态
CREATE OR REPLACE FUNCTION "dml_function_state_insert"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_result JSONB := NULL;
	BEGIN
		WITH cte AS(
			INSERT INTO sys_function_state(flag,state,text)
			SELECT COALESCE(flag,FALSE),state,text
				FROM jsonb_to_recordset ($1) AS (flag BOOLEAN,state CHAR(5),text VARCHAR)
				RETURNING sys_function_state.*
		)
		SELECT jsonb_agg(cte) INTO v_result FROM cte;
		RETURN v_result;			
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 更新函数执行状态
CREATE OR REPLACE FUNCTION "dml_function_state_update"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_result JSONB := NULL;
	BEGIN
		WITH cte AS(
			UPDATE sys_function_state
				 SET flag = COALESCE(t1.flag,sys_function_state.flag)
					 , state = COALESCE(t1.state,sys_function_state.state)
					 , text = COALESCE(t1.text,sys_function_state.text)
				FROM jsonb_to_recordset ($1) AS t1(uid UUID,flag BOOLEAN,state CHAR(5),text TEXT)
			 WHERE t1.uid = sys_function_state.uid
			 RETURNING sys_function_state.*
		)
		SELECT jsonb_agg(cte) INTO v_result FROM cte;
		RETURN v_result;		
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 查询函数路由表
CREATE OR REPLACE FUNCTION "dml_function_router_select"()
  RETURNS JSONB
AS $BODY$
DECLARE
	v_result JSONB := NULL;
	BEGIN
		SELECT jsonb_agg(t1) INTO v_result FROM sys_function_router AS t1;
		RETURN v_result;			
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 添加函数路由表
CREATE OR REPLACE FUNCTION "dml_function_router_insert"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_result JSONB := NULL;
	BEGIN
		WITH cte AS(			 
			INSERT INTO sys_function_router(flag,id,name,function,input,output,remark)
			SELECT COALESCE(flag,FALSE),id,name,function,input,output,remark
				FROM jsonb_to_recordset ($1) AS (flag BOOLEAN,id INT,name VARCHAR,function VARCHAR,input JSONB,output JSONB,remark TEXT)
				RETURNING sys_function_router.*
		)
		SELECT jsonb_agg(cte) INTO v_result FROM cte;
		RETURN v_result;			
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 更新函数路由表
CREATE OR REPLACE FUNCTION "dml_function_router_update"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_result JSONB := NULL;
	BEGIN
		WITH cte AS(
			UPDATE sys_function_router
				 SET flag = COALESCE(t1.flag,sys_function_router.flag)
					 , id = COALESCE(t1.id,sys_function_router.id)
					 , name = COALESCE(t1.name,sys_function_router.name)
					 , function = COALESCE(t1.function,sys_function_router.function)
					 , input = COALESCE(t1.input,sys_function_router.input)
					 , output = COALESCE(t1.output,sys_function_router.output)
					 , remark = COALESCE(t1.remark,sys_function_router.remark)
				FROM jsonb_to_recordset ($1) AS t1(uid UUID,flag BOOLEAN,id INT,name VARCHAR,function VARCHAR,input TEXT,output JSONB,remark TEXT)
			 WHERE t1.uid = sys_function_router.uid
			 RETURNING sys_function_router.*
		)
		SELECT jsonb_agg(cte) INTO v_result FROM cte;
		RETURN v_result;		
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;
/*
-- 替换(添加,删除,修改)实例属性
CREATE OR REPLACE FUNCTION "func_replace_ci_info"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_result JSONB := NULL;
	BEGIN
		DELETE FROM sys_item_info
		 USING jsonb_to_recordset($1) AS t1(uid UUID)
		 WHERE t1.uid = sys_item_info.item_uid;		 
		RETURN dml_ci_info_insert($1);
	END
$BODY$ 
  LANGUAGE 'plpgsql'
;
-- 查询类型
CREATE OR REPLACE FUNCTION "func_get_class_layout"()
  RETURNS JSONB
AS $BODY$
DECLARE
	BEGIN
		RETURN func_get_class_layout(null);
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

CREATE OR REPLACE FUNCTION "func_get_class_layout"(TEXT)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_result JSONB := NULL;
	BEGIN
		IF $1 IS NULL OR '' = $1 OR '[]' = $1 OR '[{}]' = $1 THEN
			SELECT jsonb_agg(v_class_layout) INTO v_result FROM v_class_layout;	
		ELSE
			SELECT jsonb_agg(rel) INTO v_result 
				FROM jsonb_to_recordset($1::JSONB) AS t1(uid UUID)
			 INNER JOIN v_class_layout AS rel ON t1.uid = rel.uid;	
		END IF;
		RETURN v_result;		
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 查询类型关联关系
CREATE OR REPLACE FUNCTION "func_get_class_relation"()
  RETURNS JSONB
AS $BODY$
DECLARE
	BEGIN
		RETURN func_get_class_relation(null);
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

CREATE OR REPLACE FUNCTION "func_get_class_relation"(TEXT)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_result JSONB := NULL;
	BEGIN	 
		IF $1 IS NULL OR '' = $1 OR '[]' = $1 OR '[{}]' = $1 THEN
			SELECT jsonb_agg(v_class_relation) INTO v_result FROm v_class_relation;
		ELSE
			SELECT jsonb_agg(rel) INTO v_result 
				FROM jsonb_to_recordset($1::JSONB) AS t1(uid UUID)
			 INNER JOIN v_class_relation AS rel ON t1.uid = rel.relation_uid;	
		END IF;
		RETURN v_result;	
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 查询类型属性
CREATE OR REPLACE FUNCTION "func_get_class_info"()
  RETURNS JSONB
AS $BODY$
DECLARE
	BEGIN
		RETURN func_get_class_info(null);
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

CREATE OR REPLACE FUNCTION "func_get_class_info"(TEXT)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_result JSONB := NULL;
	BEGIN
		IF $1 IS NULL OR '' = $1 OR '[]' = $1 OR '[{}]' = $1 THEN
			SELECT jsonb_agg(v_class_info) INTO v_result FROM v_class_info;
		ELSE
			SELECT jsonb_agg(rel) INTO v_result 
				FROM jsonb_to_recordset($1::JSONB) AS t1(uid UUID)
			 INNER JOIN v_class_info AS rel ON t1.uid = rel.uid;	
		END IF;
		RETURN v_result;		
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 根据类型查询实例
CREATE OR REPLACE FUNCTION "func_get_ci_layout"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_result JSONB := NULL;
	BEGIN
		SELECT jsonb_agg(rel) INTO v_result 
			FROM jsonb_to_recordset($1::JSONB) AS t1(class_uid UUID) 
		 INNER JOIN v_ci_layout AS rel ON t1.class_uid = rel.class_uid;		 
		RETURN v_result;		
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 根据类型查询实例属性
CREATE OR REPLACE FUNCTION "func_get_ci_info"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_result JSONB := NULL;
	BEGIN
		SELECT jsonb_agg(rel) INTO v_result
			FROM jsonb_to_recordset($1::JSONB) AS t1(class_uid UUID) 
		 INNER JOIN v_ci_info AS rel ON t1.class_uid = rel.class_uid;	 
		RETURN v_result;		
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 根据类型关联id查询实例关联关系
CREATE OR REPLACE FUNCTION "func_get_ci_relation"(JSONB)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_result JSONB := NULL;
	BEGIN
		SELECT jsonb_agg(rel) INTO v_result
			FROM jsonb_to_recordset($1::JSONB) AS t1(uid UUID) 
		 INNER JOIN v_ci_relation AS rel ON t1.uid = rel.relation_uid;	 
		RETURN v_result;		
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 获取配置项属性对象
CREATE OR REPLACE FUNCTION "func_get_ci_infoset" (UUID) RETURNS JSONB
  VOLATILE
AS $BODY$
DECLARE
  v_result JSONB := NULL;
	BEGIN
		SELECT jsonb_object(array_agg(inf.info_id),array_agg(COALESCE(inf.infoset_value,inf.info_value)))
			INTO v_result
			FROM v_ci_info AS inf
		 WHERE inf.uid = $1::UUID
			 AND inf.info_id IS NOT NULL;
	RETURN v_result;			
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 获取配置项属性列表
CREATE OR REPLACE FUNCTION "func_get_ci_infolist" (UUID) RETURNS JSONB
  VOLATILE
AS $BODY$
DECLARE
  v_result JSONB := NULL;
	BEGIN
		WITH cte AS(
			SELECT info_id AS id,info_name AS name,info_flag AS flag,COALESCE(inf.infoset_value,inf.info_value) AS value
			  FROM v_ci_info AS inf
			 WHERE inf.uid = $1::UUID
			  AND inf.info_uid IS NOT NULL
		)
		SELECT jsonb_agg(cte) INTO v_result FROM cte;
		RETURN v_result;			
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 按顺序获取子节点及属性
CREATE OR REPLACE FUNCTION "func_get_ci_sub_info" (UUID) RETURNS JSONB
  VOLATILE
AS $BODY$
DECLARE
  v_result JSONB := NULL;
	BEGIN
		IF $1 IS NOT NULL THEN
			WITH RECURSIVE cte_list AS (
			-- WITH cte_list AS (
				SELECT ci.id,ci.name,ci.uid,ci.parent_uid,ci.pre_uid,ci.flag
						 , func_get_ci_infoset(ci.uid) AS info
						 , func_get_ci_infolist(ci.uid) AS infoset
						 , func_get_ci_sub_info(ci.uid) AS sub
					FROM sys_item AS ci
				 WHERE ci.parent_uid = $1::UUID 
					 AND ci.pre_uid IS NULL 
				UNION
				SELECT ci.id,ci.name,ci.uid,ci.parent_uid,ci.pre_uid,ci.flag
						 , func_get_ci_infoset(ci.uid) AS info
						 , func_get_ci_infolist(ci.uid) AS infoset
						 , func_get_ci_sub_info(ci.uid) AS sub
					FROM sys_item AS ci
				 INNER JOIN cte_list AS ls ON ci.pre_uid = ls.uid
			)
			SELECT json_agg(cte_list) INTO v_result FROM cte_list;
		END IF;
		RETURN v_result;			
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 获取前置配置项
CREATE OR REPLACE FUNCTION "func_get_ci_previous"(UUID)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_uid UUID := $1;
	v_flag BOOLEAN := TRUE;
	v_result JSONB := NULL;
	BEGIN		
		WHILE TRUE = v_flag LOOP
			PERFORM NULL FROM sys_item WHERE uid = v_uid AND pre_uid IS NOT NULL;
			IF FOUND THEN
				v_flag := FALSE;
			ELSE
				SELECT parent_uid INTO v_uid FROM sys_item WHERE uid = v_uid;
				IF NOT FOUND THEN
					v_flag := FALSE;
				END IF;
			END IF;
		END LOOP;
		WITH cte AS(
			SELECT pre.uid,pre.id,pre.name,pre.flag
				FROM sys_item AS rel
			 INNER JOIN sys_item AS pre ON rel.pre_uid = pre.uid
			 WHERE rel.uid = v_uid
		)
		SELECT json_agg(cte) INTO v_result FROM cte;
		RETURN v_result;			
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 获取后续配置项
CREATE OR REPLACE FUNCTION "func_get_ci_next"(UUID)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_uid UUID := $1;
	v_flag BOOLEAN := TRUE;
	v_result JSONB := NULL;
	BEGIN		
		WHILE TRUE = v_flag LOOP
			PERFORM NULL FROM sys_item WHERE pre_uid = v_uid AND uid IS NOT NULL;
			IF FOUND THEN
				v_flag := FALSE;
			ELSE
				SELECT parent_uid INTO v_uid FROM sys_item WHERE uid = v_uid;
				IF NOT FOUND THEN 
					v_flag := FALSE;
				END IF;
			END IF;
		END LOOP;
		SELECT json_agg(sys_item) INTO v_result FROM sys_item WHERE pre_uid = v_uid;
		RETURN v_result;			
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 按顺序获取子节点
CREATE OR REPLACE FUNCTION "func_get_ci_sub" (UUID) RETURNS JSONB
  VOLATILE
AS $BODY$
DECLARE
  v_result JSONB := NULL;
	BEGIN
		WITH RECURSIVE cte AS (
			SELECT ci.id,ci.name,ci.uid,ci.parent_uid,ci.pre_uid,ci.flag
					 , func_get_ci_sub(ci.uid) AS sub
				FROM sys_item AS ci
			 WHERE ci.parent_uid = $1::UUID 
				 AND ci.pre_uid IS NULL 
			UNION
			SELECT ci.id,ci.name,ci.uid,ci.parent_uid,ci.pre_uid,ci.flag
					 , func_get_ci_sub(ci.uid) AS sub
				FROM sys_item AS ci
			 INNER JOIN cte AS ls ON ci.pre_uid = ls.uid
		)
		SELECT json_agg(cte) INTO v_result FROM cte_list;
		RETURN v_result;			
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 修改(提交)配置项标识信息,
-- 如果包含子配置项,则递归修改(提交)第一个子配置项标识
CREATE OR REPLACE FUNCTION "func_ci_flag_commit"(UUID,BOOLEAN)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_uid UUID := $1;
	v_flag BOOLEAN := COALESCE($2,TRUE);
	v_result JSONB := NULL;
	BEGIN
		UPDATE sys_item SET flag = v_flag WHERE uid = v_uid;		
		PERFORM NULL FROM sys_item WHERE parent_uid = v_uid AND pre_uid IS NULL;
		IF FOUND THEN
			PERFORM func_ci_flag_commit(uid,v_flag) 
				FROM sys_item WHERE parent_uid = v_uid AND pre_uid IS NULL;
		END IF;
		RETURN v_result;
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 修改(回滚)配置项标识信息,
-- 如果有父配置项,且兄弟配置项不需要修改(回滚),则递归修改(回滚)父级配置项标识
CREATE OR REPLACE FUNCTION "func_ci_flag_rollback"(UUID,BOOLEAN)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_uid UUID := $1;
	v_flag BOOLEAN := COALESCE($2,FALSE);
	v_result JSONB := NULL;
	BEGIN
		UPDATE sys_item SET flag = v_flag WHERE uid = v_uid;
		SELECT parent_uid INTO v_uid FROM sys_item WHERE uid = v_uid LIMIT 1;
		IF FOUND THEN
			PERFORM NULL FROM sys_item WHERE parent_uid = v_uid AND flag = NOT(v_flag);
			IF NOT FOUND THEN
				PERFORM func_ci_flag_rollback(v_uid,v_flag);
			END IF;
		END IF;
		RETURN v_result;
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;
*/

-- 数据库对外唯一接口
/*
SELECT jsonb_pretty(func_run(102,null,null)::JSONB);
*/
CREATE OR REPLACE FUNCTION "func_run"(INT,VARCHAR,TEXT)
  RETURNS TEXT
AS $BODY$
DECLARE
	v_log_flag BOOLEAN := FALSE;
	v_function VARCHAR(40) := NULL;
	v_sqlcmd TEXT := NULL;
	v_data JSONB := NULL;
	v_result JSONB := '[{}]';
	v_code_flag TEXT[] := '{0,flag}';
	v_code_key TEXT[] := '{0,state}';
	v_text_key TEXT[] := '{0,text}';
  v_data_key TEXT[] := '{0,data}';
	BEGIN
		SELECT flag, function INTO v_log_flag, v_function FROM sys_function_router WHERE id = $1;		
		v_sqlcmd := FORMAT('SELECT ' || v_function, quote_literal($2), quote_literal($3));
		v_result := jsonb_set(v_result, v_code_flag, to_jsonb(TRUE));
		EXECUTE v_sqlcmd INTO v_data;
		IF v_data IS NOT NULL THEN			
			IF COALESCE((v_data->>'flag')::BOOLEAN, TRUE) = TRUE THEN
				v_result := jsonb_set(v_result, v_data_key, v_data);
			ELSE
				v_result := v_data;				
			END IF;
		END IF;

		IF TRUE = v_log_flag THEN
			INSERT INTO sys_function_log(flag, time, id, function, account_uid, input, output)
			VALUES((v_result->0->>'flag')::BOOLEAN, now(), $1, v_function, $2, $3, v_result->0);
		END IF;
		RETURN v_result->>0;

		EXCEPTION 
			WHEN QUERY_CANCELED THEN
				v_result := jsonb_set(v_result, v_code_flag, to_jsonb(FALSE));
				v_result := jsonb_set(v_result, v_code_key, to_jsonb(SQLSTATE));
				v_result := jsonb_set(v_result, v_text_key, to_jsonb(SQLERRM));
				INSERT INTO sys_function_log(flag, time, id, function, account_uid, input, output)
				VALUES(FALSE, now(), $1, v_function, $2, $3, v_result-> 0);
				RETURN v_result->>0;
			WHEN OTHERS THEN
				v_result := jsonb_set(v_result, v_code_flag, to_jsonb(FALSE));
				v_result := jsonb_set(v_result, v_code_key, to_jsonb(SQLSTATE));
				v_result := jsonb_set(v_result, v_text_key, to_jsonb(SQLERRM));
				INSERT INTO sys_function_log(flag, time, id, function, account_uid, input, output)
				VALUES(FALSE, now(), $1, v_function, $2, $3, v_result-> 0);
				RETURN v_result->>0;		
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;