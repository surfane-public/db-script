/*

-- 数据库  :平台通用
-- 脚本类型:表创建脚本

-- 创建信息 --
-- 作者 :surfane
-- 版本 :V 1.0
-- 时间 :2017-10-10

-- 修订信息 --
-- 修订者 :
-- 版本 :
-- 时间 :
-- 修订概要:

*/

-- 创建数据库
/*
CREATE DATABASE "portal";
*/
-- 启用 uuid 功能
DROP EXTENSION IF EXISTS "uuid-ossp" CASCADE;
CREATE EXTENSION "uuid-ossp";

-- 1.根表,所有表均继承于此
-- flag:ture用于常规情况,或大都数情况.
DROP TABLE IF EXISTS "sys_void" CASCADE;
CREATE TABLE "sys_void" (
"uid" UUID DEFAULT uuid_generate_v4() NOT NULL,
"flag" BOOLEAN DEFAULT TRUE NOT NULL,
CONSTRAINT "pk_sys_void" PRIMARY KEY ("uid")
)
;

-- 2.基表,用于初始化公共字段
DROP TABLE IF EXISTS "sys_template" CASCADE;
CREATE TABLE "sys_template" (
"id" VARCHAR(40) NOT NULL,
"name" VARCHAR(40) NOT NULL,
CONSTRAINT "pk_sys_template" PRIMARY KEY ("uid")
)
INHERITS ("sys_void") 
;

-- 3.类型表,配置系统类型信息,实例表类型配置于此
-- flag标识是否允许修改,同时对应的实例表id是否允许为空
-- true:系统分类,不可修改,实例表id不允许为空;
-- false:非系统分类,可修改,实例表id允许为空;
DROP TABLE IF EXISTS "sys_class" CASCADE;
CREATE TABLE "sys_class" (
"parent_uid" UUID NULL,
"pre_uid" UUID NULL,
CONSTRAINT "pk_sys_class" PRIMARY KEY ("uid"),
CONSTRAINT "uq_sys_class_id" UNIQUE ("id"),
CONSTRAINT "uq_sys_class_name" UNIQUE ("name"),
CONSTRAINT "fk_sys_class_parent" FOREIGN KEY ("parent_uid") REFERENCES "sys_class" ("uid") ON DELETE SET NULL ON UPDATE CASCADE,
CONSTRAINT "fk_sys_class_pre" FOREIGN KEY ("pre_uid") REFERENCES "sys_class" ("uid") ON DELETE SET NULL ON UPDATE CASCADE
)
INHERITS ("sys_template") 
;

-- 4.类型关联关系表,用于说明关联关系(表)的含意
-- flag标识是否n:n对应关系,同时对应的实例关系表origin_uid与relate_uid是否为n:n
-- true:origin_uid:relate_uid为n:n,UNIQUE(origin_uid,relate_uid)
-- false:origin_uid:relate_uid为1:n,UNIQUE(relate_uid)
DROP TABLE IF EXISTS "sys_class_relation" CASCADE;
CREATE TABLE "sys_class_relation" (
  "origin_uid" UUID NOT NULL,
  "relate_uid" UUID NOT NULL,
  CONSTRAINT "pk_sys_class_relation" PRIMARY KEY ("uid"),
	CONSTRAINT "uq_sys_class_relation_id" UNIQUE ("id"),
  CONSTRAINT "uq_sys_class_relation_name" UNIQUE ("name"),
  CONSTRAINT "uq_sys_class_relation_uid" UNIQUE ("origin_uid", "relate_uid"),
  CONSTRAINT "fk_sys_class_relation_origin_uid" FOREIGN KEY ("origin_uid") REFERENCES "sys_class" ("uid") ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT "fk_sys_class_relation_relate_uid" FOREIGN KEY ("relate_uid") REFERENCES "sys_class" ("uid") ON DELETE NO ACTION ON UPDATE CASCADE
)
INHERITS ("sys_template")
;

-- 5.属性表,存放类型的属性集合
-- flag用于标识是否明文保存
-- true:明文保存
-- false:加密保存
DROP TABLE IF EXISTS "sys_info" CASCADE;
CREATE TABLE "sys_info" (
"class_uid" UUID NOT NULL,
CONSTRAINT "pk_sys_info" PRIMARY KEY ("uid"),
CONSTRAINT "uq_sys_info_id" UNIQUE ("id"),
CONSTRAINT "uq_sys_info_name" UNIQUE ("name"),
CONSTRAINT "fk_sys_info_class" FOREIGN KEY ("class_uid") REFERENCES "sys_class" ("uid") ON DELETE NO ACTION ON UPDATE CASCADE
)
INHERITS ("sys_template") 
;

-- 6.属性值约束表,存放类型属性约束集合
-- flag标识是否可用
DROP TABLE IF EXISTS "sys_infoset" CASCADE;
CREATE TABLE "sys_infoset" (
"value" VARCHAR(40) NOT NULL,
"info_uid" UUID NOT NULL,
CONSTRAINT "pk_sys_infoset" PRIMARY KEY ("uid"),
CONSTRAINT "uq_sys_infoset_uid" UNIQUE ("info_uid","uid"),
CONSTRAINT "uq_sys_infoset_id" UNIQUE ("info_uid", "id"),
CONSTRAINT "uq_sys_infoset_name" UNIQUE ("info_uid", "name"),
CONSTRAINT "fk_sys_infoset_info" FOREIGN KEY ("info_uid") REFERENCES "sys_info" ("uid") ON DELETE CASCADE ON UPDATE CASCADE
)
INHERITS ("sys_template") 
;

-- 7.实例表,所有实例表继承于此
-- flag标识是否可用
DROP TABLE IF EXISTS "sys_item" CASCADE;
CREATE TABLE "sys_item" (
"parent_uid" UUID NULL,
"pre_uid" UUID NULL,
"class_uid" UUID NOT NULL,
CONSTRAINT "pk_sys_item" PRIMARY KEY ("uid"),
CONSTRAINT "uq_sys_item_id" UNIQUE ("class_uid","id"),
CONSTRAINT "uq_sys_item_name" UNIQUE ("class_uid","name"),
CONSTRAINT "fk_sys_item_parent" FOREIGN KEY ("parent_uid") REFERENCES "sys_item" ("uid") ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT "fk_sys_item_pre" FOREIGN KEY ("pre_uid") REFERENCES "sys_item" ("uid") ON DELETE SET NULL ON UPDATE CASCADE,
CONSTRAINT "fk_sys_item_class" FOREIGN KEY ("class_uid") REFERENCES "sys_class" ("uid") ON DELETE CASCADE ON UPDATE CASCADE
)
INHERITS ("sys_template") 
;

-- 8.实例关联表,被继承为存放实例与实例的关联关系
-- flag 用于深入关系管理,
-- 比如角色与功能对应关系,可以根据flag来进行编辑与只读判定
-- (无关联:没有权限?);flag=true,只读;flag=false,可执行
DROP TABLE IF EXISTS "sys_item_relation" CASCADE;
CREATE TABLE "sys_item_relation" (
"relation_uid" UUID NOT NULL,
"origin_uid" UUID NOT NULL,
"relate_uid" UUID NOT NULL,
CONSTRAINT "pk_sys_item_relation" PRIMARY KEY ("uid"),
CONSTRAINT "uq_sys_item_relation" UNIQUE ("origin_uid","relate_uid"),
CONSTRAINT "fk_sys_item_relation_uid" FOREIGN KEY ("relation_uid") REFERENCES "sys_class_relation" ("uid") ON DELETE CASCADE ON UPDATE CASCADE
)
INHERITS ("sys_void") 
;

-- 9.实例属性表,被继承为存放实例对应的属性及属性值关联信息
-- value值需为text,便于存放较大的数据,如json
DROP TABLE IF EXISTS "sys_item_info" CASCADE;
CREATE TABLE "sys_item_info" (
"class_uid" UUID NOT NULL,
"item_uid" UUID NOT NULL,
"info_uid" UUID NOT NULL,
"infoset_uid" UUID NULL,
"value" TEXT NULL,
CONSTRAINT "pk_sys_item_info" PRIMARY KEY ("uid"),
CONSTRAINT "uq_sys_item_info" UNIQUE ("item_uid", "info_uid"),
CONSTRAINT "fk_sys_item_info_class" FOREIGN KEY ("class_uid") REFERENCES "sys_class" ("uid") ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT "fk_sys_item_info_info" FOREIGN KEY ("info_uid") REFERENCES "sys_info" ("uid") ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT "fk_sys_item_info_infoset" FOREIGN KEY ("info_uid","infoset_uid") REFERENCES "sys_infoset" ("info_uid","uid") ON DELETE SET NULL ON UPDATE CASCADE
)
INHERITS ("sys_void") 
;

-- 10.函数路由表
-- 数据库对外提供统一接口,此表根据id提供具体的实现函数
DROP TABLE IF EXISTS "sys_funcion_router" CASCADE;
CREATE TABLE "sys_function_router" (
"id" INT NOT NULL,
"name" VARCHAR(40) NOT NULL,
"function" VARCHAR(40) NOT NULL,
"input" TEXT NULL,
"output" JSONB NULL,
"remark" TEXT NULL,
CONSTRAINT "pk_sys_function_router" PRIMARY KEY ("uid"),
CONSTRAINT "uq_sys_function_router" UNIQUE ("id")
)
INHERITS ("sys_void") 
;

-- 11.函数执行状态代码
-- 记录sql执行不出错,而业务逻辑异常的错误信息
-- 如果sql执行会出错的,请使用系统sql_state进行错误处理
-- flag表示是否记录日志,true:不记录日记;false:记录日志
DROP TABLE IF EXISTS "sys_function_state" CASCADE;
CREATE TABLE "sys_function_state" (
"state" CHAR(5) NOT NULL,
"text" VARCHAR(40) NOT NULL,
CONSTRAINT "pk_sys_function_state" PRIMARY KEY ("uid"),
CONSTRAINT "uq_sys_function_state" UNIQUE ("state"),
CONSTRAINT "ck_sys_function_state" CHECK (LENGTH("state") = 5)
)
INHERITS ("sys_void") 
;

-- 12.函数调用log表
DROP TABLE IF EXISTS "sys_function_log" CASCADE;
CREATE TABLE "sys_function_log" (
"time" TIMESTAMPTZ(6) DEFAULT NOW() NOT NULL,
"id" INT NOT NULL,
"function" VARCHAR(40) NOT NULL,
"account_uid" VARCHAR(40) NULL,
"input" TEXT NULL,
"output" JSONB NULl,
CONSTRAINT "pk_sys_function_log" PRIMARY KEY ("uid")
)
INHERITS ("sys_void") 
;

-- 13.配置项数量变更表
-- flag:标识是否实际发生(加入统计)
DROP TABLE IF EXISTS "sys_item_state" CASCADE;
CREATE TABLE "sys_item_state" (
"class_uid" UUID NOT NULL,
"item_uid" UUID NOT NULL,
"event_uid" UUID NOT NULL,
"value" FLOAT NOT NULL,
CONSTRAINT "pk_sys_item_state" PRIMARY KEY ("uid"),
CONSTRAINT "uq_sys_item_state" UNIQUE ("item_uid","event_uid"),
CONSTRAINT "fk_sys_item_state_class" FOREIGN KEY ("class_uid") REFERENCES "sys_class" ("uid") ON DELETE CASCADE ON UPDATE CASCADE
)
INHERITS ("sys_void") 
;