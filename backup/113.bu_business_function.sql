/*

-- 数据库  :苏州普华电力工程有限公司施工项目管理系统数据库
-- 脚本类型:业务函数逻辑脚本

-- 创建信息 --
-- 作者    :杨永红
-- 版本    :V 1.0
-- 时间    :2017-05-10

-- 修订信息 --
-- 修订者  :
-- 版本    :
-- 时间    :
-- 修订概要:

*/

-- 获取之前活动
/*test script
	SELECT func_previous_process(uid) FROM t_bu_process_state;
*/
CREATE OR REPLACE FUNCTION "func_previous_process"(UUID)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_uid UUID := $1;
	v_flag BOOLEAN := TRUE;
	v_result JSONB := '{"result_state":"00000","result_text":"successful completion"}'::JSONB;
	BEGIN		
		WHILE TRUE = v_flag LOOP
			PERFORM pre_uid FROM t_bu_process_state WHERE uid = v_uid;
			IF FOUND THEN
				v_flag := FALSE;
			ELSE
				SELECT parent_uid INTO v_uid FROM t_bu_process_state WHERE uid = v_uid;
				IF NOT FOUND THEN
					v_flag := FALSE;
				END IF;
			END IF;
		END LOOP;
		SELECT json_agg(t0) INTO v_result FROM (SELECT pre_uid FROM t_bu_process_state WHERE uid = v_uid)AS t0;

		RETURN v_result;			
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 获取后续活动
/*test script
	SELECT func_next_process(uid) FROM t_bu_process_state WHERE id = 'documented';
*/
CREATE OR REPLACE FUNCTION "func_next_process"(UUID)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_uid UUID := $1;
	v_count INTEGER := 0;
	v_flag BOOLEAN := TRUE;
	v_result JSONB := '{"result_state":"00000","result_text":"successful completion"}'::JSONB;
	BEGIN		
		WHILE TRUE = v_flag LOOP
			PERFORM uid FROM t_bu_process_state WHERE pre_uid = v_uid;
			IF FOUND THEN
				v_flag := FALSE;
			ELSE
				SELECT parent_uid INTO v_uid FROM t_bu_process_state WHERE uid = v_uid;
				IF NOT FOUND THEN 
					v_flag := FALSE;
				ELSE
					SELECT count(0) INTO v_count FROM t_bu_process_state WHERE isactive = TRUE AND parent_uid = v_uid;
					IF 1 < v_count THEN
						v_uid := NULL;
						v_flag := FALSE;
					END IF;
				END IF;
			END IF;
		END LOOP;

		SELECT json_agg(t0) INTO v_result FROM (SELECT uid FROM t_bu_process_state WHERE pre_uid = v_uid)AS t0;
		
raise notice '%d',v_result;
		RETURN v_result;			
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 获取单个流程process的信息
/*
	select json_agg(func_process_info(uid)) FROM t_bu_process_state where id ='material plan'
SELECT func_process_info('cfa5ea35-89a0-4694-9520-411b3e9ae4f2')

*/
CREATE OR REPLACE FUNCTION "func_process_info" (UUID)  RETURNS JSONB
  VOLATILE
AS $BODY$
DECLARE
		v_uid UUID := $1::UUID;
    v_result jsonb := NULL;
BEGIN
	WITH doc AS(
		SELECT doc.uid,doc.id,doc.name
			FROM t_bu_document_state ds
		 INNER JOIN t_ci_document doc ON doc.uid = ds.document_uid
		 WHERE ds.process_uid = v_uid
	)
	, CTE AS(
		SELECT bu.uid,bu.id,bu.name,bu.owner,bu.isapprove,bu.isactive,bu.active_time,bu.parent_uid,bu.pre_uid
					,jsonb_object(array_agg(coalesce(attribute_id,'')),array_agg(coalesce(attributeset_value,attribute_value))) AS info
					,(SELECT json_agg(doc) FROM doc) AS document
					-- ,row_to_json(ci)::JSONB - 'class_uid'::TEXT-'valid'::TEXT -'parent_uid'::TEXT -'pre_uid'::TEXT AS owner
			FROM t_bu_process_state AS bu 
			LEFT JOIN v_flow_attribute AS fa ON bu.flow_uid = fa.uid
			-- LEFT JOIN t_cfg_item AS ci ON bu.owner = ci.uid
			
			-- LEFT JOIN t_bu_document_state c1 ON c1.process_uid=bu.uid
			-- LEFT JOIN t_ci_document c2 ON c2.uid =c1.document_uid
		 WHERE bu.uid = v_uid
		 GROUP BY bu.uid,bu.id,bu.name,bu.owner,bu.isapprove,bu.isactive,bu.active_time,bu.parent_uid,bu.pre_uid
		 -- GROUP BY bu.uid,bu.id,bu.name,bu.owner,bu.isactive,bu.active_time,ci.uid,c2.id,c2.name--,ci.id,ci.name
	)
	SELECT row_to_json(CTE) INTO v_result FROM CTE;

	RETURN v_result;
END;
$BODY$ LANGUAGE plpgsql
;

-- SELECT (row_to_json('s' AS q),row_to_json('d' AS ee)) AS aaa
-- 
-- WITH a as(
-- SELECT 'd' as id,'a' as name
-- )SELECT  json_agg(a) AS DOCUMENT from a
--SELECT json_agg(s) FROM (SELECT 'd' as id,'a' as name) AS s


-- 获取流程层级图
/*test script
	SELECT * FROM t_bu_process_state WHERE parent_uid IS NULL AND pre_uid IS NULL;
	SELECT func_process_navigation(uid) FROM t_bu_process_state WHERE parent_uid IS NULL AND pre_uid IS NULL;

SELECT func_process_info(uid)  FROM t_bu_process_state

*/
CREATE OR REPLACE FUNCTION "func_process_navigation" (UUID)  RETURNS JSONB
  VOLATILE
AS $BODY$
DECLARE
		v_uid UUID := $1;
    v_result jsonb := NULL;
BEGIN
		WITH RECURSIVE CTE AS (
			SELECT uid,parent_uid,pre_uid,id,name
						,func_process_navigation(uid) AS sub
						,func_process_info(uid) AS process
				FROM t_bu_process_state
			 WHERE parent_uid = v_uid 
				 AND pre_uid IS NULL 
			UNION
			SELECT t.uid,t.parent_uid,t.pre_uid,t.id,t.name
						,func_process_navigation(t.uid) AS sub
						,func_process_info(t.uid) AS process
				FROM t_bu_process_state t,CTE 
			 WHERE t.pre_uid = CTE.uid
		),
		CTE2 AS(
			SELECT uid,sub,process FROM CTE
		)
		SELECT json_agg (CTE2) sub into v_result 
		 FROM CTE2;

	RETURN v_result;
END;
$BODY$ LANGUAGE plpgsql
;

-- 获取单个项目资源信息
-- select func_project_resource(uid) from t_ci_project;
CREATE OR REPLACE FUNCTION "func_project_resource" (UUID)  RETURNS JSONB
  VOLATILE
AS $BODY$
DECLARE
		v_uid UUID := $1;
    v_result jsonb := NULL;
BEGIN
	WITH CTE AS(
	 SELECT cr.uid,cr.id,cr.name,cr.valid
				 ,coalesce(fa.attributeset_value,fa.attribute_value) AS stage
				 ,jsonb_object(array_agg(coalesce(ca.attribute_id,'')),array_agg(coalesce(ca.attributeset_value,ca.attribute_value))) AS info
				 , rs.quantity as quantity
		 FROM t_bu_resource_state AS rs
		 INNER JOIN t_ci_resource AS cr ON rs.resource_uid = cr.uid
		 INNER JOIN t_bu_process_state AS ps ON rs.process_uid = ps.uid
			LEFT JOIN v_flow_attribute AS fa ON ps.flow_uid = fa.uid 
			LEFT JOIN v_ci_attribute AS ca ON ca.item_uid = cr.uid
		WHERE ps.project_uid = v_uid
			AND fa.attribute_id = 'stage'
		GROUP by cr.uid,cr.id,cr.name,coalesce(fa.attributeset_value,fa.attribute_value),quantity
	),
	CTE1 AS(
		SELECT uid,id,name,valid,info,jsonb_object(array_agg(stage),array_agg(quantity::TEXT)) AS quantity
			FROM CTE	
		 GROUP BY uid,id,name,valid,info
	)
	SELECT json_agg (CTE1) INTO v_result FROM CTE1;
			 
	RETURN v_result;
END;
$BODY$ LANGUAGE plpgsql
;

-- 获取单个项目文档信息
-- select func_project_document('c8592c6a-282c-4e1c-bae0-24db7a714f8f');
CREATE OR REPLACE FUNCTION "func_project_document" (UUID)  RETURNS JSONB
  VOLATILE
AS $BODY$
DECLARE
		v_uid UUID := $1;
    v_result jsonb := NULL;
BEGIN
	WITH CTE AS(
	 SELECT cd.uid,cd.id,cd.name
				 ,coalesce(fa.attributeset_value,fa.attribute_value) AS stage
				 ,jsonb_object(array_agg(coalesce(ca.attribute_id,'')),array_agg(coalesce(ca.attributeset_value,ca.attribute_value))) AS info
		 FROM t_bu_document_state AS ds
		 INNER JOIN t_ci_document AS cd ON ds.document_uid = cd.uid
		 INNER JOIN t_bu_process_state AS ps ON ds.process_uid = ps.uid
			LEFT JOIN v_flow_attribute AS fa ON ps.flow_uid = fa.uid
			LEFT JOIN v_ci_attribute AS ca ON cd.uid = ca.item_uid
		WHERE ps.project_uid = v_uid
			AND fa.attribute_id = 'edit_item'
			GROUP by cd.uid,cd.id,cd.name,stage    --添加group
	),
	 CTE1 AS(
	 SELECT stage,json_agg(CTE)
		 FROM CTE
		GROUP BY stage
	)
	SELECT json_agg (CTE1) into v_result FROM CTE1;
			 
	RETURN v_result;
END;
$BODY$ LANGUAGE plpgsql
;

-- 获取单个项目日志
/*
 select func_project_state('e7f0cf24-e32a-4315-a649-01745ac84b31');
*/
CREATE OR REPLACE FUNCTION "func_project_state" (UUID)  RETURNS JSONB
  VOLATILE
AS $BODY$
DECLARE
		v_project_uid UUID := $1::UUID;
    v_result jsonb := NULL;
BEGIN
	WITH CTE AS(
	 SELECT pcs.uid AS process_uid,pcs.id AS process_id,pcs.name AS process_name
				 ,ca.uid AS account_uid,ca.id AS account_id,ca.id AS account_name
				 ,pjs.time,CASE pjs.approve WHEN FALSE THEN '驳回' ELSE '' END AS approve,pjs.remark
		FROM t_bu_project_state AS pjs
	 INNER JOIN t_bu_process_state AS pcs ON pjs.process_uid = pcs.uid
	 INNER JOIN t_ci_account AS ca ON pjs.account_uid = ca.uid
	 WHERE pcs.project_uid = v_project_uid
	)
	SELECT json_agg (CTE) into v_result FROM CTE;
			 
	RETURN v_result;
END;
$BODY$ LANGUAGE plpgsql
;

-- 获取单个工程信息(基本属性+流程信息+资源统计+文档信息)
-- select func_project_info('af57cbf0-fbd5-4145-b356-6608716d708a');
CREATE OR REPLACE FUNCTION "func_project_info"(UUID)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_project_uid UUID := $1;
	v_process_uid UUID := NULL;
	v_result JSONB := '{"result_state":"00000","result_text":"successful completion"}'::JSONB;
	BEGIN
		-- top process uid:
		SELECT uid INTO v_process_uid 
		  FROM t_bu_process_state 
		 WHERE project_uid = v_project_uid 
			 AND parent_uid IS NULL
			 AND pre_uid IS NULL
		 LIMIT 1;
		-- project attribute 
		WITH CTE AS(
			SELECT item_uid AS uid,item_id AS id,item_name AS name
						,jsonb_object(array_agg(attribute_id),array_agg(coalesce(attributeset_value,attribute_value)))AS info
						,func_process_navigation(v_process_uid) AS process
						,func_project_resource(v_project_uid) AS resource
						,func_project_document(v_project_uid) AS document
						,func_project_state(v_project_uid) AS state
				FROM v_ci_attribute
			 WHERE item_uid = v_project_uid
			 GROUP BY item_uid,item_id,item_name
		)
		SELECT row_to_json(CTE) INTO v_result FROM CTE LIMIT 1;
		RETURN v_result;			
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 创建建工程
/*test script
	SELECT func_project_init('{"id":"testProjectID100","name":"testProjectName100","class_uid":"9cbf652a-a6e7-4612-8591-8ef1b689811b"}');
	TRUNCATE table t_ci_project CASCADE
*/
CREATE OR REPLACE FUNCTION "func_project_init"(JSONB)
  RETURNS UUID
AS $BODY$
DECLARE
	v_data JSONB := $1::JSONB;
	v_project_uid UUID := NULL;
	v_result UUID := NULL;
	BEGIN
		-- create project
		PERFORM func_create_ci(jsonb_agg(v_data));
		-- get project uid
		SELECT cp.uid INTO v_project_uid
			FROM t_ci_project AS cp
			INNER JOIN jsonb_to_record(v_data) AS t(id VARCHAR) ON t.id = cp.id;
		-- create process
		WITH CTE AS(
			SELECT uuid_generate_v4() AS process_uid,* from t_ci_flow
		)
		INSERT INTO t_bu_process_state(uid,id,name,owner,project_uid,flow_uid,parent_uid,pre_uid,isapprove)
		SELECT t0.process_uid AS uid,t0.id,t0.name,t3.relate_uid AS owner
					,v_project_uid AS project_uid
					,t0.uid AS flow_uid
					,t1.process_uid AS parent_uid
					,t2.process_uid AS pre_uid
					,t0.valid
			FROM CTE t0
			LEFT JOIN CTE t1 ON t0.parent_uid = t1.uid
			LEFT JOIN CTE t2 ON t0.pre_uid = t2.uid
			LEFT JOIN t_rel_flow_role t3 ON t0.uid = t3.origin_uid;
		--create attribute
		INSERT INTO t_attr_project(class_uid,item_uid,attribute_uid)
		SELECT cp.class_uid,cp.uid,attr.uid
			FROM t_ci_project AS cp 
		 INNER JOIN t_cfg_attribute AS attr ON cp.class_uid = attr.class_uid
		 WHERE cp.uid = v_project_uid;
		
		SELECT uid INTO v_result 
			FROM t_bu_process_state 
		 WHERE project_uid = v_project_uid
			 AND parent_uid IS NULL
			 AND pre_uid IS NULL
		 LIMIT 1;
	
		RETURN v_result;			
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;