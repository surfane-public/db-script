/*
	system test data;
*/
-- role item
SELECT dml_ci_insert ('[
		{"id":"role_system_administrator","name":"系统管理员","class_id":"role"},
		{"id":"role_administrator","name":"超级管理员","class_id":"role"},
		{"id":"role_visitor","name":"游客","class_id":"role"},
		{"id":"role_user","name":"普通用户","class_id":"role"}
]');

-- account item
SELECT dml_ci_insert ('[
		{"id":"SysAdmin","name":"系统管理员帐号","class_id":"account"},
		{"id":"Admin","name":"超级管理员帐号","class_id":"account"},
		{"id":"Visitor","name":"游客帐号","class_id":"account"},
		{"id":"User","name":"普通用户帐号","class_id":"account"}
]');

-- ui item
SELECT dml_ci_insert ('[
		{"id":"ui_system_config","name":"系统运行配置功能","class_id":"ui"},
		{"id":"ui_energy","name":"电力能源管理视图","class_id":"ui"},
		{"id":"ui_data","name":"实时数据视图","class_id":"ui"},
		{"id":"ui_chart","name":"一次系统图视图","class_id":"ui"},
		{"id":"ui_byq","name":"变压器监视视图","class_id":"ui"},
		{"id":"ui_dyg","name":"低压柜监测视图","class_id":"ui"},
		{"id":"ui_report","name":"能源健康报告视图","class_id":"ui"},
		{"id":"ui_anasys","name":"客户自定义报表视图","class_id":"ui"}
]');
	
-- menu item
SELECT dml_ci_insert ('[
		{"id":"menu_portal","name":"平台菜单","class_id":"menu"},
		{"id":"menu_system_config","name":"系统运行配置","class_id":"menu","parent_id":"menu_portal"},
		{"id":"menu_energy","name":"电力能源管理","class_id":"menu","parent_id":"menu_portal","pre_id":"menu_system_config"},
		{"id":"menu_data","name":"实时数据","class_id":"menu","parent_id":"menu_energy"},		
		{"id":"menu_chart","name":"一次系统图","class_id":"menu","parent_id":"menu_energy","pre_id":"menu_data"},
		{"id":"menu_byq","name":"变压器监视","class_id":"menu","parent_id":"menu_energy","pre_id":"menu_chart"},
		{"id":"menu_dyg","name":"低压柜监测","class_id":"menu","parent_id":"menu_energy","pre_id":"menu_byq"},
		{"id":"menu_report","name":"能源健康报告","class_id":"menu","parent_id":"menu_energy","pre_id":"menu_dyg"},
		{"id":"menu_anasys","name":"客户自定义报表","class_id":"menu","parent_id":"menu_energy","pre_id":"menu_report"}
]');

/*
	system item info test data;
	true:encode
	false:no-encode
*/
-- account item info
SELECT dml_ci_info_insert('[
	{"class_id":"account","id":"SysAdmin","info_id":"password","info_value":"SysAdmin"},
	{"class_id":"account","id":"SysAdmin","info_id":"birthday","info_value":"2001-01-01"},
	{"class_id":"account","id":"SysAdmin","info_id":"name","info_value":"系统管理员姓名"},

	{"class_id":"account","id":"Admin","info_id":"password","info_value":"Admin"},
	{"class_id":"account","id":"Admin","info_id":"birthday","info_value":"2002-02-02"},
	{"class_id":"account","id":"Admin","info_id":"name","info_value":"超级管理员姓名"},

	{"class_id":"account","id":"Visitor","info_id":"password","info_value":"Visitor"},
	{"class_id":"account","id":"Visitor","info_id":"birthday","info_value":"2003-03-03"},
	{"class_id":"account","id":"Visitor","info_id":"name","info_value":"游客姓名"},

	{"class_id":"account","id":"User","info_id":"password","info_value":"User"},
	{"class_id":"account","id":"User","info_id":"birthday","info_value":"2004-04-04"},
	{"class_id":"account","id":"User","info_id":"name","info_value":"普通用户姓名"}
]');
-- function item info
SELECT dml_ci_info_insert('[
	{"class_id":"ui","id":"ui_dyg","info_id":"page_url","info_value":"page/lowVoltageCabinet.html"},
	{"class_id":"ui","id":"ui_dyg","info_id":"target","info_value":"div"},
	
	{"class_id":"ui","id":"ui_system_config","info_id":"page_url","info_value":"page/DiagramEditor.html"},
	{"class_id":"ui","id":"ui_system_config","info_id":"target","info_value":"div"},
		{"class_id":"ui","id":"ui_report","info_id":"page_url","info_value":"baidu.com"},
	{"class_id":"ui","id":"ui_report","info_id":"target","info_value":"iframe"},

	{"class_id":"ui","id":"ui_anasys","info_id":"page_url","info_value":"163.com"},
	{"class_id":"ui","id":"ui_anasys","info_id":"target","info_value":"iframe"}
]');
-- menu item info
SELECT dml_ci_info_insert('[
		{"class_id":"menu","id":"menu_system_config","info_id":"hint","info_value":"系统配置"},
		{"class_id":"menu","id":"menu_system_config","info_id":"icon","info_value":""},		{"class_id":"menu","id":"menu_system_config","info_id":"shortcut_icon","info_value":"image/icons/logo_Diag.ico"},
		{"class_id":"menu","id":"menu_system_config","info_id":"topic_icon","info_value":"fa fa-cog"},
		
		{"class_id":"menu","id":"menu_energy","info_id":"hint","info_value":"电力能源管理"},
		{"class_id":"menu","id":"menu_energy","info_id":"icon","info_value":""},
		{"class_id":"menu","id":"menu_energy","info_id":"shortcut_icon","info_value":"image/icons/logo_Diag.ico"},
		{"class_id":"menu","id":"menu_energy","info_id":"topic_icon","info_value":"fa fa-cog"},

		{"class_id":"menu","id":"menu_data","info_id":"hint","info_value":"实时数据"},
		{"class_id":"menu","id":"menu_data","info_id":"icon","info_value":""},
		{"class_id":"menu","id":"menu_data","info_id":"shortcut_icon","info_value":"image/icons/logo_Diag.ico"},
		{"class_id":"menu","id":"menu_data","info_id":"topic_icon","info_value":"fa fa-cog"},
		
		{"class_id":"menu","id":"menu_chart","info_id":"hint","info_value":"一次系统图"},
		{"class_id":"menu","id":"menu_chart","info_id":"icon","info_value":""},
		{"class_id":"menu","id":"menu_chart","info_id":"shortcut_icon","info_value":"image/icons/logo_Diag.ico"},
		{"class_id":"menu","id":"menu_chart","info_id":"topic_icon","info_value":"fa fa-cog"},

		{"class_id":"menu","id":"menu_byq","info_id":"hint","info_value":"变电柜监测"},
		{"class_id":"menu","id":"menu_byq","info_id":"icon","info_value":""},
		{"class_id":"menu","id":"menu_byq","info_id":"shortcut_icon","info_value":"image/icons/logo_Diag.ico"},
		{"class_id":"menu","id":"menu_byq","info_id":"topic_icon","info_value":"fa fa-cog"},

		{"class_id":"menu","id":"menu_dyg","info_id":"hint","info_value":"低压监测"},
		{"class_id":"menu","id":"menu_dyg","info_id":"icon","info_value":""},
		{"class_id":"menu","id":"menu_dyg","info_id":"shortcut_icon","info_value":"image/icons/logo_Diag.ico"},
		{"class_id":"menu","id":"menu_dyg","info_id":"topic_icon","info_value":"fa fa-cog"},

		{"class_id":"menu","id":"menu_report","info_id":"hint","info_value":"健康报告"},
		{"class_id":"menu","id":"menu_report","info_id":"icon","info_value":""},
		{"class_id":"menu","id":"menu_report","info_id":"shortcut_icon","info_value":"image/icons/logo_Diag.ico"},
		{"class_id":"menu","id":"menu_report","info_id":"topic_icon","info_value":"fa fa-cog"},

		{"class_id":"menu","id":"menu_anasys","info_id":"hint","info_value":"自定义报表"},
		{"class_id":"menu","id":"menu_anasys","info_id":"icon","info_value":""},
		{"class_id":"menu","id":"menu_anasys","info_id":"shortcut_icon","info_value":"image/icons/logo_Diag.ico"},
		{"class_id":"menu","id":"menu_anasys","info_id":"topic_icon","info_value":"fa fa-cog"}
]');

/*
	system item relation test data;
*/
-- account role relation
SELECT dml_ci_relation_insert ('[
		{"origin_class_id":"account","relate_class_id":"role","origin_id":"SysAdmin","relate_id":"role_system_administrator"},
		{"origin_class_id":"account","relate_class_id":"role","origin_id":"Admin","relate_id":"role_administrator"},
		{"origin_class_id":"account","relate_class_id":"role","origin_id":"Visitor","relate_id":"role_visitor"},
		{"origin_class_id":"account","relate_class_id":"role","origin_id":"User","relate_id":"role_user"}
]');
-- role function relation
SELECT dml_ci_relation_insert ('[
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"role_administrator","relate_id":"ui_system_config"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"role_administrator","relate_id":"ui_energy"},
	
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"role_administrator","relate_id":"ui_data"},

		{"origin_class_id":"role","relate_class_id":"function","origin_id":"role_administrator","relate_id":"ui_chart"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"role_administrator","relate_id":"ui_byq"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"role_administrator","relate_id":"ui_dyg"},

		{"origin_class_id":"role","relate_class_id":"function","origin_id":"role_administrator","relate_id":"ui_report"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"role_administrator","relate_id":"ui_anasys"}
]');
-- menu function relation
SELECT dml_ci_relation_insert ('[
		{"origin_class_id":"menu","relate_class_id":"function","origin_id":"menu_system_config","relate_id":"ui_system_config"},
		{"origin_class_id":"menu","relate_class_id":"function","origin_id":"menu_energy","relate_id":"ui_energy"},
		{"origin_class_id":"menu","relate_class_id":"function","origin_id":"menu_data","relate_id":"ui_data"},
		{"origin_class_id":"menu","relate_class_id":"function","origin_id":"menu_chart","relate_id":"ui_chart"},
		{"origin_class_id":"menu","relate_class_id":"function","origin_id":"menu_byq","relate_id":"ui_byq"},
		{"origin_class_id":"menu","relate_class_id":"function","origin_id":"menu_dyg","relate_id":"ui_dyg"},
		{"origin_class_id":"menu","relate_class_id":"function","origin_id":"menu_report","relate_id":"ui_report"},
		{"origin_class_id":"menu","relate_class_id":"function","origin_id":"menu_anasys","relate_id":"ui_anasys"}
]');