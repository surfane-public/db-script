/*

-- 数据库  :苏州普华电力工程有限公司施工项目管理系统数据库
-- 脚本类型:业务接口逻辑脚本

-- 创建信息 --
-- 作者    :杨永红
-- 版本    :V 1.0
-- 时间    :2017-05-10

-- 修订信息 --
-- 修订者  :
-- 版本    :
-- 时间    :
-- 修订概要:

*/

-- 我的任务-获取帐号流程信息
/*
 select func_account_process('',uid::TEXT) FROM t_ci_account where id = 'phjjb'

select * from t_bu_process_state where isactive= true
*/
CREATE OR REPLACE FUNCTION "public"."func_account_process"(text, varchar)
  RETURNS "pg_catalog"."jsonb" AS $BODY$
DECLARE
	v_uid UUID := $2::UUID;
	v_result JSONB := '{"result_state":"00000","result_text":"successful completion"}'::JSONB;
	BEGIN
		-- account uid and role uid
		WITH CTE1 AS(
				SELECT v_uid AS uid,'' AS role_id
				UNION
				SELECT cr.uid AS uid,cr.id AS role_id
					FROM t_ci_role AS cr
				 INNER JOIN t_rel_account_role AS ar ON ar.relate_uid = cr.uid
					WHERE ar.origin_uid = v_uid
			),
		-- active process
		CTE2 AS(
			SELECT DISTINCT cp.uid,cp.id,cp.name,ps.uid AS process_uid 
				FROM CTE1
			 INNER JOIN t_bu_process_state AS ps ON CTE1.uid = ps.owner
			 INNER JOIN t_ci_project AS cp ON cp.uid = ps.project_uid
			 WHERE ps.isactive = TRUE
			),
		CTE3 AS(
			SELECT CTE2.uid,CTE2.id,name
						,func_process_info(CTE2.process_uid) AS process
				 FROM CTE2
		),
		-- account role has law dept?
		CTE4 AS(
			SELECT SUM(CASE(coalesce(role_id,'')) WHEN 'law dept' THEN 1 ELSE 0 END)::INTEGER::BOOLEAN AS initiate FROM CTE1
		),
		CTE5 AS(
			SELECT json_agg(CTE3) AS project FROM CTE3
		),
		CTE6 AS(
			SELECT CTE4.initiate,CTE5.project FROM CTE4,CTE5
		)
		SELECT row_to_json(CTE6) INTO v_result FROM CTE6;

		RETURN v_result;			
	END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE COST 100
;


-- 我的工程-获取帐号对应工程信息
-- select func_account_project('',uid::TEXT) from t_ci_account;
CREATE OR REPLACE FUNCTION "func_account_project"(TEXT,VARCHAR)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_account_uid UUID := $2::UUID;
	v_result JSONB := '{"result_state":"00000","result_text":"successful completion"}'::JSONB;
	BEGIN
		-- account uid and role uid
		WITH CTE1 AS(
				SELECT v_account_uid AS uid
				UNION
				SELECT relate_uid AS uid FROM t_rel_account_role WHERE origin_uid = v_account_uid
			),
		CTE2 AS(
				SELECT DISTINCT ps.project_uid
					FROM CTE1
				 INNER JOIN t_bu_process_state AS ps ON CTE1.uid = ps.owner
			),
		CTE AS(
			SELECT cp.uid,cp.id,cp.name,func_process_navigation(ps.uid) AS process 
				FROM CTE2
			 INNER JOIN t_ci_project AS cp ON CTE2.project_uid = cp.uid
			 INNER JOIN t_bu_process_state AS ps ON cp.uid = ps.project_uid
			 WHERE ps.parent_uid IS NULL
				 AND ps.pre_uid IS NULL
		)
		SELECT json_agg(CTE) INTO v_result FROM CTE;
		RETURN v_result;			
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 流程加载-获取流程对应信息---资料归档 加载48edce06-9734-470c-b51d-725df9a1cc77--cfa5ea35-89a0-4694-9520-411b3e9ae4f2
--SELECT func_process_load('{"uid":"fcd0ca8a-182a-4a31-87af-57480ff866b9"}','2aaa7f06-59fd-4862-9096-cccbe0e0090a')

CREATE OR REPLACE FUNCTION "func_process_load"(TEXT,VARCHAR)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_data JSONB := $1;
	v_account_uid uuid := $2;
	v_process_uid UUID := NULL;
	v_project_uid UUID := NULL;
	v_project_id VARCHAR := NULL;
	v_result JSONB := '{"result_state":"00000","result_text":"successful completion"}'::JSONB;

v_role_uid UUID := NULL;
v_owner_uid UUID := NULL;
v_privilege BOOLEAN :=false;
v_count INT :=NULL;
	BEGIN
		SELECT t.uid,cp.uid,cp.id INTO v_process_uid,v_project_uid,v_project_id
			FROM jsonb_to_record(v_data) AS t(uid UUID)
			INNER JOIN t_bu_process_state AS ps ON ps.uid = t.uid
			INNER JOIN t_ci_project AS cp ON ps.project_uid = cp.uid;
		IF FOUND THEN
--页面权限
				SELECT OWNER INTO v_owner_uid FROM t_bu_process_state 
					WHERE uid=v_process_uid;

				WITH  role1 AS(
				SELECT v_account_uid AS uid
				UNION
				SELECT relate_uid AS uid FROM t_rel_account_role WHERE origin_uid = v_account_uid
				)SELECT count(*) INTO v_count FROM role1 WHERE role1.uid=v_owner_uid ;

				IF v_count>0 then v_privilege =TRUE ;
				END IF;

			WITH CTE AS(
					SELECT func_project_info(v_project_uid) - 'process'::TEXT AS project
								,func_process_info(v_process_uid) AS process,
								v_privilege AS privilege
			)
  		SELECT row_to_json(CTE) INTO v_result FROM CTE;
		-- new project, pre set project ID.
		ELSE
			SELECT 'PH' || (MAX(substring(id,3)::INTEGER) + 1) INTO v_project_id
				FROM t_ci_project 
			 WHERE id ~ '^PH\d{7}$' 
				 AND ('PH' || to_char(now(),'YY') || to_char(now(),'MM') || '000') < id;
			IF v_project_id IS NULL THEN
				v_project_id := 'PH'|| to_char(now(),'YY') || to_char(now(),'MM') || '001';
			END IF;
			WITH CTE2 AS(
				SELECT v_project_id AS id
			)
			SELECT row_to_json(t) INTO v_result FROM (SELECT row_to_json(CTE2) AS project FROM CTE2) AS t;
		END IF;

		RETURN v_result;			
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 保存作业
/*test script
	TRUNCATE table t_ci_project CASCADE
	SELECT func_process_save('{"id":"testProjectID888","name":"testProjectName888"}','');
*/
CREATE OR REPLACE FUNCTION "func_process_save"(TEXT,VARCHAR)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_data JSONB := $1;
	v_json JSONB := NULL;

	v_process_uid UUID := NULL;

	v_class_uid UUID := NULL;
	v_class_id VARCHAR := NULL;

	v_project_uid UUID := NULL;
	v_project_id VARCHAR := NULL;
	v_project_name VARCHAR :=NULL;

	v_exist_id VARCHAR :=NULL;
	v_exist_name VARCHAR :=NULL;

v_isactive BOOLEAN :=FALSE;

	v_result JSONB := '{"result_state":"00000","result_text":"successful completion"}'::JSONB;
	BEGIN
		SELECT t.process_uid ,coalesce(t.id,'') ,coalesce(t.name,'') ,coalesce(t.class_id,'project') 
      INTO v_process_uid ,v_project_id,v_project_name,v_class_id
			FROM jsonb_to_record(v_data) AS t(process_uid UUID,id VARCHAR,name VARCHAR,class_id VARCHAR) 
		 LIMIT 1;		
		SELECT uid INTO v_class_uid FROM t_cfg_class WHERE id = v_class_id;
		SELECT id INTO v_exist_id FROM t_ci_project WHERE id = v_project_id;
		SELECT name INTO v_exist_name FROM t_ci_project WHERE name = v_project_name;

		IF v_class_uid IS NULL THEN
			v_result := ('{"result_state":"01001","result_text":"class type <'|| v_class_id ||'> not configed!"}')::JSONB;
		ELSE
			IF v_process_uid IS NULL THEN
				IF 0 = LENGTH(v_project_id) THEN
					v_result := '{"result_state":"01002","result_text":"ci ID INPUT NULL!"}'::JSONB;
				ELSEIF 0 = LENGTH(v_project_name) THEN
					v_result := '{"result_state":"01003","result_text":"ci Name INPUT NULL!"}'::JSONB;
				ELSEIF v_exist_id IS NOT NULL THEN
					v_result := ('{"result_state":"01012","result_text":"ci ID <'|| v_project_id ||'> already exist!"}')::JSONB;
				ELSEIF v_exist_name IS NOT NULL THEN
					v_result := ('{"result_state":"01013","result_text":"ci name <'|| v_project_name ||'> already exist!"}')::JSONB;
				-- new project input;
				ELSE
					WITH CTE0 AS(
						SELECT v_class_uid AS class_uid,v_project_id AS id,v_project_name AS name
					)
					SELECT row_to_json(CTE0) INTO v_json FROM CTE0;
					PERFORM func_project_init(v_json);
					-- get process uid;
					SELECT ps.uid INTO v_process_uid 
						FROM t_bu_process_state AS ps
					 INNER JOIN t_ci_project AS ci ON ps.project_uid = ci.uid
						WHERE ci.id = v_project_id
							AND ci.name = v_project_name 
							AND ps.id = 'contract input';
					-- set active process;
					UPDATE t_bu_process_state SET isactive = TRUE,active_time = now() WHERE uid = v_process_uid;
					UPDATE t_bu_process_state SET isactive = TRUE,active_time = now() 
					 WHERE uid IN ( SELECT parent_uid FROM t_bu_process_state WHERE uid = v_process_uid);
				END IF;
			END IF;
			-- get project uid & process state
			SELECT project_uid,isactive INTO v_project_uid,v_isactive FROM t_bu_process_state WHERE uid = v_process_uid;
			IF FALSE = v_isactive THEN
				v_result := ('{"result_state":"01014","result_text":"process <'|| v_process_uid ||'> is not active!"}')::JSONB;
				-- new project input;
			ELSE
				-- set project attribute(update/create)
				WITH CTE1 AS (
					SELECT v_project_uid AS uid,ca.uid AS attribute_uid,t.value AS attribute_value
						FROM jsonb_each_text(v_data -> 'info'::TEXT) AS t
					 INNER JOIN t_cfg_attribute AS ca ON ca.id = t.key
					 WHERE ca.class_uid = v_class_uid
				)
				SELECT json_agg(CTE1) INTO v_json FROM CTE1;
				PERFORM func_delete_ci_attribute(v_json);
				PERFORM func_create_ci_attribute(v_json);
				-- set project resource(update/create)
				DELETE FROM t_bu_resource_state WHERE process_uid = v_process_uid ;
				-- insert by uid 
				INSERT INTO t_bu_resource_state(process_uid,resource_uid,quantity)
				SELECT v_process_uid,uid,quantity
					FROM jsonb_to_recordset(v_data -> 'resource'::TEXT) AS t(uid UUID,quantity FLOAT8)
				 WHERE t.uid IS NOT NULL
					 AND t.quantity IS NOT NULL;
				-- insert by id 
				INSERT INTO t_bu_resource_state(process_uid,resource_uid,quantity)
				SELECT v_process_uid,cr.uid,quantity
					FROM jsonb_to_recordset(v_data -> 'resource'::TEXT) AS t(uid UUID,id VARCHAR,quantity FLOAT8)
					LEFT JOIN t_ci_resource AS cr ON t.id = cr.id
				 WHERE t.uid IS NULL
					 AND t.id IS NOT NULL
					 AND t.quantity IS NOT NULL;
			END IF;
		END IF;

-- 		SELECT row_to_json(t) INTO v_result FROM (SELECT v_process_uid as uid) AS t;

		 WITH CTEB AS(
							SELECT v_process_uid AS uid,c1.id,c1.name FROM t_bu_process_state c1 WHERE c1.uid=v_process_uid
		)
		,CTER AS(
		SELECT row_to_json(CTEB) AS process  FROM CTEB
		)SELECT row_to_json(CTER) INTO v_result FROM CTER;

		RETURN v_result;			
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 提交作业
/*test script
	SELECT func_process_commit('{"uid":"1421ee05-f5e6-47c4-a5ea-b73a7dd972ef"}','2aaa7f06-59fd-4862-9096-cccbe0e0090a');
select * from t_bu_process_state where uid='1421ee05-f5e6-47c4-a5ea-b73a7dd972ef'
SELECT func_next_process('1421ee05-f5e6-47c4-a5ea-b73a7dd972ef')
SELECT * FROM  t_bu_process_state WHERE uid = '7dcd308f-9af0-4733-95f7-d0612e6a1bbd' OR uid = '104cc469-9767-4679-a9f8-73021869cb51'
*/
CREATE OR REPLACE FUNCTION "func_process_commit"(TEXT,VARCHAR)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_data jsonb := $1;
	v_account_uid UUID := CAST($2 AS UUID);
	v_remark VARCHAR := NULL;
	v_result JSONB := '{"result_state":"00000","result_text":"successful completion"}'::JSONB;
	v_isactive BOOLEAN := FALSE;
	v_process_uid UUID := NULL;
	BEGIN
		SELECT ps.uid,ps.isactive,t.remark INTO v_process_uid,v_isactive,v_remark
			FROM t_bu_process_state AS ps
		 INNER JOIN jsonb_to_record(v_data) AS t(uid UUID,remark VARCHAR) ON t.uid = ps.uid;
		IF NOT FOUND THEN
			v_result := ('{"result_state":"01014","result_text":"process is not exist!"}')::JSONB;
		ELSE
			IF TRUE = v_isactive THEN			
				PERFORM func_active_process(uid)
					FROM jsonb_to_recordset(func_next_process(v_process_uid)) AS t(uid UUID);
				PERFORM func_inactive_process(v_process_uid);
				UPDATE t_bu_process_state SET isactive = FALSE WHERE id IN ('end','approve_65') ;
				INSERT INTO t_bu_project_state(process_uid,account_uid,time,approve,remark)
				VALUES (v_process_uid,v_account_uid,now(),True,v_remark); 
			ELSE 
				v_result := ('{"result_state":"01014","result_text":"process <'|| v_process_uid ||'> is not active!"}')::JSONB;
			END IF;
		END IF;

		 WITH CTEB AS(
							SELECT v_process_uid AS uid,c1.id,c1.name FROM t_bu_process_state c1 WHERE c1.uid=v_process_uid
		)
		,CTER AS(
		SELECT row_to_json(CTEB) AS process  FROM CTEB
		)SELECT row_to_json(CTER) INTO v_result FROM CTER;
		RETURN v_result;		
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

CREATE OR REPLACE FUNCTION "func_inactive_process"(UUID)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_process_uid UUID := $1;
	BEGIN
		UPDATE t_bu_process_state SET isactive = FALSE WHERE uid = v_process_uid;
		SELECT parent_uid INTO v_process_uid FROM t_bu_process_state WHERE uid = v_process_uid LIMIT 1;
		IF FOUND THEN
			PERFORM uid FROM t_bu_process_state WHERE parent_uid = v_process_uid AND isactive = TRUE;
			IF NOT FOUND THEN
				PERFORM func_inactive_process(v_process_uid);
			END IF;
		END IF;
		RETURN '{}'::JSONB;
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

CREATE OR REPLACE FUNCTION "func_active_process"(UUID)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_process_uid UUID := $1;
	BEGIN
		UPDATE t_bu_process_state SET isactive = TRUE,active_time = now() WHERE uid = v_process_uid;		
		PERFORM uid FROM t_bu_process_state WHERE parent_uid = v_process_uid AND pre_uid IS NULL;
		IF FOUND THEN
			PERFORM func_active_process(uid) 
				FROM t_bu_process_state WHERE parent_uid = v_process_uid AND pre_uid IS NULL;
		END IF;
		RETURN '{}'::JSONB;
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;
-- 审核作业-拒绝
/*test script
	SELECT func_process_rollback('{"uid":"9eab5a13-7ce9-48db-b46b-27a7910bac2c","remark":""}','2aaa7f06-59fd-4862-9096-cccbe0e0090a');

SELECT func_execute(1017,'2aaa7f06-59fd-4862-9096-cccbe0e0090a','{"uid":"9eab5a13-7ce9-48db-b46b-27a7910bac2c","remark":""}')
SELECT func_previous_process('9eab5a13-7ce9-48db-b46b-27a7910bac2c')--bb75
SELECT func_next_process('9eab5a13-7ce9-48db-b46b-27a7910bac2c')---0c5a
SELECT * FROM json_to_recordset('[{"pre_uid": "bb75dad8-2386-4110-8687-b0df451546df"}]') as ( pre_uid uuid)


*/
CREATE OR REPLACE FUNCTION "func_process_rollback"(TEXT,VARCHAR)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_data jsonb := $1;
	v_account_uid UUID := $2;
	v_remark VARCHAR := NULL;
	v_result JSONB := '{"result_state":"00000","result_text":"successful completion"}'::JSONB;
	v_isactive BOOLEAN := FALSE;
	v_process_uid UUID := NULL;
	BEGIN
		SELECT ps.uid,ps.isactive,t.remark INTO v_process_uid,v_isactive,v_remark
			FROM t_bu_process_state AS ps
		 INNER JOIN jsonb_to_record(v_data) AS t(uid UUID,remark VARCHAR) ON t.uid = ps.uid;
		IF NOT FOUND THEN
			v_result := ('{"result_state":"01014","result_text":"process is not exist!"}')::JSONB;
		ELSE
			IF TRUE = v_isactive THEN
				PERFORM func_inactive_process(v_process_uid);
				PERFORM func_active_process(pre_uid)
					FROM jsonb_to_recordset(func_previous_process(v_process_uid)) AS t(pre_uid UUID);
				INSERT INTO t_bu_project_state(process_uid,account_uid,time,approve,remark)
				VALUES (v_process_uid,v_account_uid,now(),FALSE,v_remark); 
			ELSE 
				v_result := ('{"result_state":"01014","result_text":"process <'|| v_process_uid ||'> is not active!"}')::JSONB;
			END IF;
		END IF;

		 WITH CTEB AS(
							SELECT v_process_uid AS uid,c1.id,c1.name FROM t_bu_process_state c1 WHERE c1.uid=v_process_uid
		)
		,CTER AS(
		SELECT row_to_json(CTEB) AS process  FROM CTEB
		)SELECT row_to_json(CTER) INTO v_result FROM CTER;
		RETURN v_result;		
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 获取库存数量
/*test script
	SELECT func_resource_inventory(null,'');
*/
CREATE OR REPLACE FUNCTION "func_resource_inventory"(TEXT,VARCHAR)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_result JSONB := NULL;
	BEGIN
		WITH CTE AS(
			SELECT c2.resource_uid,c2.quantity
			 FROM t_bu_resource_state c2
 			INNER JOIN t_bu_process_state c3 ON c3.uid=c2.process_uid
 			LEFT JOIN t_attr_flow c4 ON c4.item_uid=c3.flow_uid
 			WHERE c4.attribute_value in ('purchase_storage','resource_delivery','resource_storage')
		),
		CTE2 AS(
			SELECT c1.uid,c1.id,c1.name,c1.valid
						,sum(coalesce(CTE.quantity,0)) AS inventory
						,jsonb_object(array_agg(coalesce(attribute_id,'')),array_agg(coalesce(attributeset_value,attribute_value))) AS info
			 FROM t_ci_resource AS c1
			 LEFT JOIN v_ci_attribute AS c2 ON c1.uid = c2.item_uid
			 LEFT JOIN CTE ON c1.uid = CTE.resource_uid
			 --WHERE c1.valid = TRUE
			GROUP BY c1.uid,c1.id,c1.name 
		)
		SELECT json_agg(CTE2) INTO v_result FROM CTE2;
		RETURN v_result;
	END;

$BODY$ 
  LANGUAGE 'plpgsql'
;


-- 根据指派项目组process_uid获取项目指派人员及指派状态信息
/*test script
	SELECT func_role_account('{"uid":"6d315992-7769-42f5-8678-1414308592fe"}','');
*/
CREATE OR REPLACE FUNCTION "func_role_account"(TEXT,VARCHAR)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_data jsonb := $1;
	v_result JSONB := NULL;
	BEGIN
		with cte as(
		select ps.project_uid
		from t_bu_process_state as ps
		INNER JOIN  jsonb_to_record(v_data) as t(uid UUID) on t.uid = ps.uid
		)
		,cte2 AS (
		SELECT c1.project_uid,c1.owner--,c2.name--,c1.flow_uid 
		FROM cte 
		LEFT JOIN t_bu_process_state c1 ON c1.project_uid=cte.project_uid
		LEFT JOIN t_ci_flow c2 ON c2.uid=c1.flow_uid
		)
-- 				select  DISTINCT cr.id,cr.uid,cr.name,cte2.project_uid,case when cte2.owner is null then false else true end as assigned
		,cte4 as(
		select  DISTINCT cr.name,case when cte2.owner is null then false else true end as assigned
		from t_ci_role as cr 
		left join cte2 on cr.uid = cte2.owner
		where cr.id like 'project_team%'
)  SELECT json_agg(cte4) INTO v_result FROM cte4;
		RETURN v_result;
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;


-- 根据指派项目组process_uid改变相应节点的owner
/*test script
	SELECT func_modify_role_account('{"process_uid":"6d315992-7769-42f5-8678-1414308592fe","assigned":[{"name": "test", "assigned": "false"}, {"name": "项目部", "assigned": "true"}, {"name": "项目组3", "assigned": "false"}]}','');
*/
CREATE OR REPLACE FUNCTION "func_modify_role_account"(TEXT,VARCHAR)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_data jsonb := $1;
	v_result JSONB := '{"result_state":"00000","result_text":"successful completion"}'::JSONB;
	v_role_uid uuid := NULL;
	v_project_uid uuid := NULL;
	v_old_role uuid:= NULL;
	BEGIN
--get process_uid
WITH CTE AS(
	SELECT 
		  --'{"process_uid":"6d315992-7769-42f5-8678-1414308592fe","assigned":[{"name": "test", "assigned": "false"}, {"name": "项目部", "assigned": "true"}, {"name": "项目组3", "assigned": "false"}]}'::jsonb ->>'process_uid'
			v_data::jsonb ->>'process_uid'
			as process_uid
) 
--use process_uid to get project
		select ps.project_uid INTO v_project_uid
		from t_bu_process_state as ps
		inner JOIN  CTE on cte.process_uid::UUID = ps.uid;
--get new role
		SELECT uid INTO v_role_uid FROM t_ci_role WHERE name = (
			SELECT name
			--FROM  json_to_recordset('{"process_uid":"6d315992-7769-42f5-8678-1414308592fe","assigned":[{"name": "test", "assigned": "false"}, {"name": "项目部", "assigned": "true"}, {"name": "项目组3", "assigned": "false"}]}'::json->'assigned')
			FROM  json_to_recordset(v_data::json->'assigned')
			AS t(name VARCHAR,assigned VARCHAR) 
			WHERE assigned='true'
		);
--get 初始化项目组owner
			SELECT DISTINCT c2.owner INTO v_old_role FROM t_ci_role c1
			LEFT JOIN t_bu_process_state c2 ON c2.owner = c1.uid
			WHERE c1.id LIKE 'project_team%' AND c2.project_uid=v_project_uid;
		UPDATE t_bu_process_state  SET owner=v_role_uid WHERE project_uid=v_project_uid AND owner=v_old_role;
	RETURN v_result;
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;

-- 文件上传
/*test script
	SELECT func_file_upload('{"process_uid":"d38b7244-63a3-4673-a566-d569074da70d","document":[{"id":"22222","name":"22222"},{"id":"33333","name":"3333333"}]}','d41b0364-7c8f-4d4f-a055-e296a5d3533e');
*/
CREATE OR REPLACE FUNCTION "func_file_upload"(TEXT,VARCHAR)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_account_uid UUID := $2::UUID;
	v_data jsonb := $1;
	v_result JSONB := '{"result_state":"00000","result_text":"successful completion"}'::JSONB;
	v_process_uid uuid:=NULL;
	v_document json :=NULL;
	v_file_info json:=NULL;
	BEGIN
			SELECT 	v_data ::jsonb ->> 'process_uid' INTO v_process_uid;
			SELECT  v_data::jsonb->>'document' INTO v_document;

			WITH CTE AS(
			SELECT id,name,'f' as valid,'document' AS class_id 
				FROM json_to_recordset(v_document) AS t0(id VARCHAR ,name VARCHAR)
				 WHERE NOT EXISTS(SELECT 0 FROM t_ci_document AS cd WHERE cd.id = t0.id)
			)select json_agg(CTE) INTO v_file_info FROM CTE;
			PERFORM func_create_ci_by_id (v_file_info::jsonb);

			INSERT INTO t_bu_document_state(process_uid,document_uid,owner) 
			SELECT v_process_uid,uid,v_account_uid FROM t_ci_document WHERE id in (
			SELECT id FROM  json_to_recordset(v_file_info) as (id VARCHAR)
				);
		RETURN v_result;
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;


-- 文件删除
/*test script
	SELECT func_file_delete('{"process_uid":"c177bc41-685e-46e5-8af9-15a695db8430","document":[{"id":"q","name":"w"},{"id":"e","name":"r"}]}','d41b0364-7c8f-4d4f-a055-e296a5d3533e');
*/
CREATE OR REPLACE FUNCTION "func_file_delete"(TEXT,VARCHAR)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_data jsonb := $1;
	v_result JSONB := '{"result_state":"00000","result_text":"successful completion"}'::JSONB;

	v_document json :=NULL;
	v_file_info json:=NULL;
	BEGIN
			SELECT  v_data::jsonb->>'document' INTO v_document;

			DELETE FROM t_ci_document WHERE id IN(
								 SELECT id FROM json_to_recordset(v_document) AS (id VARCHAR)
			);
		RETURN v_result;
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;



-- 获取配置信息
/*test script
	SELECT func_file_delete('{"process_uid":"c177bc41-685e-46e5-8af9-15a695db8430","document":[{"id":"q","name":"w"},{"id":"e","name":"r"}]}','d41b0364-7c8f-4d4f-a055-e296a5d3533e');

SELECT func_execute(1020,'','')
*/
CREATE OR REPLACE FUNCTION "func_get_config_info"(TEXT,VARCHAR)
  RETURNS JSONB
AS $BODY$
DECLARE
	v_result JSONB := '{"result_state":"00000","result_text":"successful completion"}'::JSONB;
	BEGIN
			WITH CTE AS (
			SELECT c1.id AS id,c1.name AS name,jsonb_object(array_agg(coalesce(c3.id,'')),array_agg(coalesce(c2.attribute_value,attribute_value))) AS info
			FROM t_ci_profile c1
			LEFT JOIN t_attr_profile c2 ON c2.item_uid=c1.uid
			LEFT JOIN t_cfg_attribute c3 ON c3.uid=c2.attribute_uid
			GROUP BY c1.id,c1.name
		)SELECT json_agg(CTE) INTO v_result FROM CTE;
		RETURN v_result;
	END;
$BODY$ 
  LANGUAGE 'plpgsql'
;








