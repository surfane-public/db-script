/*

-- 数据库  :苏州普华电力工程有限公司施工项目管理系统数据库
-- 脚本类型:业务表创建脚本

-- 创建信息 --
-- 作者    :杨永红
-- 版本    :V 1.0
-- 时间    :2017-05-10

-- 修订信息 --
-- 修订者  :
-- 版本    :
-- 时间    :
-- 修订概要:

*/

-- 业务.工程流程记录表
/*
 owner:有权限操作该流程的角色或帐号uid,
 isapprove,关联flow的valid(是否为审核流程),再关联flow的edit_item三者用于判断该流程的具体状态:
 isactive:该流程是否处理编辑状态,=f:只读不可编辑
 isactive=f:只读不可编辑
 isactive=t,isapprove=f:非审核流程,用户编写edit_item中内容(允许保存与提交)
 isactive=t,isapprove=t:审核流程,用户可审核edit_item中内容(允许同意与拒绝)
*/
DROP TABLE IF EXISTS "t_bu_process_state" CASCADE;
CREATE TABLE "t_bu_process_state" (
"uid" UUID NOT NULL,
"id" VARCHAR NOT NULL,
"name" VARCHAR NOT NULL,
"owner" UUID,
"isactive" BOOLEAN DEFAULT FALSE NOT NULL,
"isapprove" bool DEFAULT FALSE NOT NULL,
"active_time" TIMESTAMP,
"project_uid" UUID NOT NULL,
"flow_uid" UUID NOT NULL,
"parent_uid" UUID,
"pre_uid" UUID,
CONSTRAINT "pk_bu_process" PRIMARY KEY ("uid"),
CONSTRAINT "fk_bu_process_parent" FOREIGN KEY ("parent_uid") REFERENCES "t_bu_process_state" ("uid") ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT "fk_bu_process_pre" FOREIGN KEY ("pre_uid") REFERENCES "t_bu_process_state" ("uid") ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT "fk_bu_process_flow" FOREIGN KEY ("flow_uid") REFERENCES "t_ci_flow" ("uid") ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT "fk_bu_process_project" FOREIGN KEY ("project_uid") REFERENCES "t_ci_project" ("uid") ON DELETE CASCADE ON UPDATE CASCADE
)
;

-- 业务.材料异动记录表
DROP TABLE IF EXISTS "t_bu_resource_state" CASCADE;
CREATE TABLE "t_bu_resource_state" (
"process_uid" UUID NOT NULL,
"resource_uid" UUID NOT NULL,
"quantity" FLOAT NOT NULL,
CONSTRAINT "fk_bu_resource_resource" FOREIGN KEY ("resource_uid") REFERENCES "t_ci_resource" ("uid") ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT "fk_bu_resource_process" FOREIGN KEY ("process_uid") REFERENCES "t_bu_process_state" ("uid") ON DELETE CASCADE ON UPDATE CASCADE
)
;

DROP TABLE IF EXISTS "t_bu_document_state" CASCADE;
CREATE TABLE "t_bu_document_state" (
"process_uid" UUID NOT NULL,
"document_uid" UUID NOT NULL, 
"owner" UUID NOT NULL,
CONSTRAINT "fk_bu_document_resource" FOREIGN KEY ("document_uid") REFERENCES "t_ci_document" ("uid") ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT "fk_bu_document_process" FOREIGN KEY ("process_uid") REFERENCES "t_bu_process_state" ("uid") ON DELETE CASCADE ON UPDATE CASCADE
)
;

DROP TABLE IF EXISTS "t_bu_project_state" CASCADE;
CREATE TABLE "t_bu_project_state" (
"process_uid" UUID NOT NULL,
"account_uid" UUID NOT NULL,
"time" TIMESTAMP NOT NULL,
"approve" BOOLEAN DEFAULT TRUE NOT NULL,
"remark" TEXT
)
;




