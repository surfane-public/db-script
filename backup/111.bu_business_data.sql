/*

-- 数据库  :苏州普华电力工程有限公司施工项目管理系统数据库
-- 脚本类型:项目期初数据

-- 创建信息 --
-- 作者    :杨永红
-- 版本    :V 1.0
-- 时间    :2017-05-10

-- 修订信息 --
-- 修订者  :
-- 版本    :
-- 时间    :
-- 修订概要:

*/

/*test*/
--class
SELECT func_create_class ('[
		{"id":"project","name":"工程"},
		{"id":"resource","name":"资源"},
		{"id":"document","name":"文档"}
]');
ALTER TABLE t_ci_document DROP CONSTRAINT uq_ci_document_name;

--profile item
SELECT func_create_ci_by_id ('[
		{"id":"document_folder","name":"文档目录","class_id":"profile"}
]');

--profile item attribute
SELECT func_create_ci_attribute_by_id('[
	{"class_id":"profile","item_id":"document_folder","attribute_id":"config","attribute_value":"c:\\ePM\\document\\"}
]');

--flow attribute
SELECT func_create_attribute_by_id ('[
		{"id":"edit_item","name":"可编辑项","class_id":"flow"},
		{"id":"stage","name":"阶段","class_id":"flow"}
]');

--project attribute
SELECT func_create_attribute_by_id ('[
		{"id":"contract_name","name":"合同名称","class_id":"project"},
		{"id":"contract_money","name":"合同金额","class_id":"project"},
		{"id":"contract_begin","name":"合同开始时间","class_id":"project"},
		{"id":"contract_end","name":"合同结束时间","class_id":"project"},

		{"id":"project_name","name":"工程名称","class_id":"project"},
		{"id":"project_place","name":"工程地址","class_id":"project"},
		{"id":"project_survey","name":"工程概况","class_id":"project"},

		{"id":"project_company","name":"建设单位","class_id":"project"},
		{"id":"construction_unit","name":"施工单位","class_id":"project"},

		{"id":"role_name","name":"申领部门","class_id":"project"},
		{"id":"account_name","name":"责任人","class_id":"project"},
		{"id":"claimant_date","name":"申领日期","class_id":"project"},
		{"id":"claimant_reason","name":"申领事由","class_id":"project"},
		{"id":"delivery_time","name":"交货时间","class_id":"project"},

		{"id":"construction_organization_measures","name":"施工组织措施","class_id":"project"},
		{"id":"construction_technology_measures","name":"施工技术措施","class_id":"project"},
		{"id":"measures_for_construction_safety","name":"施工安全措施","class_id":"project"},
		{"id":"Staffing_scheme","name":"人员配置方案","class_id":"project"},
		{"id":"Construction_schedule","name":"施工进度计划表","class_id":"project"},
		{"id":"Supervision_information","name":"监理报审资料","class_id":"project"},

		{"id":"plan_start_up_time","name":"计划开工时间","class_id":"project"},
		{"id":"plan_end_up_time","name":"计划竣工时间","class_id":"project"},
		{"id":"project_quantity","name":"计划工作量","class_id":"project"},
		{"id":"prepare_work_state","name":"开工准备情况","class_id":"project"},
		{"id":"fill_time","name":"填报时间","class_id":"project"}
]');

SELECT func_create_attribute_by_id ('[
{"id":"applicant_time","name":"工程竣工验收填报时间","class_id":"project"}
]');

SELECT func_create_attribute_by_id ('[
{"id":"informant","name":"填报人","class_id":"project"}
]');

--resource attribute
SELECT func_create_attribute_by_id ('[
			{"id":"commodity_name","name":"品名","class_id":"resource"},
			{"id":"model","name":"型号","class_id":"resource"},
			{"id":"unit","name":"单位","class_id":"resource"},
			{"id":"unit_price","name":"采购价格","class_id":"resource"},
			{"id":"tax_included_price","name":"含税价格","class_id":"resource"}
]');


--role item
SELECT func_create_ci_by_id ('[
		{"id":"ceo","name":"总经理","class_id":"role"},
		{"id":"business director","name":"经营副总","class_id":"role"},
		{"id":"production director","name":"生产副总","class_id":"role"},
		{"id":"service dept","name":"综合管理部","class_id":"role"},
		{"id":"law dept","name":"合约部","class_id":"role"},
		{"id":"finance dept","name":"财务部","class_id":"role"},
		{"id":"purchase dept","name":"采购部","class_id":"role"},
		{"id":"warehouse dept","name":"仓库","class_id":"role"},
		{"id":"ie dept","name":"计经部","class_id":"role"},
		{"id":"quality dept","name":"质安部","class_id":"role"},
		{"id":"project dept","name":"工程部","class_id":"role"},
		{"id":"project team","name":"项目部","class_id":"role"}
]');

--account item
SELECT func_create_ci_by_id ('[
		{"id":"ph666","name":"普华666","class_id":"account"},
		{"id":"ph888","name":"普华888","class_id":"account"},
		{"id":"ph999","name":"普华999","class_id":"account"},

		{"id":"phzjl","name":"普华总经理","class_id":"account"},
		{"id":"phjyfz","name":"普华经营副总","class_id":"account"},
		{"id":"phscfz","name":"普华生产副总","class_id":"account"},
		{"id":"phzhglb","name":"普华综合管理部","class_id":"account"},
		{"id":"phhyb","name":"普华合约部","class_id":"account"},
		{"id":"phcwb","name":"普华财务部","class_id":"account"},
		{"id":"phcgb","name":"普华采购部","class_id":"account"},
		{"id":"phckgl","name":"普华仓库","class_id":"account"},
		{"id":"phjjb","name":"普华计经部","class_id":"account"},
		{"id":"phzab","name":"普华质安部","class_id":"account"},
		{"id":"phgcb","name":"普华工程部","class_id":"account"},
		{"id":"phxmb","name":"普华项目部","class_id":"account"}
]');

--menu item
SELECT func_create_ci_by_id('[
		{"id":"menu_epm","name":"普华施工管理菜单","class_id":"menu"},		
		{"id":"menu_task","name":"我的任务","class_id":"menu","parent_id":"menu_epm"},
		{"id":"menu_project","name":"我的项目","class_id":"menu","parent_id":"menu_epm","pre_id":"menu_task"},
		{"id":"menu_report","name":"项目分析","class_id":"menu","parent_id":"menu_epm","pre_id":"menu_project"},
		{"id":"menu_config","name":"系统管理","class_id":"menu","parent_id":"menu_epm","pre_id":"menu_report"}
]');

--function item
SELECT func_create_ci_by_id ('[
		{"id":"view_task","name":"我的任务视图","class_id":"function"},
		{"id":"view_project","name":"我的项目视图","class_id":"function"},
		{"id":"view_report","name":"我的报表视图","class_id":"function"},
		{"id":"view_config","name":"人员管理视图","class_id":"function"},

		{"id":"view_material","name":"材料管理界面","class_id":"function"},
		{"id":"view_document","name":"文档管理界面","class_id":"function"},

		{"id":"view_project_initial","name":"项目启动界面","class_id":"function"},
		{"id":"view_project_assign","name":"指派人员界面","class_id":"function"},
		{"id":"view_project_cost","name":"成本管理界面","class_id":"function"},
		{"id":"view_project_schedule","name":"进度计划界面","class_id":"function"},
		{"id":"view_kick_off","name":"开工报告界面","class_id":"function"},
		{"id":"view_acceptance","name":"验收申请界面","class_id":"function"}
]');

--function router
SELECT func_create_router('[
	{"id":1011,"name":"task save","function":"func_process_save","description":"任务保存未提交"},
	{"id":1013,"name":"task commit","function":"func_process_commit","description":"任务提交"},
	{"id":1015,"name":"task approve","function":"func_process_commit","description":"审核-通过"},
	{"id":1017,"name":"process rollback","function":"func_process_rollback","description":"审核-驳回"},

	{"id":1010,"name":"get user task","function":"func_account_process","description":"根据用户帐号uid获取需要处理的任务"},
	{"id":1012,"name":"get user project","function":"func_account_project","description":"根据用户帐号uid获取有权限查看的项目"},
	{"id":1014,"name":"process load","function":"func_process_load","description":"流程加载-获取流程对应信息"},
	{"id":1016,"name":"get resource inventory","function":"func_resource_inventory","description":"获取资源库存"}
]');

SELECT func_create_router('[
	{"id":1018,"name":"get project group","function":"func_role_account","description":"指派项目组"},
	{"id":1019,"name":"insert someone","function":"func_modify_role_account","description":"指派项目组保存"},
	{"id":1020,"name":"get config info","function":"func_get_config_info","description":"获取配置信息"},
	{"id":1021,"name":"file upload","function":"func_file_upload","description":"文件上传"},
	{"id":1023,"name":"file delete","function":"func_file_delete","description":"文件上传取消"}
]');



--resource item
SELECT func_create_ci_by_id ('[
		{"id":"one_setting","name":"一次安装","class_id":"resource","valid":"false"},
		{"id":"two_setting","name":"二次安装","class_id":"resource","valid":"false"},
		{"id":"high_voltage_test","name":"高压试验","class_id":"resource","valid":"false"},
		{"id":"The_bartender_debugging","name":"继保调试","class_id":"resource","valid":"false"},

		{"id":"Big_companies","name":"大件公司","class_id":"resource","valid":"false"},
		{"id":"principal_material","name":"主材","class_id":"resource","valid":"false"},
		{"id":"Time_limit_approval","name":"工期核定","class_id":"resource","valid":"false"},
		{"id":"Business_haa","name":"业务招待补贴费核定","class_id":"resource","valid":"false"},
		{"id":"total_quota_approved","name":"总定额金额核定","class_id":"resource","valid":"false"},

		{"id":"forklift","name":"叉车","class_id":"resource","valid":"false"},
		{"id":"trucks","name":"货车","class_id":"resource","valid":"false"},
		{"id":"crane","name":"吊车","class_id":"resource","valid":"false"},
		{"id":"backhoe","name":"挖机","class_id":"resource","valid":"false"},

		{"id":"metiral01","name":"材料一","class_id":"resource","valid":"false"},
		{"id":"metiral02","name":"材料二","class_id":"resource","valid":"false"},
		{"id":"metiral03","name":"材料三","class_id":"resource","valid":"false"}

]');


--{"id":"forklift","name":"叉车","class_id":"resource"},
--resource item attribute
SELECT func_create_ci_attribute_by_id('[
		{"class_id":"resource","item_id":"one_setting","attribute_id":"unit","attribute_value":"工日"},
		{"class_id":"resource","item_id":"two_setting","attribute_id":"unit","attribute_value":"工日"},
		{"class_id":"resource","item_id":"high_voltage_test","attribute_id":"unit","attribute_value":"工日"},
		{"class_id":"resource","item_id":"The_bartender_debugging","attribute_id":"unit","attribute_value":"工日"},
		{"class_id":"resource","item_id":"Big_companies","attribute_id":"unit","attribute_value":"台班"},
		{"class_id":"resource","item_id":"principal_material","attribute_id":"unit","attribute_value":"米"},
		{"class_id":"resource","item_id":"Time_limit_approval","attribute_id":"unit","attribute_value":"工日"},
		{"class_id":"resource","item_id":"Business_haa","attribute_id":"unit","attribute_value":"元"},
		{"class_id":"resource","item_id":"total_quota_approved","attribute_id":"unit","attribute_value":"元"},

		{"class_id":"resource","item_id":"forklift","attribute_id":"unit","attribute_value":"台班"},
		{"class_id":"resource","item_id":"trucks","attribute_id":"unit","attribute_value":"台班"},
		{"class_id":"resource","item_id":"crane","attribute_id":"unit","attribute_value":"台班"},
		{"class_id":"resource","item_id":"backhoe","attribute_id":"unit","attribute_value":"台班"},

		{"class_id":"resource","item_id":"forklift","attribute_id":"unit_price","attribute_value":"100"},
		{"class_id":"resource","item_id":"trucks","attribute_id":"unit_price","attribute_value":"200"},
		{"class_id":"resource","item_id":"crane","attribute_id":"unit_price","attribute_value":"300"},
		{"class_id":"resource","item_id":"backhoe","attribute_id":"unit_price","attribute_value":"400"},

		{"class_id":"resource","item_id":"metiral01","attribute_id":"unit","attribute_value":"米"},
		{"class_id":"resource","item_id":"metiral03","attribute_id":"unit","attribute_value":""},

		{"class_id":"resource","item_id":"metiral01","attribute_id":"unit_price","attribute_value":"10"},
		{"class_id":"resource","item_id":"metiral02","attribute_id":"unit_price","attribute_value":""}

]');

--account role relation
SELECT func_create_ci_relation_by_id ('[
		{"origin_class_id":"account","relate_class_id":"role","origin_id":"ph888","relate_id":"ceo"},
		{"origin_class_id":"account","relate_class_id":"role","origin_id":"ph666","relate_id":"role_administrator"},
		{"origin_class_id":"account","relate_class_id":"role","origin_id":"ph888","relate_id":"role_administrator"},
		{"origin_class_id":"account","relate_class_id":"role","origin_id":"ph888","relate_id":"role_user"},
		{"origin_class_id":"account","relate_class_id":"role","origin_id":"ph999","relate_id":"role_visitor"},

		{"origin_class_id":"account","relate_class_id":"role","origin_id":"phzjl","relate_id":"ceo"},
		{"origin_class_id":"account","relate_class_id":"role","origin_id":"phjyfz","relate_id":"business director"},
		{"origin_class_id":"account","relate_class_id":"role","origin_id":"phscfz","relate_id":"production director"},
		{"origin_class_id":"account","relate_class_id":"role","origin_id":"phzhglb","relate_id":"service dept"},
		{"origin_class_id":"account","relate_class_id":"role","origin_id":"phhyb","relate_id":"law dept"},
		{"origin_class_id":"account","relate_class_id":"role","origin_id":"phcwb","relate_id":"finance dept"},
		{"origin_class_id":"account","relate_class_id":"role","origin_id":"phcgb","relate_id":"purchase dept"},
		{"origin_class_id":"account","relate_class_id":"role","origin_id":"phckgl","relate_id":"warehouse dept"},
		{"origin_class_id":"account","relate_class_id":"role","origin_id":"phjjb","relate_id":"ie dept"},
		{"origin_class_id":"account","relate_class_id":"role","origin_id":"phzab","relate_id":"quality dept"},
		{"origin_class_id":"account","relate_class_id":"role","origin_id":"phgcb","relate_id":"project dept"},
		{"origin_class_id":"account","relate_class_id":"role","origin_id":"phxmb","relate_id":"project team"}
]');

--account attribute
SELECT func_create_ci_attribute_by_id('[
	{"class_id":"account","item_id":"phzjl","attribute_id":"password","attribute_value":"ph123456","encode":"true"},
	{"class_id":"account","item_id":"phzjl","attribute_id":"birthday","attribute_value":"2001-01-01"},
	{"class_id":"account","item_id":"phzjl","attribute_id":"name","attribute_value":"普华总经理"},

	{"class_id":"account","item_id":"phjyfz","attribute_id":"password","attribute_value":"ph123456","encode":"true"},
	{"class_id":"account","item_id":"phjyfz","attribute_id":"birthday","attribute_value":"2002-02-02"},
	{"class_id":"account","item_id":"phjyfz","attribute_id":"name","attribute_value":"普华经营副总"},

	{"class_id":"account","item_id":"phscfz","attribute_id":"password","attribute_value":"ph123456","encode":"true"},
	{"class_id":"account","item_id":"phscfz","attribute_id":"birthday","attribute_value":"2003-03-03"},
	{"class_id":"account","item_id":"phscfz","attribute_id":"name","attribute_value":"普华生产副总"},

	{"class_id":"account","item_id":"phzhglb","attribute_id":"password","attribute_value":"ph123456","encode":"true"},
	{"class_id":"account","item_id":"phzhglb","attribute_id":"birthday","attribute_value":"2003-03-03"},
	{"class_id":"account","item_id":"phzhglb","attribute_id":"name","attribute_value":"普华综合管理部"},

	{"class_id":"account","item_id":"phhyb","attribute_id":"password","attribute_value":"ph123456","encode":"true"},
	{"class_id":"account","item_id":"phhyb","attribute_id":"birthday","attribute_value":"2003-03-03"},
	{"class_id":"account","item_id":"phhyb","attribute_id":"name","attribute_value":"普华合约部"},

	{"class_id":"account","item_id":"phcwb","attribute_id":"password","attribute_value":"ph123456","encode":"true"},
	{"class_id":"account","item_id":"phcwb","attribute_id":"birthday","attribute_value":"2003-03-03"},
	{"class_id":"account","item_id":"phcwb","attribute_id":"name","attribute_value":"普华财务部"},

	{"class_id":"account","item_id":"phcgb","attribute_id":"password","attribute_value":"ph123456","encode":"true"},
	{"class_id":"account","item_id":"phcgb","attribute_id":"birthday","attribute_value":"2003-03-03"},
	{"class_id":"account","item_id":"phcgb","attribute_id":"name","attribute_value":"普华采购部"},

	{"class_id":"account","item_id":"phckgl","attribute_id":"password","attribute_value":"ph123456","encode":"true"},
	{"class_id":"account","item_id":"phckgl","attribute_id":"birthday","attribute_value":"2003-03-03"},
	{"class_id":"account","item_id":"phckgl","attribute_id":"name","attribute_value":"普华仓库"},

	{"class_id":"account","item_id":"phjjb","attribute_id":"password","attribute_value":"ph123456","encode":"true"},
	{"class_id":"account","item_id":"phjjb","attribute_id":"birthday","attribute_value":"2003-03-03"},
	{"class_id":"account","item_id":"phjjb","attribute_id":"name","attribute_value":"普华计经部"},

	{"class_id":"account","item_id":"phzab","attribute_id":"password","attribute_value":"ph123456","encode":"true"},
	{"class_id":"account","item_id":"phzab","attribute_id":"birthday","attribute_value":"2003-03-03"},
	{"class_id":"account","item_id":"phzab","attribute_id":"name","attribute_value":"普华质安部"},

	{"class_id":"account","item_id":"phgcb","attribute_id":"password","attribute_value":"ph123456","encode":"true"},
	{"class_id":"account","item_id":"phgcb","attribute_id":"birthday","attribute_value":"2003-03-03"},
	{"class_id":"account","item_id":"phgcb","attribute_id":"name","attribute_value":"普华工程部"},

	{"class_id":"account","item_id":"phxmb","attribute_id":"password","attribute_value":"ph123456","encode":"true"},
	{"class_id":"account","item_id":"phxmb","attribute_id":"birthday","attribute_value":"2003-03-03"},
	{"class_id":"account","item_id":"phxmb","attribute_id":"name","attribute_value":"普华项目部"}
]');

--role function relation
SELECT func_create_ci_relation_by_id ('[
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"role_administrator","relate_id":"view_config"},

		{"origin_class_id":"role","relate_class_id":"function","origin_id":"role_user","relate_id":"view_task"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"role_user","relate_id":"view_project"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"role_user","relate_id":"view_report"},

		{"origin_class_id":"role","relate_class_id":"function","origin_id":"role_visitor","relate_id":"view_report"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"role_administrator","relate_id":"view_task"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"role_administrator","relate_id":"view_project"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"role_administrator","relate_id":"view_report"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"role_administrator","relate_id":"view_material"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"role_administrator","relate_id":"view_document"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"role_administrator","relate_id":"view_project_initial"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"role_administrator","relate_id":"view_project_assign"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"role_administrator","relate_id":"view_project_cost"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"role_administrator","relate_id":"view_project_schedule"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"role_administrator","relate_id":"view_kick_off"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"role_administrator","relate_id":"view_acceptance"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"role_administrator","relate_id":"view_system_config"},

		{"origin_class_id":"role","relate_class_id":"function","origin_id":"ceo","relate_id":"view_task"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"ceo","relate_id":"view_project"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"business director","relate_id":"view_task"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"business director","relate_id":"view_project"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"production director","relate_id":"view_task"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"production director","relate_id":"view_project"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"service dept","relate_id":"view_task"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"service dept","relate_id":"view_project"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"law dept","relate_id":"view_task"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"law dept","relate_id":"view_project"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"finance dept","relate_id":"view_task"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"finance dept","relate_id":"view_project"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"purchase dept","relate_id":"view_task"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"purchase dept","relate_id":"view_project"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"warehouse dept","relate_id":"view_task"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"warehouse dept","relate_id":"view_project"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"ie dept","relate_id":"view_task"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"ie dept","relate_id":"view_project"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"quality dept","relate_id":"view_task"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"quality dept","relate_id":"view_project"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"project dept","relate_id":"view_task"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"project dept","relate_id":"view_project"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"project team","relate_id":"view_task"},
		{"origin_class_id":"role","relate_class_id":"function","origin_id":"project team","relate_id":"view_project"}
]');

--function item attribute
SELECT func_create_ci_attribute_by_id('[
		{"class_id":"function","item_id":"view_task","attribute_id":"page_url","attribute_value":"epm/view/task.html"},
		{"class_id":"function","item_id":"view_task","attribute_id":"page_class","attribute_value":"com.pr.views.Task"},

		{"class_id":"function","item_id":"view_project","attribute_id":"page_url","attribute_value":"epm/view/project.html"},
		{"class_id":"function","item_id":"view_project","attribute_id":"page_class","attribute_value":"com.pr.views.Project"},

		{"class_id":"function","item_id":"view_report","attribute_id":"page_url","attribute_value":"epm/view/count.html"},
		{"class_id":"function","item_id":"view_report","attribute_id":"page_class","attribute_value":"com.pr.views.Count"},

		{"class_id":"function","item_id":"view_config","attribute_id":"page_url","attribute_value":"epm/view/user.html"},
		{"class_id":"function","item_id":"view_config","attribute_id":"page_class","attribute_value":"com.pr.views.User"},


		{"class_id":"function","item_id":"view_project_cost","attribute_id":"page_url","attribute_value":"epm/view/task_count.html"},
		{"class_id":"function","item_id":"view_project_cost","attribute_id":"page_class","attribute_value":"com.pr.views.Task_Count"},

		{"class_id":"function","item_id":"view_project_schedule","attribute_id":"page_url","attribute_value":"epm/view/task_proces_plan.html"},
		{"class_id":"function","item_id":"view_project_schedule","attribute_id":"page_class","attribute_value":"com.pr.views.Task_Proces_Plan"},

		{"class_id":"function","item_id":"view_material","attribute_id":"page_url","attribute_value":"epm/view/task_material.html"},
		{"class_id":"function","item_id":"view_material","attribute_id":"page_class","attribute_value":"com.pr.views.Task_Material"},

		{"class_id":"function","item_id":"view_kick_off","attribute_id":"page_url","attribute_value":"epm/view/task_report.html"},
		{"class_id":"function","item_id":"view_kick_off","attribute_id":"page_class","attribute_value":"com.pr.views.Task_Report"},

		{"class_id":"function","item_id":"view_acceptance","attribute_id":"page_url","attribute_value":"epm/view/task_check.html"},
		{"class_id":"function","item_id":"view_acceptance","attribute_id":"page_class","attribute_value":"com.pr.views.Task_Check"},

		{"class_id":"function","item_id":"view_document","attribute_id":"page_url","attribute_value":"epm/view/task_document.html"},
		{"class_id":"function","item_id":"view_document","attribute_id":"page_class","attribute_value":"com.pr.views.task_document"},

		{"class_id":"function","item_id":"view_project_initial","attribute_id":"page_url","attribute_value":"epm/view/task_newcontract.html"},
		{"class_id":"function","item_id":"view_project_initial","attribute_id":"page_class","attribute_value":"com.pr.views.Task_Newcontract"},

		{"class_id":"function","item_id":"view_project_assign","attribute_id":"page_url","attribute_value":"epm/view/task_assign.html"},
		{"class_id":"function","item_id":"view_project_assign","attribute_id":"page_class","attribute_value":"com.pr.views.Task_Assign"}
]');

--account item attribute
SELECT func_create_ci_attribute_by_id('[
	{"class_id":"account","item_id":"ph666","attribute_id":"password","attribute_value":"ph6666","encode":"true"},
	{"class_id":"account","item_id":"ph666","attribute_id":"birthday","attribute_value":"2001-01-01"},
	{"class_id":"account","item_id":"ph666","attribute_id":"name","attribute_value":"普华666的姓名"},

	{"class_id":"account","item_id":"ph888","attribute_id":"password","attribute_value":"ph8888","encode":"true"},
	{"class_id":"account","item_id":"ph888","attribute_id":"birthday","attribute_value":"2002-02-02"},
	{"class_id":"account","item_id":"ph888","attribute_id":"name","attribute_value":"普华888的姓名"},

	{"class_id":"account","item_id":"ph999","attribute_id":"password","attribute_value":"ph9999","encode":"true"},
	{"class_id":"account","item_id":"ph999","attribute_id":"birthday","attribute_value":"2003-03-03"},
	{"class_id":"account","item_id":"ph999","attribute_id":"name","attribute_value":"普华999的姓名"}
]');

--menu item attribute
SELECT func_create_ci_attribute_by_id('[
		{"class_id":"menu","item_id":"menu_task","attribute_id":"hint","attribute_value":"等待处理的任务"},
		{"class_id":"menu","item_id":"menu_task","attribute_id":"icon","attribute_value":""},
		{"class_id":"menu","item_id":"menu_task","attribute_id":"shortcut_icon","attribute_value":"images/icons/logo_task.ico"},
		{"class_id":"menu","item_id":"menu_task","attribute_id":"topic_icon","attribute_value":"fa fa-cog"},

		{"class_id":"menu","item_id":"menu_project","attribute_id":"hint","attribute_value":"与我相关的工程"},
		{"class_id":"menu","item_id":"menu_project","attribute_id":"icon","attribute_value":""},
		{"class_id":"menu","item_id":"menu_project","attribute_id":"shortcut_icon","attribute_value":"images/icons/logo_project.ico"},
		{"class_id":"menu","item_id":"menu_project","attribute_id":"topic_icon","attribute_value":"fa fa-cog"},

		{"class_id":"menu","item_id":"menu_report","attribute_id":"hint","attribute_value":"项目统计分析"},
		{"class_id":"menu","item_id":"menu_report","attribute_id":"icon","attribute_value":""},
		{"class_id":"menu","item_id":"menu_report","attribute_id":"shortcut_icon","attribute_value":"images/icons/logo_count.ico"},
		{"class_id":"menu","item_id":"menu_report","attribute_id":"topic_icon","attribute_value":"fa fa-cog"},

		{"class_id":"menu","item_id":"menu_config","attribute_id":"hint","attribute_value":"系统管理"},
		{"class_id":"menu","item_id":"menu_config","attribute_id":"icon","attribute_value":""},
		{"class_id":"menu","item_id":"menu_config","attribute_id":"shortcut_icon","attribute_value":"images/icons/logo_user.ico"},
		{"class_id":"menu","item_id":"menu_config","attribute_id":"topic_icon","attribute_value":"fa fa-cog"}
]');

--menu function relation
SELECT func_create_ci_relation_by_id ('[
		{"origin_class_id":"menu","relate_class_id":"function","origin_id":"menu_task","relate_id":"view_task"},
		{"origin_class_id":"menu","relate_class_id":"function","origin_id":"menu_project","relate_id":"view_project"},
		{"origin_class_id":"menu","relate_class_id":"function","origin_id":"menu_report","relate_id":"view_report"},
		{"origin_class_id":"menu","relate_class_id":"function","origin_id":"menu_config","relate_id":"view_config"}
]');

--flow item
SELECT func_create_ci_by_id ('[
		{"id":"epm flow","name":"普华施工管理流程","class_id":"flow"},	

		{"id":"begin","name":"开始","class_id":"flow","parent_id":"epm flow"},	
		{"id":"plan","name":"工程规划阶段","class_id":"flow","parent_id":"epm flow","pre_id":"begin"},	
		{"id":"prepare","name":"工程准备阶段","class_id":"flow","parent_id":"epm flow","pre_id":"plan"},	
		{"id":"actualize","name":"工程施工阶段","class_id":"flow","parent_id":"epm flow","pre_id":"prepare"},	
		{"id":"acceptance","name":"工程验收阶段","class_id":"flow","parent_id":"epm flow","pre_id":"actualize"},	
		{"id":"closure","name":"工程结案阶段","class_id":"flow","parent_id":"epm flow","pre_id":"acceptance"},	
		{"id":"end","name":"结束","class_id":"flow","parent_id":"epm flow","pre_id":"closure"},

		{"id":"contract input","name":"合同录入","class_id":"flow","parent_id":"plan"},
		{"id":"task assign","name":"指派项目组","class_id":"flow","parent_id":"plan","pre_id":"contract input"},

		{"id":"single task plan","name":"编制单项成本任务书","class_id":"flow","parent_id":"plan","pre_id":"contract input"},
		{"id":"approve_01","name":"单项成本任务书-审核-生产副总","class_id":"flow","parent_id":"plan","pre_id":"single task plan"},
		{"id":"approve_02","name":"单项成本任务书-审核-经营副总","class_id":"flow","parent_id":"plan","pre_id":"approve_01"},

		{"id":"material prepare","name":"材料准备阶段","class_id":"flow","parent_id":"prepare"},
		{"id":"material plan","name":"编制材料申领单","class_id":"flow","parent_id":"material prepare"},
		{"id":"approve_11","name":"材料申领单-审核-工程部","class_id":"flow","parent_id":"material prepare","pre_id":"material plan"},
		{"id":"approve_12","name":"材料申领单-审核-计经部","class_id":"flow","parent_id":"material prepare","pre_id":"approve_11"},
		{"id":"approve_13","name":"材料申领单-审核-生产副总","class_id":"flow","parent_id":"material prepare","pre_id":"approve_12"},

		{"id":"purchase plan","name":"填写材料申购单","class_id":"flow","parent_id":"material prepare","pre_id":"approve_13"},
		{"id":"approve_14","name":"材料申购单-审核-采购部","class_id":"flow","parent_id":"material prepare","pre_id":"purchase plan"},
		{"id":"approve_15","name":"材料申购单-审核-经营副总","class_id":"flow","parent_id":"material prepare","pre_id":"approve_14"},
		{"id":"purchase storage","name":"仓库收货","class_id":"flow","parent_id":"material prepare","pre_id":"approve_15"},

		{"id":"make task plan","name":"编制施工计划","class_id":"flow","parent_id":"prepare"},
		{"id":"approve_21","name":"施工计划-审核-工程部","class_id":"flow","parent_id":"prepare","pre_id":"make task plan"},
		{"id":"approve_22","name":"施工计划-审核-质安部","class_id":"flow","parent_id":"prepare","pre_id":"approve_21"},
		{"id":"approve_23","name":"施工计划-审核-生产副总","class_id":"flow","parent_id":"prepare","pre_id":"approve_22"},

		{"id":"starting report","name":"编制开工报告","class_id":"flow","parent_id":"prepare","pre_id":"approve_23"},
		{"id":"approve_24","name":"开工报告-审核-工程部","class_id":"flow","parent_id":"prepare","pre_id":"starting report"},
		{"id":"approve_25","name":"开工报告-审核-质安部","class_id":"flow","parent_id":"prepare","pre_id":"approve_24"},
		{"id":"approve_26","name":"开工报告-审核-生产副总","class_id":"flow","parent_id":"prepare","pre_id":"approve_25"},

		{"id":"material get","name":"材料发放阶段","class_id":"flow","parent_id":"prepare","pre_id":"approve_26"},
		{"id":"material apply","name":"材料领用","class_id":"flow","parent_id":"material get"},
		{"id":"material delivery","name":"仓库发货","class_id":"flow","parent_id":"material get","pre_id":"material apply"},

		{"id":"document_01","name":"工程资料上报-财务部","class_id":"flow","parent_id":"actualize"},
		{"id":"approve_31","name":"财务部-工程资料-审核-总经理","class_id":"flow","parent_id":"actualize","pre_id":"document_01"},	
	
		{"id":"document_02","name":"工程资料上报-采购部","class_id":"flow","parent_id":"actualize"},
		{"id":"approve_32","name":"采购部-工程资料-审核-经营副总","class_id":"flow","parent_id":"actualize","pre_id":"document_02"},

		{"id":"document_03","name":"工程资料上报-计经部","class_id":"flow","parent_id":"actualize"},
		{"id":"approve_33","name":"计经部-工程资料-审核-生产副总","class_id":"flow","parent_id":"actualize","pre_id":"document_03"},
		{"id":"document_04","name":"工程资料上报-质安部","class_id":"flow","parent_id":"actualize"},
		{"id":"approve_34","name":"质安部-工程资料-审核-生产副总","class_id":"flow","parent_id":"actualize","pre_id":"document_04"},
		{"id":"document_05","name":"工程资料上报-工程部","class_id":"flow","parent_id":"actualize"},
		{"id":"approve_35","name":"工程部-工程资料-审核-生产副总","class_id":"flow","parent_id":"actualize","pre_id":"document_05"},
		{"id":"document_06","name":"工程资料上报-项目部","class_id":"flow","parent_id":"actualize"},
		{"id":"approve_36","name":"项目部-工程资料-审核-生产副总","class_id":"flow","parent_id":"actualize","pre_id":"document_06"},

		{"id":"acceptance_01","name":"一级验收申请-项目部","class_id":"flow","parent_id":"acceptance"},
		{"id":"document_11","name":"一级验收资料上报-项目部","class_id":"flow","parent_id":"acceptance","pre_id":"acceptance_01"},
		{"id":"approve_41","name":"一级验收-审核-工程部","class_id":"flow","parent_id":"acceptance","pre_id":"document_11"},

		{"id":"acceptance_02","name":"二级验收申请-工程部","class_id":"flow","parent_id":"acceptance","pre_id":"approve_41"},
		{"id":"document_12","name":"二级验收资料上报-工程部","class_id":"flow","parent_id":"acceptance","pre_id":"acceptance_02"},
		{"id":"approve_42","name":"二级验收-审核-质安部","class_id":"flow","parent_id":"acceptance","pre_id":"document_12"},		

		{"id":"acceptance_03","name":"三级验收申请-质安部","class_id":"flow","parent_id":"acceptance","pre_id":"approve_42"},
		{"id":"document_13","name":"三级验收资料上报-质安部","class_id":"flow","parent_id":"acceptance","pre_id":"acceptance_03"},
		{"id":"approve_43","name":"三级验收-审核-生产副总","class_id":"flow","parent_id":"acceptance","pre_id":"document_13"},	

		{"id":"material back","name":"材料回收阶段","class_id":"flow","parent_id":"closure"},
		{"id":"material return","name":"工程退料申请","class_id":"flow","parent_id":"material back"},	
		{"id":"material storage","name":"退料收货","class_id":"flow","parent_id":"material back","pre_id":"material return"},	

		{"id":"balance accounts","name":"项目结算阶段","class_id":"flow","parent_id":"closure","pre_id":"material back"},	
		{"id":"balance report","name":"编制结算报告","class_id":"flow","parent_id":"balance accounts"},
		{"id":"approve_51","name":"结算报告-审核-财务部","class_id":"flow","parent_id":"balance accounts","pre_id":"balance report"},	
		{"id":"approve_52","name":"结算报告-审核-采购部","class_id":"flow","parent_id":"balance accounts","pre_id":"balance report"},	
		{"id":"approve_53","name":"结算报告-审核-仓  库","class_id":"flow","parent_id":"balance accounts","pre_id":"balance report"},	
		{"id":"approve_54","name":"结算报告-审核-工程部","class_id":"flow","parent_id":"balance accounts","pre_id":"balance report"},	
		{"id":"approve_55","name":"结算报告-审核-项目部","class_id":"flow","parent_id":"balance accounts","pre_id":"balance report"},

		{"id":"approve_56","name":"结算报告-审核-生产副总","class_id":"flow","parent_id":"closure","pre_id":"balance accounts"},	
		{"id":"approve_57","name":"结算报告-审核-经营副总","class_id":"flow","parent_id":"closure","pre_id":"approve_56"},	
		{"id":"approve_58","name":"结算报告-审核-总经理","class_id":"flow","parent_id":"closure","pre_id":"approve_57"},

		{"id":"documented","name":"资料归档阶段","class_id":"flow","parent_id":"closure","pre_id":"approve_58"},	

		{"id":"document_21","name":"竣工资料上报-采购部","class_id":"flow","parent_id":"documented"},
		{"id":"approve_61","name":"采购部-竣工资料-审核-经营副总","class_id":"flow","parent_id":"documented","pre_id":"document_21"},		
		{"id":"document_22","name":"竣工资料上报-计经部","class_id":"flow","parent_id":"documented"},
		{"id":"approve_62","name":"计经部-竣工资料-审核-生产副总","class_id":"flow","parent_id":"documented","pre_id":"document_22"},
		{"id":"document_23","name":"竣工资料上报-质安部","class_id":"flow","parent_id":"documented"},
		{"id":"approve_63","name":"质安部-竣工资料-审核-生产副总","class_id":"flow","parent_id":"documented","pre_id":"document_23"},
		{"id":"document_24","name":"竣工资料上报-项目部","class_id":"flow","parent_id":"documented"},
		{"id":"approve_64","name":"项目部-竣工资料-审核-生产副总","class_id":"flow","parent_id":"documented","pre_id":"document_24"},

		{"id":"approve_65","name":"竣工资料归档-管理部","class_id":"flow","parent_id":"closure","pre_id":"documented"}
]');

--set flow is approve
UPDATE t_ci_flow SET valid = FALSE WHERE id NOT LIKE 'approve_%';

--flow item attribute
SELECT func_create_ci_attribute_by_id('[
	{"class_id":"flow","item_id":"material plan","attribute_id":"stage","attribute_value":"resource_plan"},
	{"class_id":"flow","item_id":"purchase plan","attribute_id":"stage","attribute_value":"purchase_plan"},
	{"class_id":"flow","item_id":"purchase storage","attribute_id":"stage","attribute_value":"purchase_storage"},
	{"class_id":"flow","item_id":"material apply","attribute_id":"stage","attribute_value":"resource_apply"},
	{"class_id":"flow","item_id":"material delivery","attribute_id":"stage","attribute_value":"resource_delivery"},
	{"class_id":"flow","item_id":"material return","attribute_id":"stage","attribute_value":"resource_return"},
	{"class_id":"flow","item_id":"material storage","attribute_id":"stage","attribute_value":"resource_storage"},

	{"class_id":"flow","item_id":"contract input","attribute_id":"stage","attribute_value":"contract_input"},
	{"class_id":"flow","item_id":"make task plan","attribute_id":"stage","attribute_value":"make_task_plan"},
	{"class_id":"flow","item_id":"starting report","attribute_id":"stage","attribute_value":"starting_report"},
	{"class_id":"flow","item_id":"acceptance","attribute_id":"stage","attribute_value":"acceptance"},

	{"class_id":"flow","item_id":"single task plan","attribute_id":"stage","attribute_value":"cost_plan"},
	{"class_id":"flow","item_id":"balance report","attribute_id":"stage","attribute_value":"cost_result"},

	{"class_id":"flow","item_id":"document_01","attribute_id":"edit_item","attribute_value":"6-1"},
	{"class_id":"flow","item_id":"approve_31","attribute_id":"edit_item","attribute_value":"6-2"},
	{"class_id":"flow","item_id":"document_02","attribute_id":"edit_item","attribute_value":"5-1"},
	{"class_id":"flow","item_id":"approve_32","attribute_id":"edit_item","attribute_value":"5-2"},
	{"class_id":"flow","item_id":"document_03","attribute_id":"edit_item","attribute_value":"4-1"},
	{"class_id":"flow","item_id":"approve_33","attribute_id":"edit_item","attribute_value":"4-2"},
	{"class_id":"flow","item_id":"document_04","attribute_id":"edit_item","attribute_value":"3-1"},
	{"class_id":"flow","item_id":"approve_34","attribute_id":"edit_item","attribute_value":"3-2"},
	{"class_id":"flow","item_id":"document_05","attribute_id":"edit_item","attribute_value":"2-1"},
	{"class_id":"flow","item_id":"approve_35","attribute_id":"edit_item","attribute_value":"2-2"},
	{"class_id":"flow","item_id":"document_06","attribute_id":"edit_item","attribute_value":"1-1"},
	{"class_id":"flow","item_id":"approve_36","attribute_id":"edit_item","attribute_value":"1-2"},

	
	{"class_id":"flow","item_id":"document_11","attribute_id":"edit_item","attribute_value":"1-3"},
	{"class_id":"flow","item_id":"document_12","attribute_id":"edit_item","attribute_value":"2-3"},
	{"class_id":"flow","item_id":"document_13","attribute_id":"edit_item","attribute_value":"3-3"},

	{"class_id":"flow","item_id":"document_21","attribute_id":"edit_item","attribute_value":"5-5"},
	{"class_id":"flow","item_id":"approve_61","attribute_id":"edit_item","attribute_value":"5-6"},
	{"class_id":"flow","item_id":"document_22","attribute_id":"edit_item","attribute_value":"4-5"},
	{"class_id":"flow","item_id":"approve_62","attribute_id":"edit_item","attribute_value":"4-6"},
	{"class_id":"flow","item_id":"document_23","attribute_id":"edit_item","attribute_value":"3-5"},
	{"class_id":"flow","item_id":"approve_63","attribute_id":"edit_item","attribute_value":"3-6"},
	{"class_id":"flow","item_id":"document_24","attribute_id":"edit_item","attribute_value":"1-5"},
	{"class_id":"flow","item_id":"approve_64","attribute_id":"edit_item","attribute_value":"1-6"}

]');

--flow function relation
SELECT func_create_ci_relation_by_id ('[
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"contract input","relate_id":"view_project_initial"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"single task plan","relate_id":"view_project_cost"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"approve_01","relate_id":"view_project_cost"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"approve_02","relate_id":"view_project_cost"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"task assign","relate_id":"view_project_assign"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"material plan","relate_id":"view_material"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"approve_11","relate_id":"view_material"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"approve_12","relate_id":"view_material"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"approve_13","relate_id":"view_material"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"purchase plan","relate_id":"view_material"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"approve_14","relate_id":"view_material"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"approve_15","relate_id":"view_material"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"purchase storage","relate_id":"view_material"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"make task plan","relate_id":"view_project_schedule"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"approve_21","relate_id":"view_project_schedule"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"approve_22","relate_id":"view_project_schedule"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"approve_23","relate_id":"view_project_schedule"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"starting report","relate_id":"view_kick_off"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"approve_24","relate_id":"view_kick_off"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"approve_25","relate_id":"view_kick_off"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"approve_26","relate_id":"view_kick_off"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"material apply","relate_id":"view_material"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"material delivery","relate_id":"view_material"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"document_01","relate_id":"view_document"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"approve_31","relate_id":"view_document"},	
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"document_02","relate_id":"view_document"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"approve_32","relate_id":"view_document"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"document_03","relate_id":"view_document"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"approve_33","relate_id":"view_document"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"document_04","relate_id":"view_document"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"approve_34","relate_id":"view_document"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"document_05","relate_id":"view_document"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"approve_35","relate_id":"view_document"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"document_06","relate_id":"view_document"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"approve_36","relate_id":"view_document"},		
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"acceptance_01","relate_id":"view_acceptance"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"document_11","relate_id":"view_document"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"approve_41","relate_id":"view_acceptance"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"acceptance_02","relate_id":"view_acceptance"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"document_12","relate_id":"view_document"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"approve_42","relate_id":"view_acceptance"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"acceptance_03","relate_id":"view_acceptance"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"document_13","relate_id":"view_document"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"approve_43","relate_id":"view_acceptance"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"material return","relate_id":"view_material"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"material storage","relate_id":"view_material"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"balance report","relate_id":"view_project_cost"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"approve_51","relate_id":"view_project_cost"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"approve_52","relate_id":"view_project_cost"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"approve_53","relate_id":"view_project_cost"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"approve_54","relate_id":"view_project_cost"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"approve_55","relate_id":"view_project_cost"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"approve_56","relate_id":"view_project_cost"},	
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"approve_57","relate_id":"view_project_cost"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"approve_58","relate_id":"view_project_cost"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"document_21","relate_id":"view_document"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"approve_61","relate_id":"view_document"},	
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"document_22","relate_id":"view_document"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"approve_62","relate_id":"view_document"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"document_23","relate_id":"view_document"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"approve_63","relate_id":"view_document"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"document_24","relate_id":"view_document"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"approve_64","relate_id":"view_document"},
		{"origin_class_id":"flow","relate_class_id":"function","origin_id":"approve_65","relate_id":"view_document"}
]');

--flow role relation
SELECT func_create_ci_relation_by_id ('[
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"law dept","origin_id":"contract input"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"project dept","origin_id":"task assign"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"ie dept","origin_id":"single task plan"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"production director","origin_id":"approve_01"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"business director","origin_id":"approve_02"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"project team","origin_id":"material plan"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"project dept","origin_id":"approve_11"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"ie dept","origin_id":"approve_12"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"production director","origin_id":"approve_13"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"warehouse dept","origin_id":"purchase plan"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"purchase dept","origin_id":"approve_14"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"business director","origin_id":"approve_15"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"warehouse dept","origin_id":"purchase storage"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"project team","origin_id":"make task plan"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"project dept","origin_id":"approve_21"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"quality dept","origin_id":"approve_22"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"production director","origin_id":"approve_23"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"project team","origin_id":"starting report"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"project dept","origin_id":"approve_24"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"quality dept","origin_id":"approve_25"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"production director","origin_id":"approve_26"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"project team","origin_id":"material apply"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"warehouse dept","origin_id":"material delivery"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"finance dept","origin_id":"document_01"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"ceo","origin_id":"approve_31"},	
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"purchase dept","origin_id":"document_02"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"business director","origin_id":"approve_32"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"ie dept","origin_id":"document_03"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"production director","origin_id":"approve_33"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"quality dept","origin_id":"document_04"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"production director","origin_id":"approve_34"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"project dept","origin_id":"document_05"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"production director","origin_id":"approve_35"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"project team","origin_id":"document_06"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"production director","origin_id":"approve_36"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"project team","origin_id":"acceptance_01"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"project team","origin_id":"document_11"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"project dept","origin_id":"approve_41"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"project dept","origin_id":"acceptance_02"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"project dept","origin_id":"document_12"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"quality dept","origin_id":"approve_42"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"quality dept","origin_id":"acceptance_03"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"quality dept","origin_id":"document_13"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"production director","origin_id":"approve_43"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"project team","origin_id":"material return"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"warehouse dept","origin_id":"material storage"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"ie dept","origin_id":"balance report"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"finance dept","origin_id":"approve_51"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"purchase dept","origin_id":"approve_52"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"warehouse dept","origin_id":"approve_53"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"project dept","origin_id":"approve_54"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"project team","origin_id":"approve_55"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"production director","origin_id":"approve_56"},	
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"business director","origin_id":"approve_57"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"ceo","origin_id":"approve_58"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"purchase dept","origin_id":"document_21"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"business director","origin_id":"approve_61"},	
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"ie dept","origin_id":"document_22"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"production director","origin_id":"approve_62"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"quality dept","origin_id":"document_23"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"production director","origin_id":"approve_63"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"project team","origin_id":"document_24"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"production director","origin_id":"approve_64"},
		{"origin_class_id":"flow","relate_class_id":"role","relate_id":"service dept","origin_id":"approve_65"}
]');
INSERT INTO t_rel_account_role
SELECT t1.class_uid,t2.class_uid,t1.uid,t2.uid
FROM t_ci_account AS t1,t_ci_role AS t2
WHERE t1.id = 'ph999'
  AND t2.id NOT IN('role_visitor');