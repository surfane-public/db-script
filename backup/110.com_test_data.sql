/*
	system data;
*/
-- account item
SELECT dml_ci_insert ('[
	{"class_id":"account","id":"SysAdmin","name":"系统管理员帐号"}
]');

-- account item info
SELECT dml_ci_info_insert('[
	{"id":"SysAdmin","info_id":"password","info_value":"SysAdmin"},
	{"id":"SysAdmin","info_id":"birthday","info_value":"2001-01-01"},
	{"id":"SysAdmin","info_id":"name","info_value":"系统管理员姓名"}
]');

-- role item
SELECT dml_ci_insert ('[
	{"class_id":"role","id":"role_system_administrator","name":"系统管理员"}
]');

-- account role relation
SELECT dml_ci_relation_insert ('[
	{"origin_id":"SysAdmin","relate_id":"role_system_administrator"}
]');

-- ui item
SELECT dml_ci_insert ('[
	{"class_id":"ui","id":"ui_system_class_manage","name":"分类管理界面"},		
	{"class_id":"ui","id":"ui_system_class_info_manage","name":"分类属性管理界面"},		
	{"class_id":"ui","id":"ui_system_class_relation_manage","name":"分类关系管理界面"},
	{"class_id":"ui","id":"ui_system_ci_manage","name":"实例管理界面"},
	{"class_id":"ui","id":"ui_system_ci_relation_manage","name":"实例关系管理界面"}
]');

-- ui item info
SELECT dml_ci_info_insert('[
	{"id":"ui_system_class_manage","info_id":"page_url","info_value":"page/classManagement.html"},
	{"id":"ui_system_class_manage","info_id":"page_class","info_value":"classManagement"},
	{"id":"ui_system_class_manage","info_id":"target","info_value":"div"},
	
	{"id":"ui_system_class_info_manage","info_id":"page_url","info_value":"page/classInfoManagement.html"},
	{"id":"ui_system_class_info_manage","info_id":"page_class","info_value":"classInfoManagement"},
	{"id":"ui_system_class_info_manage","info_id":"target","info_value":"div"},
	
	{"id":"ui_system_class_relation_manage","info_id":"page_url","info_value":"page/classRelationManagement.html"},
	{"id":"ui_system_class_relation_manage","info_id":"page_class","info_value":"classRelationManagement"},
	{"id":"ui_system_class_relation_manage","info_id":"target","info_value":"div"},
	
	{"id":"ui_system_ci_manage","info_id":"page_url","info_value":"page/ciManagement.html"},
	{"id":"ui_system_ci_manage","info_id":"page_class","info_value":"ciManagement"},
	{"id":"ui_system_ci_manage","info_id":"target","info_value":"div"},
	
	{"id":"ui_system_ci_relation_manage","info_id":"page_url","info_value":"page/ciRelationManagement.html"},
	{"id":"ui_system_ci_relation_manage","info_id":"page_class","info_value":"ciRelationManagement"},
	{"id":"ui_system_ci_relation_manage","info_id":"target","info_value":"div"}
]');
	
-- menu item
SELECT dml_ci_insert ('[
	{"class_id":"menu","id":"menu_portal","name":"平台菜单"},
	{"class_id":"menu","parent_id":"menu_portal","id":"menu_system_config","name":"系统运行配置"},
	{"class_id":"menu","parent_id":"menu_system_config","id":"menu_system_class_manage","name":"分类管理"},
	{"class_id":"menu","parent_id":"menu_system_config","id":"menu_system_class_info_manage","name":"分类属性管理","pre_id":"menu_system_class_manage"},		
	{"class_id":"menu","parent_id":"menu_system_config","id":"menu_system_class_relation_manage","name":"分类关系管理","pre_id":"menu_system_class_info_manage"},
	{"class_id":"menu","parent_id":"menu_system_config","id":"menu_system_ci_manage","name":"实例管理","pre_id":"menu_system_class_relation_manage"},
	{"class_id":"menu","parent_id":"menu_system_config","id":"menu_system_ci_relation_manage","name":"实例关系管理","pre_id":"menu_system_ci_manage"}
]');

-- menu item info
SELECT dml_ci_info_insert('[
	{"id":"menu_system_config","info_id":"hint","info_value":"系统配置"},
	{"id":"menu_system_config","info_id":"icon","info_value":""},
	{"id":"menu_system_config","info_id":"shortcut_icon","info_value":"image/icons/logo_Diag.ico"},
	{"id":"menu_system_config","info_id":"topic_icon","info_value":"fa fa-cog"},
	
	{"id":"menu_system_class_manage","info_id":"hint","info_value":"分类管理"},
	{"id":"menu_system_class_manage","info_id":"icon","info_value":""},
	{"id":"menu_system_class_manage","info_id":"shortcut_icon","info_value":"image/icons/logo_Diag.ico"},
	{"id":"menu_system_class_manage","info_id":"topic_icon","info_value":"fa fa-cog"},

	{"id":"menu_system_class_info_manage","info_id":"hint","info_value":"分类信息管理"},
	{"id":"menu_system_class_info_manage","info_id":"icon","info_value":""},
	{"id":"menu_system_class_info_manage","info_id":"shortcut_icon","info_value":"image/icons/logo_Diag.ico"},
	{"id":"menu_system_class_info_manage","info_id":"topic_icon","info_value":"fa fa-cog"},
	
	{"id":"menu_system_class_relation_manage","info_id":"hint","info_value":"分类关系管理"},
	{"id":"menu_system_class_relation_manage","info_id":"icon","info_value":""},
	{"id":"menu_system_class_relation_manage","info_id":"shortcut_icon","info_value":"image/icons/logo_Diag.ico"},
	{"id":"menu_system_class_relation_manage","info_id":"topic_icon","info_value":"fa fa-cog"},

	{"id":"menu_system_ci_manage","info_id":"hint","info_value":"实例管理"},
	{"id":"menu_system_ci_manage","info_id":"icon","info_value":""},
	{"id":"menu_system_ci_manage","info_id":"shortcut_icon","info_value":"image/icons/logo_Diag.ico"},
	{"id":"menu_system_ci_manage","info_id":"topic_icon","info_value":"fa fa-cog"},

	{"id":"menu_system_ci_relation_manage","info_id":"hint","info_value":"实例关系管理"},
	{"id":"menu_system_ci_relation_manage","info_id":"icon","info_value":""},
	{"id":"menu_system_ci_relation_manage","info_id":"shortcut_icon","info_value":"image/icons/logo_Diag.ico"},
	{"id":"menu_system_ci_relation_manage","info_id":"topic_icon","info_value":"fa fa-cog"}
]');

-- menu ui relation
SELECT dml_ci_relation_insert ('[
	{"origin_id":"menu_system_class_manage","relate_id":"ui_system_class_manage"},
	{"origin_id":"menu_system_class_info_manage","relate_id":"ui_system_class_info_manage"},
	{"origin_id":"menu_system_class_relation_manage","relate_id":"ui_system_class_relation_manage"},
	{"origin_id":"menu_system_ci_manage","relate_id":"ui_system_ci_manage"},
	{"origin_id":"menu_system_ci_relation_manage","relate_id":"ui_system_ci_relation_manage"}
]');

-- role ui relation
SELECT dml_ci_relation_insert ('[
	{"origin_id":"role_system_administrator","relate_id":"ui_system_class_manage"},	
	{"origin_id":"role_system_administrator","relate_id":"ui_system_class_info_manage"},
	{"origin_id":"role_system_administrator","relate_id":"ui_system_class_relation_manage"},
	{"origin_id":"role_system_administrator","relate_id":"ui_system_ci_manage"},
	{"origin_id":"role_system_administrator","relate_id":"ui_system_ci_relation_manage"}
]');